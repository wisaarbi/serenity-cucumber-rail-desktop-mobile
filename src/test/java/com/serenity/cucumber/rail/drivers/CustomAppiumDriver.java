package com.serenity.cucumber.rail.drivers;

import io.appium.java_client.android.AndroidDriver;
import lombok.SneakyThrows;
import org.apache.commons.text.WordUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.util.HashMap;

public class CustomAppiumDriver extends CustomWebDriver {

    private HashMap<String, String> appiumCapabilities;
    private DesiredCapabilities capabilities;

    public CustomAppiumDriver(HashMap<String, String> appiumCapabilities) {
        this.appiumCapabilities = appiumCapabilities;
        capabilities = new DesiredCapabilities();
    }

    @SneakyThrows
    @Override
    public void initializeLocalDriver() {
        URL url = new URL(appiumCapabilities.get("hub"));
        driver = new AndroidDriver<>(url, capabilities);
    }

    @Override
    public void initializeRemoteDriver(String remoteUrl) {

    }

    @Override
    public void setLocalCapability() {
        CustomMobilePlatform platformName = CustomMobilePlatform.valueOf(appiumCapabilities.get("platformName"));
        capabilities.setCapability("platformName", WordUtils.capitalizeFully(platformName.toString()));
        capabilities.setCapability("automationName", appiumCapabilities.get("automationName"));
        capabilities.setCapability("udid", appiumCapabilities.get("udid"));
        capabilities.setCapability("noReset", false);
        switch (platformName) {
            case ANDROID:
                capabilities.setCapability("appPackage", "blibli.mobile.commerce");
                capabilities.setCapability("appActivity", "blibli.mobile.ng.commerce.core.init.view.SplashActivity");
                capabilities.setCapability("systemPort", appiumCapabilities.get("systemPort"));
                break;
            default:
                capabilities.setCapability("appPackage", "blibli.mobile.commerce");
                capabilities.setCapability("appActivity", "blibli.mobile.ng.commerce.core.init.view.SplashActivity");
                capabilities.setCapability("systemPort", appiumCapabilities.get("systemPort"));
                break;
        }
    }

    @Override
    public void setRemoteCapability(boolean enableVnc, boolean enableVideo) {

    }

    @Override
    public WebDriver getDriver() {
        return driver;
    }
}
