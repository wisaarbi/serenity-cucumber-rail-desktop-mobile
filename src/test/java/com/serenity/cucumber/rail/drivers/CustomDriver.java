package com.serenity.cucumber.rail.drivers;

public enum CustomDriver {
    CHROME,
    FIREFOX,
    APPIUM
}
