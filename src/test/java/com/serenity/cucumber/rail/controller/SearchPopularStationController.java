package com.serenity.cucumber.rail.controller;

import com.serenity.cucumber.rail.responses.railavailability.getsearchpopularstation.GetSearchPopularStationResponse;
import io.restassured.response.Response;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.controller.SearchPopularStationController")
public class SearchPopularStationController extends ServiceApi {

    public GetSearchPopularStationResponse getSearchPopularStation() {
        Response response =  services("blibli")
                .when().get("/travel/tiket-kereta-api/search-popular-station");
        response.prettyPrint();
        return response.getBody().as(GetSearchPopularStationResponse.class);
    }
}
