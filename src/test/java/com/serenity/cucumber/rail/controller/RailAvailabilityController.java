package com.serenity.cucumber.rail.controller;

import com.serenity.cucumber.rail.data.RailAPIData;
import com.serenity.cucumber.rail.responses.railavailability.getrailticketavailability.GetRailTicketAvailabilityResponse;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.controller.RailAvailabilityController")
public class RailAvailabilityController extends ServiceApi {

    @Autowired
    RailAPIData railAPIData;

    public GetRailTicketAvailabilityResponse getRailAvailability() {
        Response response =  services("blibli")
                .queryParam("originCode", railAPIData.getOriginCode())
                .queryParam("originType", railAPIData.getOriginType())
                .queryParam("destinationCode", railAPIData.getDestinationCode())
                .queryParam("destinationType", railAPIData.getDestinationType())
                .queryParam("departureDate", railAPIData.getDepartureDate())
                .queryParam("returnDate", railAPIData.getReturnDate())
                .queryParam("adult", railAPIData.getAdult())
                .queryParam("infant", railAPIData.getInfant())
                .when().get("/travel/tiket-kereta-api/rail-search-availability");
        response.prettyPrint();
        return response.getBody().as(GetRailTicketAvailabilityResponse.class);
    }
}
