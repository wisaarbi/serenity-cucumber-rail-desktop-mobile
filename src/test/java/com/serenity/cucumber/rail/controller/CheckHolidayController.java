package com.serenity.cucumber.rail.controller;

import com.serenity.cucumber.rail.data.CheckHolidayAPIData;
import com.serenity.cucumber.rail.responses.checkholiday.getcheckholiday.GetCheckHolidayResponse;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.controller.CheckHolidayController")
public class CheckHolidayController extends ServiceApi {

    @Autowired
    CheckHolidayAPIData checkHolidayAPIData;

    public GetCheckHolidayResponse getCheckHoliday() {
        Response response =  services("blibli")
                .queryParam("yearStart", checkHolidayAPIData.getYearStart())
                .queryParam("monthStart", checkHolidayAPIData.getMonthStart())
                .queryParam("yearEnd", checkHolidayAPIData.getYearEnd())
                .queryParam("monthEnd", checkHolidayAPIData.getMonthEnd())
                .when().get("/backend/travel/holiday");
        response.prettyPrint();
        return response.getBody().as(GetCheckHolidayResponse.class);
    }
}
