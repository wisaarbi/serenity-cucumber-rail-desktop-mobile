package com.serenity.cucumber.rail.controller;

import com.serenity.cucumber.rail.data.LoginAPIData;
import com.serenity.cucumber.rail.responses.login.getuserlogin.GetUserLoginResponse;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.controller.LoginController")
public class LoginController extends ServiceApi {

    @Autowired
    LoginAPIData loginAPIData;

    public GetUserLoginResponse getUserLogin() {
        Response response =  services("blibli")
                .cookies(loginAPIData.getLoginCookies())
                .when().get("/backend/common/users");
        response.prettyPrint();
        return response.getBody().as(GetUserLoginResponse.class);
    }
}
