package com.serenity.cucumber.rail.controller;

import com.serenity.cucumber.rail.properties.DefaultProperties;
import com.serenity.cucumber.rail.properties.ServicesProperties;
import io.restassured.specification.RequestSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.given;

@Component("com.serenity.cucumber.rail.controller.ServiceApi")
public class ServiceApi {

    @Autowired
    ServicesProperties servicesProperties;

    @Autowired
    DefaultProperties defaultProperties;
    
    public RequestSpecification services(String serviceName) {
        RequestSpecification requestSpecification = given();
        addDefaultHeader(requestSpecification);
        addDefaultServiceUrl(requestSpecification, serviceName);
        addDefaultCookies(requestSpecification);
        return requestSpecification;
    }

    private void addDefaultServiceUrl(RequestSpecification requestSpecification, String serviceName) {
        String baseUri = servicesProperties.getClient().get(serviceName);
        requestSpecification.baseUri(baseUri);
    }

    private void addDefaultHeader(RequestSpecification requestSpecification) {
        HashMap<String, String> headers = defaultProperties.getHeaders();
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            requestSpecification.header(entry.getKey(), entry.getValue());
        }
    }

    private void addDefaultCookies(RequestSpecification requestSpecification) {
        HashMap<String, String> cookies = defaultProperties.getCookies();
        for (Map.Entry<String, String> entry : cookies.entrySet()) {
            requestSpecification.cookie(entry.getKey(), entry.getValue());
        }
    }
}
