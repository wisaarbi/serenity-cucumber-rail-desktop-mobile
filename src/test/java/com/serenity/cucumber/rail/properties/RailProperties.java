package com.serenity.cucumber.rail.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component("com.serenity.cucumber.rail.properties.RailProperties")
@Data
@ConfigurationProperties(prefix = "rail")
public class RailProperties {
    private HashMap<String, String> data;

    public String get(String key) {
        return data.get(key);
    }
}
