package com.serenity.cucumber.rail.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component("com.serenity.cucumber.rail.properties.DefaultProperties")
@Data
@ConfigurationProperties(prefix = "default")
public class DefaultProperties {
    private HashMap<String, String> headers;
    private HashMap<String, String> cookies;
}
