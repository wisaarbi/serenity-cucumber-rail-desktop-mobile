package com.serenity.cucumber.rail.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component("com.serenity.cucumber.rail.properties.LoginProperties")
@Data
@ConfigurationProperties(prefix = "login")
public class LoginProperties {
    private HashMap<String, String> data;

    public String get(String key) {
        return data.get(key);
    }
}
