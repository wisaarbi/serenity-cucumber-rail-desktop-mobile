package com.serenity.cucumber.rail.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component("com.serenity.cucumber.rail.properties.ServicesProperties")
@Data
@ConfigurationProperties(prefix = "services")
public class ServicesProperties {
    private HashMap<String, String> client;
}

