package com.serenity.cucumber.rail.responses.railavailability.getrailticketavailability;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DepartureRailListItem{
	private String departureTime;
	private double infantFare;
	private String trainSubClass;
	private boolean bookable;
	private double totalFare;
	private String providerCode;
	private Origin origin;
	private Destination destination;
	private String trainClass;
	private double discount;
	private String trainNo;
	private double offerFare;
	private int availableSeat;
	private int infant;
	private int duration;
	private String arrivalTime;
	private String trainName;
	private String currency;
	private String departureDate;
	private double totalInfantFare;
	private String trainClassName;
	private double adultFare;
	private double totalAdultFare;
	private String arrivalDate;
	private int adult;
}