package com.serenity.cucumber.rail.responses.login.getuserlogin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class UserLoginConfig {
	private String whatsappEnabled;
}