package com.serenity.cucumber.rail.responses.railavailability.getsearchpopularstation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CityListItem{
	private String originalCityName;
	private String cityName;
	private String cityCode;
	private List<StationListItem> stationList;
	private double timeZone;
}