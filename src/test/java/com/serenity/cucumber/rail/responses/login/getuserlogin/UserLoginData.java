package com.serenity.cucumber.rail.responses.login.getuserlogin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class UserLoginData {
	private String lastName;
	private UserLoginConfig userLoginConfig;
	private String hashedEmail;
	private boolean hasRecoveryAccount;
	private boolean showCustomerInfo;
	private boolean walletActive;
	private boolean blipaySuspendedStatus;
	private String firstName;
	private long createdAt;
	private boolean emailVerified;
	private String phoneNumber;
	private boolean postPnvMember;
	private boolean needVerifiedPhoneNumber;
	private boolean phoneNumberVerified;
	private int id;
	private int rewardPoints;
	private String email;
	private int wishlistCount;
	private String username;
}