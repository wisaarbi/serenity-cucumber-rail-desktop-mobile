package com.serenity.cucumber.rail.responses.railavailability.getrailticketavailability;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Value{
	private int totalRail;
	private List<DepartureRailListItem> departureRailList;
}