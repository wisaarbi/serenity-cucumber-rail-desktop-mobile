package com.serenity.cucumber.rail.responses.railavailability.getsearchpopularstation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GetSearchPopularStationResponse {
	private int code;
	private String subCode;
	private boolean success;
	private Object message;
	private Value value;
	private Object errors;
}
