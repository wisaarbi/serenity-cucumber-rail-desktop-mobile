package com.serenity.cucumber.rail.responses.railavailability.getrailticketavailability;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GetRailTicketAvailabilityResponse{
	private int code;
	private String subCode;
	private boolean success;
	private String message;
	private Value value;
	private Object errors;
}