package com.serenity.cucumber.rail.responses.railavailability.getrailticketavailability;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Origin{
	private Object stationCompleteName;
	private String stationCode;
	private City city;
	private double latitude;
	private String stationName;
	private double longitude;
}