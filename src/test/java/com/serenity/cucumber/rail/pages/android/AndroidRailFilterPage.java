package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("com.serenity.cucumber.rail.pages.android.AndroidRailFilterPage")
public class AndroidRailFilterPage extends MobileUtility {

    @FindBy(xpath = "//android.widget.CheckBox[contains(@text,':')]")
    private List<WebElementFacade> listFilterTimeDeparture;

    @FindBy(id = "blibli.mobile.commerce:id/bt_apply")
    private WebElementFacade btnApply;

    @FindBy(xpath = "//android.widget.TextView[contains(@text,'Kelas')]")
    private WebElementFacade titleClass;

    @FindBy(xpath = "//android.widget.TextView[contains(@text,'Harga')]")
    private WebElementFacade titlePrice;

    @FindBy(xpath = "//android.widget.CheckBox[contains(@resource-id,'cb_class')]")
    private List<WebElementFacade> listFilterClassDeparture;

    @FindBy(xpath = "//android.widget.CheckBox[contains(@resource-id,'cb_class')]")
    private List<WebElementFacade> listFilterClassReturn;

    @FindBy(xpath = "//android.widget.CheckBox[contains(@resource-id,'cb_train')]")
    private List<WebElementFacade> listFilterTrainTypeDeparture;

    @FindBy(xpath = "//android.widget.CheckBox[contains(@resource-id,'cb_train')]")
    private List<WebElementFacade> listFilterTrainTypeReturn;

    @FindBy(id = "blibli.mobile.commerce:id/sb_price_range")
    private WebElementFacade seekBarPriceRange;

    @FindBy(id = "blibli.mobile.commerce:id/tv_price_range_value")
    private WebElementFacade txtPriceRangeValue;

    public List<String> getListFilterTimeDeparture() {
        List<String> listFilterDepartureTime = new ArrayList<>();
        for (WebElementFacade webElementFacade : listFilterTimeDeparture) {
            listFilterDepartureTime.add(webElementFacade.getText());
        }
        return listFilterDepartureTime;
    }

    public AndroidRailFilterPage filterTimeDeparture(String timeDeparture) {
        for (WebElementFacade webElementFacade : listFilterTimeDeparture) {
            if (webElementFacade.getText().contains(timeDeparture)) {
                webElementFacade.click();
                break;
            }
        }
        return this;
    }

    public void clickApply() {
        btnApply.click();
    }

    public AndroidRailFilterPage scrollToTitleClass() {
        scrollToElement(titleClass, 5);
        return this;
    }

    public List<String> getListFilterClassDeparture() {
        List<String> listFilterDepartureClass = new ArrayList<>();
        for (WebElementFacade webElementFacade : listFilterClassDeparture) {
            listFilterDepartureClass.add(webElementFacade.getText());
        }
        return listFilterDepartureClass;
    }

    public List<String> getListFilterClassReturn() {
        List<String> listFilterReturnClass = new ArrayList<>();
        for (WebElementFacade webElementFacade : listFilterClassReturn) {
            listFilterReturnClass.add(webElementFacade.getText());
        }
        return listFilterReturnClass;
    }

    public AndroidRailFilterPage filterClassDeparture(String classDeparture) {
        for (WebElementFacade webElementFacade : listFilterClassDeparture) {
            if (webElementFacade.getText().contains(classDeparture)) {
                webElementFacade.click();
                break;
            }
        }
        return this;
    }

    public AndroidRailFilterPage filterClassReturn(String classReturn) {
        for (WebElementFacade webElementFacade : listFilterClassReturn) {
            if (webElementFacade.getText().contains(classReturn)) {
                webElementFacade.click();
                break;
            }
        }
        return this;
    }

    public List<String> getListFilterTrainTypeDeparture() {
        List<String> listFilterDepartureTrainType = new ArrayList<>();
        for (WebElementFacade webElementFacade : listFilterTrainTypeDeparture) {
            listFilterDepartureTrainType.add(webElementFacade.getText());
        }
        return listFilterDepartureTrainType;
    }

    public List<String> getListFilterTrainTypeReturn() {
        List<String> listFilterReturnTrainType = new ArrayList<>();
        for (WebElementFacade webElementFacade : listFilterTrainTypeReturn) {
            listFilterReturnTrainType.add(webElementFacade.getText());
        }
        return listFilterReturnTrainType;
    }

    public AndroidRailFilterPage filterTrainTypeDeparture(String trainTypeDeparture) {
        for (WebElementFacade webElementFacade : listFilterTrainTypeDeparture) {
            if (webElementFacade.getText().contains(trainTypeDeparture)) {
                webElementFacade.click();
                break;
            }
        }
        return this;
    }

    public AndroidRailFilterPage filterTrainTypeReturn(String trainTypeReturn) {
        for (WebElementFacade webElementFacade : listFilterTrainTypeReturn) {
            if (webElementFacade.getText().contains(trainTypeReturn)) {
                webElementFacade.click();
                break;
            }
        }
        return this;
    }

    public AndroidRailFilterPage scrollToTitlePrice() {
        scrollToElement(seekBarPriceRange, 5);
        return this;
    }

    public String getMaxPriceDeparture() {
        String[] priceRange = txtPriceRangeValue.getText().split(" - ");
        return priceRange[1].substring(0, 4).replace(".", "");
    }

    public AndroidRailFilterPage filterDeparturePriceToMaxPrice() {
        int xAxisStartPoint = seekBarPriceRange.getLocation().getX();
        int xAxisEndPoint = xAxisStartPoint + seekBarPriceRange.getSize().getWidth();
        int yAxis = seekBarPriceRange.getLocation().getY();
        swipe(xAxisStartPoint, yAxis, xAxisEndPoint, yAxis);
        return this;
    }

    public String getMinPriceDeparture() {
        String[] priceRange = txtPriceRangeValue.getText().split(" - ");
        return priceRange[0].substring(0, 4).replace(".", "");
    }

    public AndroidRailFilterPage filterDeparturePriceToMinPrice() {
        int xAxisStartPoint = seekBarPriceRange.getLocation().getX();
        int xAxisEndPoint = xAxisStartPoint + seekBarPriceRange.getSize().getWidth();
        int yAxis = seekBarPriceRange.getLocation().getY();
        swipe(xAxisEndPoint, yAxis, xAxisStartPoint, yAxis);
        return this;
    }
}
