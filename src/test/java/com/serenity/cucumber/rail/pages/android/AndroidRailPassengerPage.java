package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.pages.android.AndroidRailPassengerPage")
public class AndroidRailPassengerPage extends MobileUtility {

    @FindBy(id = "blibli.mobile.commerce:id/baby_count")
    private WebElementFacade currentTotalInfantPassenger;

    @FindBy(id = "blibli.mobile.commerce:id/adult_count")
    private WebElementFacade currentTotalAdultPassenger;

    @FindBy(id = "blibli.mobile.commerce:id/baby_incrementor")
    private WebElementFacade btnPlusInfantPassenger;

    @FindBy(id = "blibli.mobile.commerce:id/baby_decrementor")
    private WebElementFacade btnMinusInfantPassenger;

    @FindBy(id = "blibli.mobile.commerce:id/adult_incrementor")
    private WebElementFacade btnPlusAdultPassenger;

    @FindBy(id = "blibli.mobile.commerce:id/adult_decrementor")
    private WebElementFacade btnMinusAdultPassenger;

    @FindBy(id = "blibli.mobile.commerce:id/confirmation")
    private WebElementFacade btnConfirmation;

    public AndroidRailPassengerPage fillPassenger(int totalAdultPassenger, int totalInfantPassenger) {
        fillAdultPassenger(totalAdultPassenger);
        fillInfantPassenger(totalInfantPassenger);
        return this;
    }

    private void fillInfantPassenger(int totalInfantPassenger) {
        int totalInfant = Integer.parseInt(currentTotalInfantPassenger.getText());
        while (totalInfant != totalInfantPassenger) {
            if(totalInfant < totalInfantPassenger) {
                btnPlusInfantPassenger.click();
                totalInfant++;
            }
            else {
                btnMinusInfantPassenger.click();
                totalInfant--;
            }
        }
    }

    private void fillAdultPassenger(int totalAdultPassenger) {
        int totalAdult = Integer.parseInt(currentTotalAdultPassenger.getText());
        while (totalAdult != totalAdultPassenger) {
            if(totalAdult < totalAdultPassenger) {
                btnPlusAdultPassenger.click();
                totalAdult++;
            }
            else {
                btnMinusAdultPassenger.click();
                totalAdult--;
            }
        }
    }

    public void clickConfirm() {
        btnConfirmation.click();
    }
}
