package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("com.serenity.cucumber.rail.pages.android.AndroidRailSeatPage")
public class AndroidRailSeatPage extends MobileUtility {

    @FindBy(id = "blibli.mobile.commerce:id/et_wagon_drop_down")
    private WebElementFacade dropdownCarriage;

    @FindBy(id = "blibli.mobile.commerce:id/tv_passenger_name")
    private List<WebElementFacade> listPassenger;

    @FindBy(xpath = "//android.widget.TextView[@clickable='true'][@text='']")
    private List<WebElementFacade> listAvailableSeat;

    @FindBy(id = "blibli.mobile.commerce:id/tv_economy")
    private List<WebElementFacade> selectedCarriage;
    
    @FindBy(id = "blibli.mobile.commerce:id/tv_coach_no")
    private List<WebElementFacade> selectedSeat;

    @FindBy(id = "blibli.mobile.commerce:id/bt_proceed")
    private WebElementFacade btnChangeReturnSeat;

    @FindBy(id = "blibli.mobile.commerce:id/bt_proceed")
    private WebElementFacade btnProceedToPayment;

    public AndroidRailSeatPage chooseCarriage() {
        for(int i = 0; i < 3; i++) {
            clickByCoordinatesUsingId(dropdownCarriage, 60, 140 * (i+1));
            if(isSeatAvailable()) {
                break;
            }
        }
        return this;
    }

    private boolean isSeatAvailable() {
        return listAvailableSeat.size() > 0;
    }

    public AndroidRailSeatPage choosePassenger(int indexPassenger) {
        listPassenger.get(indexPassenger).click();
        return this;
    }

    public AndroidRailSeatPage chooseSeat() {
        scrollToElement(listAvailableSeat.get(0), 5);
        listAvailableSeat.get(0).click();
        return this;
    }

    public List<String> getSelectedCarriage() {
        List<String> listSelectedCarriage = new ArrayList<>();
        for (WebElementFacade webElementFacade : selectedCarriage) {
            listSelectedCarriage.add(webElementFacade.getText());
        }
        return listSelectedCarriage;
    }

    public List<String> getSelectedSeat() {
        List<String> listSelectedSeat = new ArrayList<>();
        for (WebElementFacade webElementFacade : selectedSeat) {
            listSelectedSeat.add(webElementFacade.getText());
        }
        return listSelectedSeat;
    }

    public AndroidRailSeatPage changeReturnSeat() {
        btnChangeReturnSeat.click();
        return this;
    }

    public void clickProceedToPayment() {
        btnProceedToPayment.click();
    }
}
