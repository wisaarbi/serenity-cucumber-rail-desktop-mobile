package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.pages.android.AndroidAccountPage")
public class AndroidAccountPage extends MobileUtility {

    @FindBy(id = "blibli.mobile.commerce:id/vw_layout_header")
    private WebElementFacade profileInfo;

    public void clickProfileInfo() {
        profileInfo.click();
    }
}
