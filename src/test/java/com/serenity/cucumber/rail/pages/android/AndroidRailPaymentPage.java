package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("com.serenity.cucumber.rail.pages.android.AndroidRailPaymentPage")
public class AndroidRailPaymentPage extends MobileUtility {

    @FindBy(id = "blibli.mobile.commerce:id/down_arrow_image")
    private WebElementFacade billDetails;

    @FindBy(id = "blibli.mobile.commerce:id/total_amount_layout")
    private WebElementFacade totalPayment;

    @FindBy(xpath = "(//android.widget.TextView[@resource-id='blibli.mobile.commerce:id/tv_from'])[1]")
    private WebElementFacade departureRoute;

    @FindBy(xpath = "(//android.widget.TextView[@resource-id='blibli.mobile.commerce:id/tv_from'])[2]")
    private WebElementFacade returnRoute;

    @FindBy(id = "blibli.mobile.commerce:id/tv_adults")
    private WebElementFacade totalAdultPassenger;

    @FindBy(id = "blibli.mobile.commerce:id/tv_infants")
    private WebElementFacade totalInfantPassenger;

    @FindBy(id = "blibli.mobile.commerce:id/rb_payment_category")
    private List<WebElementFacade> listRadioButtonPaymentCategory;

    @FindBy(id = "blibli.mobile.commerce:id/rb_payment_option")
    private List<WebElementFacade> listRadioButtonPaymentOption;

    @FindBy(id = "blibli.mobile.commerce:id/bt_understand")
    private WebElementFacade btnUnderstand;

    @FindBy(id = "blibli.mobile.commerce:id/pay_now_button")
    private WebElementFacade btnPayNow;

    @FindBy(id = "blibli.mobile.commerce:id/cancel_button")
    private WebElementFacade btnCancelOrder;

    @FindBy(id = "android:id/button1")
    private WebElementFacade btnConfirmCancelOrder;

    public void clickBillDetails() {
        billDetails.click();
    }

    public boolean isTotalPaymentVisible() {
        return totalPayment.isVisible();
    }

    public boolean isDepartureRouteVisible() {
        return departureRoute.isVisible();
    }

    public boolean isReturnRouteVisible() {
        return returnRoute.isVisible();
    }

    public boolean isTotalAdultPassengerVisible() {
        return totalAdultPassenger.isVisible();
    }

    public boolean isTotalInfantPassengerVisible() {
        return totalInfantPassenger.isVisible();
    }

    public void fillPaymentMethod() {
        listRadioButtonPaymentCategory.get(2).click();
        listRadioButtonPaymentOption.get(0).click();
        btnUnderstand.click();
        btnPayNow.click();
    }

    public AndroidRailPaymentPage clickCancelOrder() {
        btnCancelOrder.click();
        return this;
    }

    public AndroidRailPaymentPage clickConfirmCancelOrder() {
        btnConfirmCancelOrder.click();
        return this;
    }
}
