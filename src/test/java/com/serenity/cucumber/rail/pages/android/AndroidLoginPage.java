package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.pages.android.AndroidLoginPage")
public class AndroidLoginPage extends MobileUtility {

    @FindBy(xpath = "//android.widget.EditText[contains(@text,'Email')]")
    private WebElementFacade inputEmail;

    @FindBy(xpath = "//android.widget.EditText[contains(@text,'Kata sandi')]")
    private WebElementFacade inputPassword;

    @FindBy(id = "blibli.mobile.commerce:id/bt_login")
    private WebElementFacade btnLogin;

    @FindBy(id = "blibli.mobile.commerce:id/tv_message")
    private WebElementFacade errorMessage;

    public AndroidLoginPage fillEmail(String email) {
        inputEmail.type(email);
        return this;
    }

    public AndroidLoginPage fillPassword(String password) {
        inputPassword.type(password);
        return this;
    }

    public void clickLogin() {
        btnLogin.click();
    }

    public boolean isErrorMessageVisible() {
        return errorMessage.isVisible();
    }

    public String getErrorMessage() {
        return errorMessage.getText();
    }

    public boolean isButtonLoginEnabled() {
        return btnLogin.isEnabled();
    }
}
