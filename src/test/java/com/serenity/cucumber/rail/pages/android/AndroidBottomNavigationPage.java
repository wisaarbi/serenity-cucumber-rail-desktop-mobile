package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("com.serenity.cucumber.rail.pages.android.AndroidBottomNavigationPage")
public class AndroidBottomNavigationPage extends MobileUtility {

    private static final String xpathContentBottomNavigation = "//android.widget.FrameLayout[@content-desc='%s']";

    public void clickBottomNavigation(String content) {
        WebElementFacade contentBottomNavigation = find(By.xpath(String.format(xpathContentBottomNavigation, content)));
        contentBottomNavigation.click();
    }

}
