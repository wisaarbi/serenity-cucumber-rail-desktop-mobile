package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.List;
import java.util.Locale;

@Component("com.serenity.cucumber.rail.pages.android.AndroidRailDateDeparturePage")
public class AndroidRailDateDeparturePage extends MobileUtility {

    @FindBy(xpath = "//android.widget.ImageView[@content-desc=\"Go to previous\"]/following-sibling::android.widget.TextView")
    private WebElementFacade currentMonth;

    @FindBy(xpath = "//android.widget.ImageView[@content-desc=\"Lanjut\"]")
    private WebElementFacade btnNextMonth;

    private static final String xpathListCurrentDateDeparture = "//blibli.mobile.materialcalendarview.f[@content-desc=\"Calendar\"]//android.widget.CheckedTextView[@enabled='true']";

    public void fillDateDeparture(String dateDeparture) {
        Locale locale = new Locale("in", "ID");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", locale);
        LocalDate localDateDeparture = LocalDate.parse(dateDeparture, formatter);
        while (!currentMonth.getText().contains(localDateDeparture.getMonth().getDisplayName(TextStyle.SHORT, locale))) {
            btnNextMonth.click();
        }
        List<WebElementFacade> listCurrentDateTableDeparture = findAll(xpathListCurrentDateDeparture);
        for (WebElementFacade webElementFacade : listCurrentDateTableDeparture) {
            if (webElementFacade.getText().equals(String.valueOf(localDateDeparture.getDayOfMonth()))) {
                webElementFacade.click();
                break;
            }
        }
    }
}
