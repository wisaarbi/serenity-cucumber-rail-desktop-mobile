package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.pages.android.AndroidRailSearchPage")
public class AndroidRailSearchPage extends MobileUtility {

    @FindBy(id = "blibli.mobile.commerce:id/support_home")
    private WebElementFacade railSearchForm;

    @FindBy(id = "blibli.mobile.commerce:id/tv_source_station")
    private WebElementFacade btnDepartureLocation;

    @FindBy(id = "blibli.mobile.commerce:id/tv_destination_station")
    private WebElementFacade btnDestinationLocation;

    @FindBy(id = "blibli.mobile.commerce:id/tv_start_date")
    private WebElementFacade btnDepartureDate;

    @FindBy(id = "blibli.mobile.commerce:id/sw_round_trip")
    private WebElementFacade toggleSwitchRoundTrip;

    @FindBy(id = "blibli.mobile.commerce:id/tv_return_date")
    private WebElementFacade btnReturnDate;

    @FindBy(id = "blibli.mobile.commerce:id/tv_no_of_passengers")
    private WebElementFacade btnPassenger;

    @FindBy(id = "blibli.mobile.commerce:id/bt_select_train")
    private WebElementFacade btnCari;

    @FindBy(id = "blibli.mobile.commerce:id/iv_swap_stations")
    private WebElementFacade btnSwapStation;

    public boolean isRailSearchFormVisible() {
        return railSearchForm.isVisible();
    }

    public void clickDepartureLocation() {
        btnDepartureLocation.click();
    }

    public void clickDestinationLocation() {
        btnDestinationLocation.click();
    }

    public void clickDateDeparture() {
        btnDepartureDate.click();
    }

    public void clickPassenger() {
        btnPassenger.click();
    }

    public void clickCari() {
        btnCari.click();
    }

    public AndroidRailSearchPage clickRoundTrip() {
        toggleSwitchRoundTrip.click();
        return this;
    }

    public void clickDateReturn() {
        btnReturnDate.click();
    }

    public AndroidRailSearchPage clickSwapStation() {
        btnSwapStation.click();
        return this;
    }

    public String getDepartureStation() {
        return btnDepartureLocation.getText();
    }

    public String getDestinationStation() {
        return btnDestinationLocation.getText();
    }
}
