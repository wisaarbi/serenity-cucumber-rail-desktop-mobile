package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.commons.text.WordUtils;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("com.serenity.cucumber.rail.pages.android.AndroidRailDepartureLocationPage")
public class AndroidRailDepartureLocationPage extends MobileUtility {

    @FindBy(id = "blibli.mobile.commerce:id/tv_station_name")
    private List<WebElementFacade> listPopularCity;

    @FindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Kembali ke atas\"]")
    private WebElementFacade btnBack;

    @FindBy(id = "blibli.mobile.commerce:id/auto_complete_text")
    private WebElementFacade inputLocationDeparture;

    @FindBy(id = "blibli.mobile.commerce:id/hotel_no_items_found_text")
    private WebElementFacade searchErrorMessage;

    private static final String xpathDeptStation = "//android.widget.TextView[contains(@resource-id,'tv_city_name')]/preceding-sibling::android.widget.TextView[contains(@text,'%s')]";

    public boolean isListPopularCityVisible() {
        for (WebElementFacade webElementFacade : listPopularCity) {
            if (!webElementFacade.isVisible()) {
                return false;
            }
        }
        return true;
    }

    public List<String> getListPopularCity() {
        List<String> listCity = new ArrayList<>();
        for (WebElementFacade webElementFacade : listPopularCity) {
            listCity.add(webElementFacade.getText());
        }
        return listCity;
    }

    public void clickBackButton() {
        btnBack.click();
    }

    public AndroidRailDepartureLocationPage fillDepartureCity(String deptCity) {
        inputLocationDeparture.type(deptCity);
        return this;
    }

    public void fillDepartureStation(String deptStation) {
        String formattedDeptStation = WordUtils.capitalizeFully(deptStation);
        WebElementFacade departureStation = find(By.xpath(String.format(xpathDeptStation, formattedDeptStation)));
        if(departureStation.isVisible()) {
            departureStation.click();
        }
    }

    public boolean isSearchErrorMessageVisible() {
        return searchErrorMessage.isVisible();
    }
}
