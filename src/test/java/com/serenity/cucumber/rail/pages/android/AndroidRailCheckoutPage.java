package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;

@Component("com.serenity.cucumber.rail.pages.android.AndroidRailCheckoutPage")
public class AndroidRailCheckoutPage extends MobileUtility {

    @FindBy(xpath = "//android.widget.TextView[contains(@resource-id,'tv_see_details')]//parent::android.view.ViewGroup")
    private WebElementFacade orderDetails;

    @FindBy(id = "blibli.mobile.commerce:id/tv_see_details")
    private WebElementFacade btnSeeOrderDetails;

    @FindBy(id = "blibli.mobile.commerce:id/et_title")
    private WebElementFacade btnDropdownTitle;

    @FindBy(id = "blibli.mobile.commerce:id/et_name")
    private WebElementFacade txtFieldCustomerName;

    @FindBy(id = "blibli.mobile.commerce:id/et_phone_no")
    private WebElementFacade txtFieldCustomerPhoneNumber;

    @FindBy(id = "blibli.mobile.commerce:id/et_email")
    private WebElementFacade txtFieldCustomerEmail;

    @FindBy(id = "blibli.mobile.commerce:id/bt_proceed")
    private WebElementFacade btnContinueToFillInPassengerData;

    @FindBy(id = "blibli.mobile.commerce:id/pb_process_data")
    private WebElementFacade progressBar;

    @FindBy(id = "blibli.mobile.commerce:id/cb_copy_buyer_info")
    private WebElementFacade btnSameAsCustomer;

    @FindBy(id = "blibli.mobile.commerce:id/et_drop_down")
    private WebElementFacade dropdownPassengerTitle;

    @FindBy(xpath = "(//android.widget.EditText[contains(@resource-id,'et_data')])[1]")
    private WebElementFacade txtFieldPassengerName;

    @FindBy(xpath = "(//android.widget.EditText[contains(@resource-id,'et_data')])[2]")
    private WebElementFacade txtFieldPassengerIdentityCard;

    @FindBy(id = "blibli.mobile.commerce:id/bt_proceed")
    private WebElementFacade btnSavePassengerData;

    @FindBy(id = "blibli.mobile.commerce:id/tv_contact_change")
    private WebElementFacade btnChangeCustomerData;

    @FindBy(xpath = "(//android.widget.TextView[contains(@resource-id,'tv_change')])[1]")
    private WebElementFacade btnChangeAdultPassengerData;

    @FindBy(xpath = "(//android.widget.TextView[contains(@resource-id,'tv_change')])[2]")
    private WebElementFacade btnChangeInfantPassengerData;

    @FindBy(id = "blibli.mobile.commerce:id/bt_proceed")
    private WebElementFacade btnProceedToPayment;

    @FindBy(id = "blibli.mobile.commerce:id/bt_seat_layout")
    private WebElementFacade btnChangeSeat;

    @FindBy(xpath = "//android.widget.Toast")
    private WebElementFacade toastView;

    public boolean isOrderDetailsVisible() {
        return orderDetails.isVisible();
    }

    public void clickSeeOrderDetails() {
        btnSeeOrderDetails.click();
    }

    public AndroidRailCheckoutPage fillCustomerTitle(String customerTitle) {
        btnDropdownTitle.click();
        switch (customerTitle) {
            case "Tuan":
                clickByCoordinatesUsingId(btnDropdownTitle, 100, 130);
                break;
            case "Nyonya":
                clickByCoordinatesUsingId(btnDropdownTitle, 100, 130 * 2);
                break;
            case "Nona":
                clickByCoordinatesUsingId(btnDropdownTitle, 100, 130 * 3);
                break;
            default:
                break;
        }
        return this;
    }

    public AndroidRailCheckoutPage fillCustomerName(String customerName) {
        txtFieldCustomerName.type(customerName);
        return this;
    }

    public AndroidRailCheckoutPage fillCustomerPhoneNumber(String customerPhoneNumber) {
        txtFieldCustomerPhoneNumber.type(customerPhoneNumber);
        return this;
    }

    public AndroidRailCheckoutPage fillCustomerEmail(String customerEmail) {
        txtFieldCustomerEmail.type(customerEmail);
        return this;
    }

    public AndroidRailCheckoutPage clickContinueToFillInPassengerData() {
        btnContinueToFillInPassengerData.click();
        return this;
    }

    public String getValueProgressBar() {
        return progressBar.getText();
    }

    public boolean IsProgressBarValueIncreased(double currentProgressBarValue, double progressBarValue) {
        return currentProgressBarValue > progressBarValue;
    }

    public AndroidRailCheckoutPage clickSameAsCustomer() {
        btnSameAsCustomer.click();
        return this;
    }

    public String getTitlePassengerSameAsCustomer() {
        return dropdownPassengerTitle.getText();
    }

    public String getNamePassengerSameAsCustomer() {
        return txtFieldPassengerName.getText();
    }

    public void fillAdultPassenger(int totalAdultPassenger) {
        for (int i = 0; i < totalAdultPassenger; i++) {
            fillAdultPassengerTitleWithRandomTitle();
            fillAdultPassengerNameWithRandomName();
            fillAdultPassengerIdentityCardWithRandomIdentityCard();
            clickSavePassengerData();
        }
    }

    public void clickSavePassengerData() {
        btnSavePassengerData.click();
    }

    private void fillAdultPassengerIdentityCardWithRandomIdentityCard() {
        String adultPassengerIdentityCard = randomUtility.generateRandomDigitNumber(8);
        txtFieldPassengerIdentityCard.type(adultPassengerIdentityCard);
    }

    private void fillAdultPassengerNameWithRandomName() {
        String adultPassengerName = RandomStringUtils.randomAlphabetic(8);
        txtFieldPassengerName.type(adultPassengerName);
    }

    private void fillAdultPassengerTitleWithRandomTitle() {
        ArrayList<String> titles = new ArrayList<>(Arrays. asList("Tuan", "Nyonya", "Nona"));
        String adultPassengerTitle = randomUtility.getSingleRandomItemFromList(titles);
        dropdownPassengerTitle.click();
        switch (adultPassengerTitle) {
            case "Tuan":
                clickByCoordinatesUsingId(dropdownPassengerTitle, 100, 130);
                break;
            case "Nyonya":
                clickByCoordinatesUsingId(dropdownPassengerTitle, 100, 130 * 2);
                break;
            case "Nona":
                clickByCoordinatesUsingId(dropdownPassengerTitle, 100, 130 * 3);
                break;
            default:
                break;
        }
    }

    public void fillInfantPassenger(int totalInfantPassenger) {
        for (int i = 0; i < totalInfantPassenger; i++) {
            fillInfantPassengerTitleWithRandomTitle();
            fillInfantPassengerNameWithRandomName();
            fillInfantPassengerIdentityCardWithRandomIdentityCard();
            clickSavePassengerData();
        }
    }

    private void fillInfantPassengerIdentityCardWithRandomIdentityCard() {
        String infantPassengerIdentityCard = randomUtility.generateRandomDigitNumber(8);
        txtFieldPassengerIdentityCard.type(infantPassengerIdentityCard);
    }

    private void fillInfantPassengerNameWithRandomName() {
        String infantPassengerName = RandomStringUtils.randomAlphabetic(8);
        txtFieldPassengerName.type(infantPassengerName);
    }

    private void fillInfantPassengerTitleWithRandomTitle() {
        ArrayList<String> titles = new ArrayList<>(Arrays. asList("Sdr", "Sdri"));
        String infantPassengerTitle = randomUtility.getSingleRandomItemFromList(titles);
        dropdownPassengerTitle.click();
        switch (infantPassengerTitle) {
            case "Sdr":
                clickByCoordinatesUsingId(dropdownPassengerTitle, 60, 130);
                break;
            case "Sdri":
                clickByCoordinatesUsingId(dropdownPassengerTitle, 60, 130 * 2);
                break;
            default:
                break;
        }
    }

    public boolean isCustomerDataVisible() {
        return btnChangeCustomerData.isVisible();
    }

    public boolean isAdultPassengerDataVisible() {
        return btnChangeAdultPassengerData.isVisible();
    }

    public boolean isInfantPassengerDataVisible() {
        return btnChangeInfantPassengerData.isVisible();
    }

    public void clickProceedToPayment() {
        btnProceedToPayment.click();
    }

    public void clickChangeSeat() {
        btnChangeSeat.click();
    }

    public AndroidRailCheckoutPage fillAdultPassengerTitle(String adultPassengerTitle) {
        switch (adultPassengerTitle) {
            case "Tuan":
                clickByCoordinatesUsingId(dropdownPassengerTitle, 100, 130);
                break;
            case "Nyonya":
                clickByCoordinatesUsingId(dropdownPassengerTitle, 100, 130 * 2);
                break;
            case "Nona":
                clickByCoordinatesUsingId(dropdownPassengerTitle, 100, 130 * 3);
                break;
            default:
                break;
        }
        return this;
    }

    public AndroidRailCheckoutPage fillAdultPassengerName(String adultPassengerName) {
        txtFieldPassengerName.type(adultPassengerName);
        return this;
    }

    public void fillAdultPassengerIdentityCard(String adultPassengerIdentityCard) {
        txtFieldPassengerIdentityCard.type(adultPassengerIdentityCard);
    }
}
