package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.pages.android.AndroidAllCategoryPage")
public class AndroidAllCategoryPage extends MobileUtility {

    @FindBy(xpath = "//android.widget.TextView[contains(@text,'Kereta')]")
    private WebElementFacade btnKereta;

    public void clickKereta() {
        scrollToElement(btnKereta, 5);
        btnKereta.click();
    }
}
