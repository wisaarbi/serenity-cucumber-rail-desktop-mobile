package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.pages.android.AndroidProfilePage")
public class AndroidProfilePage extends MobileUtility {

    @FindBy(xpath = "//android.widget.TextView[contains(@text,'Email')]/following-sibling::android.widget.EditText")
    private WebElementFacade txtEmail;

    public String getEmail() {
        return getText(txtEmail, 15);
    }
}
