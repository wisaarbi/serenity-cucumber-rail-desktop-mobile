package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.pages.android.AndroidHomePage")
public class AndroidHomePage extends MobileUtility {

    @FindBy(id = "blibli.mobile.commerce:id/bt_login")
    private WebElementFacade btnLogin;

    @FindBy(xpath = "//android.widget.TextView[@text='Semua']")
    private WebElementFacade btnAllCategory;

    public AndroidHomePage openApp() {
        fluentWait(btnAllCategory, 30);
        return this;
    }

    public void clickLoginButton() {
        btnLogin.click();
    }

    public void clickAllCategory() {
        btnAllCategory.click();
    }
}
