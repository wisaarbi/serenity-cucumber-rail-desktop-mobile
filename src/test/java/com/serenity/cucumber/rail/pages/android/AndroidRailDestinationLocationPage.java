package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.commons.text.WordUtils;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.pages.android.AndroidRailDestinationLocationPage")
public class AndroidRailDestinationLocationPage extends MobileUtility {

    @FindBy(id = "blibli.mobile.commerce:id/rv_search_list")
    private WebElementFacade listPopularCity;

    @FindBy(id = "blibli.mobile.commerce:id/auto_complete_text")
    private WebElementFacade inputLocationDestination;

    private static final String xpathDestStation = "//android.widget.TextView[contains(@resource-id,'tv_city_name')]/preceding-sibling::android.widget.TextView[contains(@text,'%s')]";

    @FindBy(id = "blibli.mobile.commerce:id/hotel_no_items_found_text")
    private WebElementFacade searchErrorMessage;

    public boolean isListPopularCityVisible() {
        return listPopularCity.isVisible();
    }

    public AndroidRailDestinationLocationPage fillDestinationCity(String destCity) {
        inputLocationDestination.type(destCity);
        return this;
    }

    public void fillDestinationStation(String destStation) {
        String formattedDestStation = WordUtils.capitalizeFully(destStation);
        WebElementFacade destinationStation = find(By.xpath(String.format(xpathDestStation, formattedDestStation)));
        if(destinationStation.isVisible()) {
            destinationStation.click();
        }
    }

    public boolean isSearchErrorMessageVisible() {
        return searchErrorMessage.isVisible();
    }
}
