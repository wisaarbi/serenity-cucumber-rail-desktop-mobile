package com.serenity.cucumber.rail.pages.desktop;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.pages.desktop.DesktopHeaderPage")
public class DesktopHeaderPage extends WebElementHelper {

    @FindBy(xpath = "//div[@class='account__text']")
    private WebElementFacade userAlreadyLogin;

    public boolean isUserAlreadyLoginVisible() {
        return userAlreadyLogin.isVisible();
    }
}
