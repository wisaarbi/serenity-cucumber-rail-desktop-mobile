package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.pages.android.AndroidRailOrderDetailsPage")
public class AndroidRailOrderDetailsPage extends MobileUtility {

    @FindBy(xpath = "(//android.view.ViewGroup[contains(@resource-id,'booking_detail_onward')]//android.widget.TextView[contains(@resource-id,'tv_station')])[1]")
    private WebElementFacade departureOriginStation;

    @FindBy(xpath = "(//android.view.ViewGroup[contains(@resource-id,'booking_detail_return')]//android.widget.TextView[contains(@resource-id,'tv_station')])[1]")
    private WebElementFacade returnOriginStation;

    @FindBy(xpath = "(//android.view.ViewGroup[contains(@resource-id,'booking_detail_onward')]//android.widget.TextView[contains(@resource-id,'tv_station')])[2]")
    private WebElementFacade departureDestinationStation;

    @FindBy(xpath = "(//android.view.ViewGroup[contains(@resource-id,'booking_detail_return')]//android.widget.TextView[contains(@resource-id,'tv_station')])[2]")
    private WebElementFacade returnDestinationStation;

    @FindBy(id = "blibli.mobile.commerce:id/tv_total_price_val")
    private WebElementFacade totalTicketPrice;

    @FindBy(id = "blibli.mobile.commerce:id/onward_price")
    private WebElementFacade departureTicketPrice;

    @FindBy(id = "blibli.mobile.commerce:id/return_price")
    private WebElementFacade returnTicketPrice;

    @FindBy(xpath = "(//android.widget.TextView[contains(@resource-id,'tv_adult_count')])[1]")
    private WebElementFacade totalAdultPassenger;

    @FindBy(xpath = "(//android.widget.TextView[contains(@resource-id,'tv_baby_count')])[1]")
    private WebElementFacade totalInfantPassenger;

    @FindBy(id = "blibli.mobile.commerce:id/tv_depart_class")
    private WebElementFacade departureTrainClass;

    @FindBy(id = "blibli.mobile.commerce:id/tv_return_class")
    private WebElementFacade returnTrainClass;

    @FindBy(id = "blibli.mobile.commerce:id/tv_depart_train_name")
    private WebElementFacade departureTrainType;

    @FindBy(id = "blibli.mobile.commerce:id/tv_return_train_name")
    private WebElementFacade returnTrainType;

    @FindBy(xpath = "//android.view.ViewGroup[contains(@resource-id,'toolbar')]//android.widget.ImageButton")
    private WebElementFacade btnCloseOrderDetailsPage;

    public String getDepartureOriginStation() {
        return departureOriginStation.getText();
    }

    public String getReturnOriginStation() {
        return returnOriginStation.getText();
    }

    public String getDepartureDestinationStation() {
        return departureDestinationStation.getText();
    }

    public String getReturnDestinationStation() {
        return returnDestinationStation.getText();
    }

    public boolean isTotalTicketPriceVisible() {
        return totalTicketPrice.isVisible();
    }

    public AndroidRailOrderDetailsPage scrollToTotalTicketPrice() {
        scrollToElement(totalTicketPrice, 5);
        return this;
    }

    public boolean isDepartureTicketPriceVisible() {
        return departureTicketPrice.isVisible();
    }

    public boolean isReturnTicketPriceVisible() {
        return returnTicketPrice.isVisible();
    }

    public String getTotalAdultPassenger() {
        return StringUtils.substringBetween(totalAdultPassenger.getText(), "Dewasa (", "x");
    }

    public String getTotalInfantPassenger() {
        return StringUtils.substringBetween(totalInfantPassenger.getText(), "Bayi (", ")");
    }

    public String getDepartureTrainClass() {
        return departureTrainClass.getText();
    }

    public String getReturnTrainClass() {
        return returnTrainClass.getText();
    }

    public String getDepartureTrainType() {
        return departureTrainType.getText();
    }

    public String getReturnTrainType() {
        return returnTrainType.getText();
    }

    public void closeOrderDetailsPage() {
        btnCloseOrderDetailsPage.click();
    }
}
