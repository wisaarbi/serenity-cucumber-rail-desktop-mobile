package com.serenity.cucumber.rail.pages.android;

import com.serenity.cucumber.rail.properties.DefaultProperties;
import com.serenity.cucumber.rail.utility.RandomUtility;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;
import net.thucydides.core.webdriver.WebDriverFacade;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component("com.serenity.cucumber.rail.pages.android.MobileUtility")
public class MobileUtility extends PageObject {

    @Autowired
    DefaultProperties defaultProperties;

    @Autowired
    RandomUtility randomUtility;

    public int startx, starty, endx;

    public static AndroidDriver getCurrentDriver(){
        return (AndroidDriver) ((WebDriverFacade) ThucydidesWebDriverSupport.getDriver()).getProxiedDriver();
    }

    public void type(WebElementFacade element, String text, int timeOut) {
        fluentWait(element, timeOut);
        element.type(text);
    }

    public void fluentWait(WebElementFacade element, int timeOut) {
        element.withTimeoutOf(Duration.ofSeconds(timeOut)).waitUntilVisible();
    }

    public void waitForElementToBeClickable(By by, int timeOut) {
        WebDriverWait wait = new WebDriverWait(getCurrentDriver(), timeOut);
        wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public void back() {
        getCurrentDriver().navigate().back();
    }

    public String getText(WebElementFacade element, int timeOut) {
        fluentWait(element, timeOut);
        return element.getText();
    }

    public void initWidthSize() {
        Dimension size = getCurrentDriver().manage().window().getSize();
        startx = (int) (size.width * 0.90);
        endx = (int) (size.width * 0.10);
        starty = size.height / 2;
    }

    public void swiperToLeft() {
        initWidthSize();
        swipe(startx, starty, endx, starty);
    }

    public void scrollUpTo() {
        Dimension size;
        size = getCurrentDriver().manage().window().getSize();
        Double screenHeightStart = size.getHeight() * 0.8;
        int scrollStart = screenHeightStart.intValue();
        Double screenHeightEnd = size.getHeight() * 0.2;
        int scrollEnd = screenHeightEnd.intValue();
        swipe(0, scrollStart, 0, scrollEnd);
    }

    public void scrollDownTo() {
        Dimension size;
        size = getCurrentDriver().manage().window().getSize();
        Double screenHeightStart = size.getHeight() * 0.8;
        int scrollStart = screenHeightStart.intValue();
        Double screenHeightEnd = size.getHeight() * 0.2;
        int scrollEnd = screenHeightEnd.intValue();
        swipe(0, scrollEnd, 0, scrollStart);
    }

    public void scrollDownToElement(WebElementFacade elementIndicator, int count) {
        int i = 0;
        do {
            boolean isPresent = isVisible(elementIndicator);
            if (isPresent) {
                break;
            } else {
                scrollDownTo();
            }
            i++;
        } while (i <= count);
    }

    public static void swipe(int startx, int starty, int endx, int endy) {
        TouchAction touchAction = new TouchAction(getCurrentDriver());

        touchAction.longPress(PointOption.point(startx, starty))
                .moveTo(PointOption.point(endx, endy))
                .release()
                .perform();
    }

    public void scrollToElement(WebElementFacade elementIndicator, int count) {
        int i = 0;
        do {
            boolean isPresent = isVisible(elementIndicator);
            if (isPresent) {
                break;
            } else {
                scrollUpTo();
            }
            i++;
        } while (i <= count);
    }

    public boolean isVisible(WebElementFacade element) {
        return element.isVisible();
    }

    public void clickByCoordinatesUsingId(WebElementFacade elementId, int x, int y) {
        Point point = elementId.getLocation();
        System.out.println("clicked on :" + point.x + " - " + point.y);
        int xCoordinate = point.x + x;
        int yCoordinate = point.y + y;
        TouchAction a2 = new TouchAction(getCurrentDriver());
        System.out.println("clicked on :" + xCoordinate + " - " + yCoordinate);
        a2.tap(PointOption.point(xCoordinate, yCoordinate)).perform();
    }

    public String getToastByAttribute(String attribute) {
        String attributeToast = StringUtils.substringBetween(getCurrentDriver().getPageSource(), "<android.widget.Toast ", " />");
        return StringUtils.substringBetween(attributeToast, attribute + "=\"", "\"");
    }
}
