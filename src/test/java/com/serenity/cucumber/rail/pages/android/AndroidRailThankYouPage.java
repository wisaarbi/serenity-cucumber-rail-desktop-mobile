package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.pages.android.AndroidRailThankYouPage")
public class AndroidRailThankYouPage extends MobileUtility {

    @FindBy(id =  "blibli.mobile.commerce:id/finish")
    private WebElementFacade btnFinish;

    public boolean isThankYouSectionVisible() {
        return btnFinish.isVisible();
    }
}
