package com.serenity.cucumber.rail.pages.desktop;

import lombok.SneakyThrows;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.commons.text.WordUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component("com.serenity.cucumber.rail.pages.desktop.DesktopRailListPage")
public class DesktopRailListPage extends WebElementHelper {

    @FindBy(xpath = "//div[@class='route__departure-station']")
    private WebElementFacade departureStation;

    @FindBy(xpath = "//div[@class='route__arrival-station']")
    private WebElementFacade destinationStation;

    @FindBy(xpath = "//span[@ng-bind='rail.origin.stationName']")
    private List<WebElementFacade> listDepartureStation;

    @FindBy(xpath = "//span[@ng-bind='rail.destination.stationName']")
    private List<WebElementFacade> listDestinationStation;

    @FindBy(xpath = "//div[@id='search-page__loading-progress']")
    private WebElementFacade loadingProgress;

    @FindBy(xpath = "//span[@ng-bind='display.passenger']")
    private WebElementFacade passenger;

    @FindBy(xpath = "//label[contains(@for,'dep-filter__time')]")
    private List<WebElementFacade> listDepartureFilterTime;

    @FindBy(xpath = "//label[contains(@for,'ret-filter__time')]")
    private List<WebElementFacade> listReturnFilterTime;

    @FindBy(xpath = "//span[@ng-bind='rail.departureTime']")
    private List<WebElementFacade> listDepartureTime;

    @FindBy(xpath = "//span[@ng-bind='railReturn.departureTime']")
    private List<WebElementFacade> listReturnTime;

    @FindBy(xpath = "//label[contains(@for,'dep-filter__class')]")
    private List<WebElementFacade> listDepartureFilterClass;

    @FindBy(xpath = "//label[contains(@for,'ret-filter__class')]")
    private List<WebElementFacade> listReturnFilterClass;

    @FindBy(xpath = "//span[@class='catalog-train__name-class ng-binding'][@ng-bind='rail.trainClassCompleteName']")
    private List<WebElementFacade> listDepartureClass;

    @FindBy(xpath = "//span[@class='catalog-train__name-class ng-binding'][@ng-bind='railReturn.trainClassCompleteName']")
    private List<WebElementFacade> listReturnClass;

    @FindBy(xpath = "//input[@id='price-end-dep']")
    private WebElementFacade departureMaxPrice;

    @FindBy(xpath = "//input[@id='price-start-dep']")
    private WebElementFacade departureMinPrice;

    @FindBy(xpath = "//div[@id='js-filter__price-slider-dep']//div[@class='connect']")
    private WebElementFacade sliderPrice;

    @FindBy(xpath = "//div[@id='js-filter__price-slider-dep']//div[@class='noUi-handle noUi-handle-upper']")
    private WebElementFacade upperSliderPrice;

    @FindBy(xpath = "//div[@id='js-filter__price-slider-dep']//div[@class='noUi-handle noUi-handle-lower']")
    private WebElementFacade lowerSliderPrice;

    @FindBy(xpath = "//span[@ng-bind='rail.mainPrice']")
    private List<WebElementFacade> listDepartureMainPrice;

    @FindBy(xpath = "//span[@ng-bind='rail.secondaryPrice']")
    private List<WebElementFacade> listDepartureSecondaryPrice;

    @FindBy(xpath = "//button[@id='filter__dep--train-type']")
    private WebElementFacade btnTrainTypeDeparture;

    @FindBy(xpath = "//button[@id='filter__ret--train-type']")
    private WebElementFacade btnTrainTypeReturn;

    @FindBy(xpath = "//label[contains(@for,'dep-filter__train-type')]")
    private List<WebElementFacade> listDepartureFilterTrainType;

    @FindBy(xpath = "//label[contains(@for,'ret-filter__train-type')]")
    private List<WebElementFacade> listReturnFilterTrainType;

    @FindBy(xpath = "//span[@class='catalog-train__name ng-binding'][@ng-bind='rail.trainNameAndNo']")
    private List<WebElementFacade> listDepartureTrainType;

    @FindBy(xpath = "//span[@class='catalog-train__name ng-binding'][@ng-bind='railReturn.trainNameAndNo']")
    private List<WebElementFacade> listReturnTrainType;

    @FindBy(xpath = "//div[contains(@class,'ins-coupon-content-opened')]")
    private WebElementFacade couponContent;

    @FindBy(xpath = "//div[contains(@class,'search-page__input-search-change')]")
    private WebElementFacade btnGantiPencarian;

    @FindBy(xpath = "//div[@class='rail-search__container-dest--from']//input")
    private WebElementFacade inputLocationDeparture;

    @FindBy(xpath = "//div[@class='rail-search__container-dest--to']//input")
    private WebElementFacade inputLocationDestination;

    private static final String xpathDeptStation = "//div[@class='rail-search__container-dest--from']//p[@data-type='station'][@data-name='%s']/ancestor::li";

    private static final String xpathDestStation = "//div[@class='rail-search__container-dest--from']//p[@data-type='station'][@data-name='%s']/ancestor::li";

    private static final String xpathListCurrentDateTableDeparture = "(//table[@class='month1'])[3]//div[contains(@class,'day toMonth  valid')]";

    private static final String xpathListCurrentDateTableReturn = "(//table[@class='month1'])[4]//div[contains(@class,'day toMonth  valid')]";

    @FindBy(xpath = "//input[@id='search__dep-date']")
    private WebElementFacade btnDateDeparture;

    @FindBy(xpath = "//label[@for='train-return']")
    private WebElementFacade btnDateReturn;

    @FindBy(xpath = "(//table[@class='month2'])[1]//span[@class='next datepicker__next']")
    private WebElementFacade btnNextMonthDeparture;

    @FindBy(xpath = "(//table[@class='month1'])[3]//th[@class='month-name']")
    private WebElementFacade currentMonthTableDeparture;

    @FindBy(xpath = "(//table[@class='month2'])[2]//span[@class='next datepicker__next']")
    private WebElementFacade btnNextMonthReturn;

    @FindBy(xpath = "(//table[@class='month1'])[4]//th[@class='month-name']")
    private WebElementFacade currentMonthTableReturn;

    @FindBy(xpath = "//div[@class='rail-search__container-passenger']//input")
    private WebElementFacade btnPassenger;

    @FindBy(xpath = "//div[@class='passenger__value']//span[contains(@class,'adult')]")
    private WebElementFacade currentTotalAdultPassenger;

    @FindBy(xpath = "//div[@class='passenger__value']//span[contains(@class,'infant')]")
    private WebElementFacade currentTotalInfantPassenger;

    @FindBy(xpath = "(//a[@class='passenger__inc'])[3]")
    private WebElementFacade btnMinusAdultPassenger;

    @FindBy(xpath = "(//a[@class='passenger__desc'])[3]")
    private WebElementFacade btnPlusAdultPassenger;

    @FindBy(xpath = "(//a[@class='passenger__inc'])[4]")
    private WebElementFacade btnMinusInfantPassenger;

    @FindBy(xpath = "(//a[@class='passenger__desc'])[4]")
    private WebElementFacade btnPlusInfantPassenger;

    @FindBy(xpath = "//button[@class='rail-search__btn']")
    private WebElementFacade btnCari;

    @FindBy(xpath = "//button[@ng-click='chooseDeparture(rail)']")
    private List<WebElementFacade> listBtnChooseDeparture;

    @FindBy(xpath = "//button[@ng-click='chooseReturn(railReturn)']")
    private List<WebElementFacade> listBtnChooseReturn;

    @FindBy(xpath = "//tr[@ng-show='itineraryDeparture']")
    private WebElementFacade ticketSummaryDeparture;

    @FindBy(xpath = "//tr[@ng-show='itineraryReturn']")
    private WebElementFacade ticketSummaryReturn;

    @FindBy(xpath = "//button[@class='itinerary__btn-buy']")
    private WebElementFacade btnOrder;

    public boolean isDepartureStationVisible() {
        couponContent.withTimeoutOf(Duration.ofSeconds(7)).waitUntilNotVisible();
        loadingProgress.withTimeoutOf(Duration.ofSeconds(7)).waitUntilNotVisible();
        return departureStation.withTimeoutOf(Duration.ofSeconds(7)).waitUntilVisible().isVisible();
    }

    public String getDepartureStation() {
        return departureStation.getText();
    }

    public boolean isDestinationStationVisible() {
        return destinationStation.withTimeoutOf(Duration.ofSeconds(7)).waitUntilVisible().isVisible();
    }

    public String getDestinationStation() {
        return destinationStation.getText();
    }

    public List<String> getListDepartureStation() {
        List<String> departureStations = new ArrayList<>();
        for (WebElementFacade webElementFacade : listDepartureStation) {
            departureStations.add(webElementFacade.getText());
        }
        return departureStations;
    }

    public List<String> getListDestinationStation() {
        List<String> destinationStations = new ArrayList<>();
        for (WebElementFacade webElementFacade : listDestinationStation) {
            destinationStations.add(webElementFacade.getText());
        }
        return destinationStations;
    }

    public String getPassenger() {
        return passenger.getText();
    }

    public DesktopRailListPage filterDepartureTime(String timeDeparture) {
        for (WebElementFacade webElementFacade : listDepartureFilterTime) {
            if (!webElementFacade.getText().contains(timeDeparture)) {
                webElementFacade.click();
            }
        }
        return this;
    }

    public DesktopRailListPage filterReturnTime(String timeReturn) {
        for (WebElementFacade webElementFacade : listReturnFilterTime) {
            if (!webElementFacade.getText().contains(timeReturn)) {
                webElementFacade.click();
            }
        }
        return this;
    }

    public List<String> getListDepartureTime() {
        List<String> departureTime = new ArrayList<>();
        for (WebElementFacade webElementFacade : listDepartureTime) {
            departureTime.add(webElementFacade.getText());
        }
        return departureTime;
    }

    public List<String> getListReturnTime() {
        List<String> returnTime = new ArrayList<>();
        for (WebElementFacade webElementFacade : listReturnTime) {
            returnTime.add(webElementFacade.getText());
        }
        return returnTime;
    }

    public DesktopRailListPage filterDepartureClass(String classDeparture) {
        for (WebElementFacade webElementFacade : listDepartureFilterClass) {
            if (!webElementFacade.getText().equalsIgnoreCase(classDeparture)) {
                webElementFacade.click();
            }
        }
        return this;
    }

    public DesktopRailListPage filterReturnClass(String classReturn) {
        for (WebElementFacade webElementFacade : listReturnFilterClass) {
            if (!webElementFacade.getText().equalsIgnoreCase(classReturn)) {
                webElementFacade.click();
            }
        }
        return this;
    }

    public List<String> getListDepartureClass() {
        List<String> departureClass = new ArrayList<>();
        for (WebElementFacade webElementFacade : listDepartureClass) {
            departureClass.add(webElementFacade.getText());
        }
        return departureClass;
    }

    public List<String> getListReturnClass() {
        List<String> returnClass = new ArrayList<>();
        for (WebElementFacade webElementFacade : listReturnClass) {
            returnClass.add(webElementFacade.getText());
        }
        return returnClass;
    }

    public String getMaxPriceDeparture() {
        return departureMaxPrice.getAttribute("value").replaceFirst("Rp", "");
    }

    public String getMinPriceDeparture() {
        return departureMinPrice.getAttribute("value").replaceFirst("Rp", "");
    }

    public DesktopRailListPage filterDeparturePriceToMaxPrice() {
        Dimension sliderPriceSize = sliderPrice.getSize();
        withAction().click().dragAndDropBy(lowerSliderPrice, 0 + sliderPriceSize.getWidth(), 0).build().perform();
        return this;
    }

    public List<String> getListDeparturePrice() {
        List<String> departurePrice = new ArrayList<>();
        for (int i = 0; i < listDepartureMainPrice.size(); i++) {
            departurePrice.add(listDepartureMainPrice.get(i).getText() + listDepartureSecondaryPrice.get(i).getText());
        }
        return departurePrice;
    }

    public DesktopRailListPage filterDeparturePriceToMinPrice() {
        Dimension sliderPriceSize = sliderPrice.getSize();
        withAction().click().dragAndDropBy(upperSliderPrice, 0 - sliderPriceSize.getWidth(), 0).build().perform();
        return this;
    }

    public DesktopRailListPage clickDepartureTrainType() {
        scrollToElement(btnTrainTypeDeparture);
        btnTrainTypeDeparture.waitUntilClickable().click();
        return this;
    }

    public DesktopRailListPage clickReturnTrainType() {
        scrollToElement(btnTrainTypeReturn);
        btnTrainTypeReturn.waitUntilClickable().click();
        return this;
    }

    public DesktopRailListPage filterDepartureTrainType(String trainTypeDeparture) {
        for (WebElementFacade webElementFacade : listDepartureFilterTrainType) {
            if (!webElementFacade.getText().equalsIgnoreCase(trainTypeDeparture)) {
                webElementFacade.click();
            }
        }
        return this;
    }

    public DesktopRailListPage filterReturnTrainType(String trainTypeReturn) {
        for (WebElementFacade webElementFacade : listReturnFilterTrainType) {
            if (!webElementFacade.getText().equalsIgnoreCase(trainTypeReturn)) {
                webElementFacade.click();
            }
        }
        return this;
    }

    public List<String> getListDepartureTrainType() {
        List<String> departureTrainType = new ArrayList<>();
        for (WebElementFacade webElementFacade : listDepartureTrainType) {
            departureTrainType.add(webElementFacade.getText());
        }
        return departureTrainType;
    }

    public List<String> getListReturnTrainType() {
        List<String> returnTrainType = new ArrayList<>();
        for (WebElementFacade webElementFacade : listReturnTrainType) {
            returnTrainType.add(webElementFacade.getText());
        }
        return returnTrainType;
    }

    public DesktopRailListPage clickGantiPencarian() {
        btnGantiPencarian.click();
        return this;
    }

    public DesktopRailListPage clickDepartureLocation() {
        inputLocationDeparture.click();
        return this;
    }

    public DesktopRailListPage clickDestinationLocation() {
        inputLocationDestination.click();
        return this;
    }

    public DesktopRailListPage changeDepartureCity(String deptCity) {
        inputLocationDeparture.type(deptCity);
        return this;
    }

    public DesktopRailListPage changeDepartureStation(String deptStation) {
        waitABit(3000);
        String formattedDeptStation = WordUtils.capitalizeFully(deptStation);
        WebElementFacade departureStation = find(By.xpath(String.format(xpathDeptStation, formattedDeptStation)));
        if (departureStation.isVisible()) {
            departureStation.click();
        }
        return this;
    }

    public DesktopRailListPage changeDestinationCity(String destCity) {
        inputLocationDestination.type(destCity);
        return this;
    }

    public DesktopRailListPage changeDestinationStation(String destStation) {
        waitABit(3000);
        String formattedDestStation = WordUtils.capitalizeFully(destStation);
        WebElementFacade destinationStation = find(By.xpath(String.format(xpathDestStation, formattedDestStation)));
        if (destinationStation.isVisible()) {
            destinationStation.click();
        }
        return this;
    }

    public DesktopRailListPage clickDateDeparture() {
        btnDateDeparture.click();
        return this;
    }

    @SneakyThrows
    public DesktopRailListPage changeDateDeparture(String dateDeparture) {
        Locale locale = new Locale("in", "ID");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", locale);
        LocalDate localDateDeparture = LocalDate.parse(dateDeparture, formatter);
        while (!currentMonthTableDeparture.getText().contains(localDateDeparture.getMonth().getDisplayName(TextStyle.FULL, locale))) {
            btnNextMonthDeparture.click();
        }
        List<WebElementFacade> listCurrentDateTableDeparture = findAll(xpathListCurrentDateTableDeparture);
        for (WebElementFacade webElementFacade : listCurrentDateTableDeparture) {
            if (webElementFacade.getText().equals(String.valueOf(localDateDeparture.getDayOfMonth()))) {
                webElementFacade.click();
                break;
            }
        }
        return this;
    }

    public DesktopRailListPage clickDateReturn() {
        btnDateReturn.click();
        return this;
    }

    public DesktopRailListPage changeDateReturn(String dateReturn) {
        Locale locale = new Locale("in", "ID");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", locale);
        LocalDate localDateReturn = LocalDate.parse(dateReturn, formatter);
        while (!currentMonthTableReturn.getText().contains(localDateReturn.getMonth().getDisplayName(TextStyle.FULL, locale))) {
            btnNextMonthReturn.click();
        }
        List<WebElementFacade> listCurrentDateTableReturn = findAll(xpathListCurrentDateTableReturn);
        for (WebElementFacade webElementFacade : listCurrentDateTableReturn) {
            if (webElementFacade.getText().equals(String.valueOf(localDateReturn.getDayOfMonth()))) {
                webElementFacade.click();
                break;
            }
        }
        return this;
    }

    public DesktopRailListPage clickPassenger() {
        btnPassenger.click();
        return this;
    }

    public DesktopRailListPage changePassenger(int totalAdultPassenger, int totalInfantPassenger) {
        fillAdultPassenger(totalAdultPassenger);
        fillInfantPassenger(totalInfantPassenger);
        return this;
    }

    private void fillInfantPassenger(int totalInfantPassenger) {
        int totalInfant = Integer.parseInt(currentTotalInfantPassenger.getText());
        while (totalInfant != totalInfantPassenger) {
            if (totalInfant < totalInfantPassenger) {
                btnPlusInfantPassenger.click();
                totalInfant++;
            } else {
                btnMinusInfantPassenger.click();
                totalInfant--;
            }
        }
    }

    private void fillAdultPassenger(int totalAdultPassenger) {
        int totalAdult = Integer.parseInt(currentTotalAdultPassenger.getText());
        while (totalAdult != totalAdultPassenger) {
            if (totalAdult < totalAdultPassenger) {
                btnPlusAdultPassenger.click();
                totalAdult++;
            } else {
                btnMinusAdultPassenger.click();
                totalAdult--;
            }
        }
    }

    public DesktopRailListPage clickCari() {
        btnCari.click();
        return this;
    }

    public DesktopRailListPage clickChooseDepartureTicket() {
        scrollToElement(listBtnChooseDeparture.get(0));
        listBtnChooseDeparture.get(0).click();
        return this;
    }

    public DesktopRailListPage clickChooseReturnTicket() {
        scrollToElement(listBtnChooseReturn.get(0));
        listBtnChooseReturn.get(0).click();
        return this;
    }

    public boolean isTicketSummaryDepartureVisible() {
        return ticketSummaryDeparture.isVisible();
    }

    public boolean isTicketSummaryReturnVisible() {
        return ticketSummaryReturn.isVisible();
    }

    public void clickOrder() {
        btnOrder.click();
    }

    public List<String> getListFilterTimeDeparture() {
        List<String> listFilterTimeDeparture = new ArrayList<>();
        for (WebElementFacade webElementFacade : listDepartureFilterTime) {
            listFilterTimeDeparture.add(webElementFacade.getText());
        }
        return listFilterTimeDeparture;
    }

    public List<String> getListFilterClassDeparture() {
        List<String> listFilterClassDeparture = new ArrayList<>();
        for (WebElementFacade webElementFacade : listDepartureFilterClass) {
            listFilterClassDeparture.add(webElementFacade.getText());
        }
        return listFilterClassDeparture;
    }

    public List<String> getListFilterTrainTypeDeparture() {
        List<String> listFilterTrainTypeDeparture = new ArrayList<>();
        for (WebElementFacade webElementFacade : listDepartureFilterTrainType) {
            listFilterTrainTypeDeparture.add(webElementFacade.getText());
        }
        return listFilterTrainTypeDeparture;
    }

    public List<String> getListFilterTimeReturn() {
        List<String> listFilterTimeReturn = new ArrayList<>();
        for (WebElementFacade webElementFacade : listReturnFilterTime) {
            listFilterTimeReturn.add(webElementFacade.getText());
        }
        return listFilterTimeReturn;
    }

    public List<String> getListFilterClassReturn() {
        List<String> listFilterClassReturn = new ArrayList<>();
        for (WebElementFacade webElementFacade : listReturnFilterClass) {
            listFilterClassReturn.add(webElementFacade.getText());
        }
        return listFilterClassReturn;
    }

    public List<String> getListFilterTrainTypeReturn() {
        List<String> listFilterTrainTypeReturn = new ArrayList<>();
        for (WebElementFacade webElementFacade : listReturnFilterTrainType) {
            listFilterTrainTypeReturn.add(webElementFacade.getText());
        }
        return listFilterTrainTypeReturn;
    }
}
