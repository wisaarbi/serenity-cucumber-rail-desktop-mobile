package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("com.serenity.cucumber.rail.pages.android.AndroidRailListPage")
public class AndroidRailListPage extends MobileUtility {

    @FindBy(id = "blibli.mobile.commerce:id/tv_sub_title")
    private WebElementFacade departureStation;

    @FindBy(id = "blibli.mobile.commerce:id/tv_sub_title_two")
    private WebElementFacade destinationStation;

    @FindBy(id = "blibli.mobile.commerce:id/tv_passenger_count")
    private WebElementFacade totalPassenger;

    @FindBy(id = "blibli.mobile.commerce:id/tv_filter")
    private WebElementFacade btnFilter;

    @FindBy(id = "blibli.mobile.commerce:id/tv_start_time")
    private List<WebElementFacade> listDepartureTime;

    @FindBy(id = "blibli.mobile.commerce:id/tv_class_type")
    private List<WebElementFacade> listDepartureClass;

    @FindBy(id = "blibli.mobile.commerce:id/tv_class_type")
    private List<WebElementFacade> listReturnClass;

    @FindBy(xpath = "//android.view.ViewGroup[contains(@resource-id,'cl_parent')]//android.widget.TextView[contains(@resource-id,'tv_title')]")
    private List<WebElementFacade> listDepartureTrainType;

    @FindBy(xpath = "//android.view.ViewGroup[contains(@resource-id,'cl_parent')]//android.widget.TextView[contains(@resource-id,'tv_title')]")
    private List<WebElementFacade> listReturnTrainType;

    @FindBy(id = "blibli.mobile.commerce:id/tv_start_station_code")
    private List<WebElementFacade> listDepartureStation;

    @FindBy(id = "blibli.mobile.commerce:id/tv_end_station_code")
    private List<WebElementFacade> listDestinationStation;

    @FindBy(id = "blibli.mobile.commerce:id/tv_change")
    private WebElementFacade btnGantiPencarian;

    @FindBy(id = "blibli.mobile.commerce:id/bt_select_train")
    private List<WebElementFacade> listBtnChooseDeparture;

    @FindBy(id = "blibli.mobile.commerce:id/bt_select_train")
    private List<WebElementFacade> listBtnChooseReturn;

    @FindBy(id = "blibli.mobile.commerce:id/tv_going")
    private WebElementFacade ticketSummaryDeparture;

    @FindBy(id = "blibli.mobile.commerce:id/tv_price")
    private List<WebElementFacade> listDeparturePrice;

    public boolean isDepartureStationVisible() {
        return departureStation.isVisible();
    }

    public String getDepartureStation() {
        String deptStation = departureStation.getText()
                .substring(0, departureStation.getText().indexOf("(") - 1);
        return deptStation;
    }

    public boolean isDestinationStationVisible() {
        return destinationStation.isVisible();
    }

    public String getDestinationStation() {
        String destStation = destinationStation.getText()
                .substring(0, destinationStation.getText().indexOf("(") - 1);
        return destStation;
    }

    public boolean isTotalPassengerVisible() {
        return totalPassenger.isVisible();
    }

    public int getTotalPassenger() {
        return Integer.parseInt(totalPassenger.getText());
    }

    public AndroidRailListPage clickFilter() {
        btnFilter.click();
        return this;
    }

    public List<String> getListDepartureTime() {
        List<String> departureTime = new ArrayList<>();
        for (WebElementFacade webElementFacade : listDepartureTime) {
            departureTime.add(webElementFacade.getText());
        }
        return departureTime;
    }

    public List<String> getListDepartureClass() {
        List<String> departureClass = new ArrayList<>();
        for (WebElementFacade webElementFacade : listDepartureClass) {
            departureClass.add(webElementFacade.getText());
        }
        return departureClass;
    }

    public List<String> getListDepartureTrainType() {
        List<String> departureTrainType = new ArrayList<>();
        for (WebElementFacade webElementFacade : listDepartureTrainType) {
            departureTrainType.add(webElementFacade.getText());
        }
        return departureTrainType;
    }

    public void clickGantiPencarian() {
        btnGantiPencarian.click();
    }

    public boolean isListDepartureStationVisible() {
        if (listDepartureStation.isEmpty()) {
            return false;
        }
        return true;
    }

    public boolean isListDestinationStationVisible() {
        if (listDestinationStation.isEmpty()) {
            return false;
        }
        return true;
    }

    public AndroidRailListPage clickChooseDepartureTicket() {
        scrollToElement(listBtnChooseDeparture.get(0), 5);
        listBtnChooseDeparture.get(0).click();
        return this;
    }

    public boolean isTicketSummaryDepartureVisible() {
        return ticketSummaryDeparture.isVisible();
    }

    public void clickChooseReturnTicket() {
        scrollToElement(listBtnChooseReturn.get(0), 5);
        listBtnChooseReturn.get(0).click();
    }

    public List<String> getListReturnClass() {
        List<String> returnClass = new ArrayList<>();
        for (WebElementFacade webElementFacade : listReturnClass) {
            returnClass.add(webElementFacade.getText());
        }
        return returnClass;
    }

    public List<String> getListReturnTrainType() {
        List<String> returnTrainType = new ArrayList<>();
        for (WebElementFacade webElementFacade : listReturnTrainType) {
            returnTrainType.add(webElementFacade.getText());
        }
        return returnTrainType;
    }

    public List<String> getListDeparturePrice() {
        List<String> departurePrice = new ArrayList<>();
        for (WebElementFacade webElementFacade : listDeparturePrice) {
            departurePrice.add(webElementFacade.getText());
        }
        return departurePrice;
    }
}
