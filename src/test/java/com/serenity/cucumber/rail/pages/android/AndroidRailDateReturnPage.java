package com.serenity.cucumber.rail.pages.android;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.List;
import java.util.Locale;

@Component("com.serenity.cucumber.rail.pages.android.AndroidRailDateReturnPage")
public class AndroidRailDateReturnPage extends MobileUtility {

    @FindBy(xpath = "//android.widget.ImageView[@content-desc=\"Go to previous\"]/following-sibling::android.widget.TextView")
    private WebElementFacade currentMonth;

    @FindBy(xpath = "//android.widget.ImageView[@content-desc=\"Lanjut\"]")
    private WebElementFacade btnNextMonth;

    private static final String xpathListCurrentDateReturn = "//blibli.mobile.materialcalendarview.f[@content-desc=\"Calendar\"]//android.widget.CheckedTextView[@enabled='true']";

    public void fillDateReturn(String dateReturn) {
        Locale locale = new Locale("in", "ID");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", locale);
        LocalDate localDateReturn = LocalDate.parse(dateReturn, formatter);
        while (!currentMonth.getText().contains(localDateReturn.getMonth().getDisplayName(TextStyle.SHORT, locale))) {
            btnNextMonth.click();
        }
        List<WebElementFacade> listCurrentDateTableReturn = findAll(xpathListCurrentDateReturn);
        for (WebElementFacade webElementFacade : listCurrentDateTableReturn) {
            if (webElementFacade.getText().equals(String.valueOf(localDateReturn.getDayOfMonth()))) {
                webElementFacade.click();
                break;
            }
        }
    }
}
