package com.serenity.cucumber.rail.steps;

import com.serenity.cucumber.rail.pages.android.AndroidRailThankYouPage;
import com.serenity.cucumber.rail.pages.desktop.DesktopRailThankYouPage;
import cucumber.api.java.en.Then;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RailThankYouSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    DesktopRailThankYouPage desktopRailThankYouPage;

    @Autowired
    AndroidRailThankYouPage androidRailThankYouPage;

    @Then("^\\[desktop-rail-thankyou-page\\] user should see thank you section$")
    public void desktopRailThankyouPageUserShouldSeeThankYouSection() {
        assertThat("thankyou section is not visible", desktopRailThankYouPage.isThankYouSectionVisible(),
                equalTo(true));
    }

    @Then("^\\[android-rail-thankyou-page\\] user should see thank you section$")
    public void androidRailThankyouPageUserShouldSeeThankYouSection() {
        assertThat("thankyou section is not visible", androidRailThankYouPage.isThankYouSectionVisible(),
                equalTo(true));

    }
}
