package com.serenity.cucumber.rail.steps;

import com.serenity.cucumber.rail.controller.RailAvailabilityController;
import com.serenity.cucumber.rail.controller.SearchPopularStationController;
import com.serenity.cucumber.rail.data.*;
import com.serenity.cucumber.rail.pages.android.*;
import com.serenity.cucumber.rail.pages.desktop.DesktopRailListPage;
import com.serenity.cucumber.rail.pages.desktop.DesktopRailSearchPage;
import com.serenity.cucumber.rail.properties.RailAPIProperties;
import com.serenity.cucumber.rail.properties.RailProperties;
import com.serenity.cucumber.rail.responses.railavailability.getrailticketavailability.DepartureRailListItem;
import com.serenity.cucumber.rail.responses.railavailability.getsearchpopularstation.CityListItem;
import com.serenity.cucumber.rail.utility.DateUtility;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class RailSearchTicketSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    DesktopRailSearchPage desktopRailSearchPage;

    @Autowired
    DesktopRailListPage desktopRailListPage;

    @Autowired
    RailProperties railProperties;

    @Autowired
    RailData railData;

    @Autowired
    RailAPIProperties railAPIProperties;

    @Autowired
    RailAPIData railAPIData;

    @Autowired
    RailAvailabilityController railAvailabilityController;

    @Autowired
    SearchPopularStationController searchPopularStationController;

    @Autowired
    DateUtility dateUtility;

    @Autowired
    AndroidHomePage androidHomePage;

    @Autowired
    AndroidAllCategoryPage androidAllCategoryPage;

    @Autowired
    AndroidRailSearchPage androidRailSearchPage;

    @Autowired
    AndroidRailDepartureLocationPage androidRailDepartureLocationPage;

    @Autowired
    AndroidRailDestinationLocationPage androidRailDestinationLocationPage;

    @Autowired
    AndroidRailDateDeparturePage androidRailDateDeparturePage;

    @Autowired
    AndroidRailDateReturnPage androidRailDateReturnPage;

    @Autowired
    AndroidRailPassengerPage androidRailPassengerPage;

    @Autowired
    APIData apiData;

    @Autowired
    DesktopData desktopData;

    @Autowired
    MobileData mobileData;

    @When("^\\[desktop-rail-search-page\\] user fill departure city '(.*)'$")
    public void desktopRailSearchPageUserFillDepartureCity(String deptCity) {
        switch (deptCity) {
            case "validDeptCity":
                railData.setDeptCity(railProperties.get("validDeptCity"));
                break;
            case "invalidDeptCity":
                railData.setDeptCity(railProperties.get("invalidDeptCity"));
                break;
            default:
                railData.setDeptCity(deptCity);
                break;
        }
        desktopRailSearchPage.fillDepartureCity(railData.getDeptCity());
    }

    @And("^\\[desktop-rail-search-page\\] user fill departure station '(.*)'$")
    public void desktopRailSearchPageUserFillDepartureStation(String deptStation) {
        switch (deptStation) {
            case "validDeptStation":
                railData.setDeptStation(railProperties.get("validDeptStation"));
                break;
            default:
                railData.setDeptStation(deptStation);
                break;
        }
        desktopRailSearchPage.fillDepartureStation(railData.getDeptStation());
    }

    @And("^\\[desktop-rail-search-page\\] user fill destination city '(.*)'$")
    public void desktopRailSearchPageUserFillDestinationCity(String destCity) {
        switch (destCity) {
            case "validDestCity":
                railData.setDestCity(railProperties.get("validDestCity"));
                break;
            case "invalidDestCity":
                railData.setDestCity(railProperties.get("invalidDestCity"));
                break;
            default:
                railData.setDestCity(destCity);
                break;
        }
        desktopRailSearchPage.fillDestinationCity(railData.getDestCity());
    }

    @And("^\\[desktop-rail-search-page\\] user fill destination station '(.*)'$")
    public void desktopRailSearchPageUserFillDestinationStation(String destStation) {
        switch (destStation) {
            case "validDestStation":
                railData.setDestStation(railProperties.get("validDestStation"));
                break;
            default:
                railData.setDestStation(destStation);
                break;
        }
        desktopRailSearchPage.fillDestinationStation(railData.getDestStation());
    }

    @And("^\\[desktop-rail-search-page\\] user fill passenger$")
    public void desktopRailSearchPageUserFillPassenger(DataTable passenger) {
        List<Map<String, String>> listTotalPassenger = passenger.asMaps(String.class, String.class);
        String totalAdultPassenger = listTotalPassenger.get(0).get("totalAdultPassenger");
        String totalInfantPassenger = listTotalPassenger.get(0).get("totalInfantPassenger");
        switch (totalAdultPassenger) {
            case "validTotalAdultPassenger":
                railData.setTotalAdultPassenger(Integer.parseInt(railProperties.get("validTotalAdultPassenger")));
                break;
            default:
                railData.setTotalAdultPassenger(Integer.parseInt(totalAdultPassenger));
                break;
        }
        switch (totalInfantPassenger) {
            case "validTotalInfantPassenger":
                railData.setTotalInfantPassenger(Integer.parseInt(railProperties.get("validTotalInfantPassenger")));
                break;
            default:
                railData.setTotalInfantPassenger(Integer.parseInt(totalInfantPassenger));
                break;
        }
        desktopRailSearchPage.clickPassenger()
                .fillPassenger(railData.getTotalAdultPassenger(), railData.getTotalInfantPassenger());
    }

    @And("^\\[desktop-rail-search-page\\] user click cari$")
    public void desktopRailSearchPageUserClickCari() {
        desktopRailSearchPage.clickCari();
    }

    @Then("^\\[desktop-rail-search-page\\] user should see empty station error message '(.*)'$")
    public void desktopRailSearchPageUserShouldSeeEmptyStationErrorMessage(String emptyStationErrorMessage) {
        assertThat("empty station error message is not visible",
                desktopRailSearchPage.isEmptyStationErrorMessageVisible(), equalTo(true));
        assertThat("empty station error message is different with expected",
                desktopRailSearchPage.getEmptyStationErrorMessage(), equalTo(emptyStationErrorMessage));
    }

    @When("^\\[desktop-rail-search-page\\] user click departure location$")
    public void desktopRailSearchPageUserClickDepartureLocation() {
        desktopRailSearchPage.clickDepartureLocation();
    }

    @Then("^\\[desktop-rail-search-page\\] user should see list popular city$")
    public void desktopRailSearchPageUserShouldSeeListPopularCity() {
        assertThat("list popular city is not visible",
                desktopRailSearchPage.isListPopularCityVisible(), equalTo(true));
    }

    @And("^\\[desktop-rail-search-page\\] user click destination location$")
    public void desktopRailSearchPageUserClickDestinationLocation() {
        desktopRailSearchPage.clickDestinationLocation();
    }

    @Then("^\\[desktop-rail-list-page\\] user should see route departure station$")
    public void desktopRailListPageUserShouldSeeRouteDepartureStation() {
        assertThat("departure station is not visible",
                desktopRailListPage.isDepartureStationVisible(), equalTo(true));
        assertThat("departure station is different with expected",
                desktopRailListPage.getDepartureStation(), equalTo(railData.getDeptStation()));
    }

    @And("^\\[desktop-rail-list-page\\] user should see route destination station$")
    public void desktopRailListPageUserShouldSeeRouteDestinationStation() {
        assertThat("destination station is not visible",
                desktopRailListPage.isDestinationStationVisible(), equalTo(true));
        assertThat("destination station is different with expected",
                desktopRailListPage.getDestinationStation(), equalTo(railData.getDestStation()));
    }

    @And("^\\[desktop-rail-list-page\\] user should see list departure station$")
    public void desktopRailListPageUserShouldSeeListDepartureStation() {
        assertThat("list departure station is different with expected",
                desktopRailListPage.getListDepartureStation(), everyItem(equalTo(railData.getDeptStation())));
    }

    @And("^\\[desktop-rail-list-page\\] user should see list destination station$")
    public void desktopRailListPageUserShouldSeeListDestinationStation() {
        assertThat("list destination station is different with expected",
                desktopRailListPage.getListDestinationStation(), everyItem(equalTo(railData.getDestStation())));
    }

    @And("^\\[desktop-rail-list-page\\] user should see total passenger$")
    public void desktopRailListPageUserShouldSeeTotalPassenger() {
        String[] passenger = desktopRailListPage.getPassenger().split(", ");
        assertThat("adult passenger is different with expected",
                passenger[0], containsString(String.valueOf(railData.getTotalAdultPassenger())));
        if (railData.getTotalInfantPassenger() > 0) {
            assertThat("infant passenger is different with expected",
                    passenger[1], containsString(String.valueOf(railData.getTotalInfantPassenger())));
        }
    }

    @Then("^\\[desktop-rail-search-page\\] user should see total passenger error message '(.*)'$")
    public void desktopRailSearchPageUserShouldSeeTotalPassengerErrorMessage(String totalPassengerErrorMessage) {
        assertThat("total passenger error message is not visible",
                desktopRailSearchPage.isTotalPassengerErrorMessageVisible(), equalTo(true));
        assertThat("total passenger error message is different with expected",
                desktopRailSearchPage.getTotalPassengerErrorMessage(), equalTo(totalPassengerErrorMessage));
    }

    @Then("^\\[desktop-rail-search-page\\] user should see search error message$")
    public void desktopRailSearchPageUserShouldSeeSearchErrorMessage() {
        assertThat("search error message is not visible",
                desktopRailSearchPage.isSearchErrorMessageVisible(), equalTo(true));
    }

    @When("^\\[rail-search-api\\] user send get rail ticket availability request$")
    public void railSearchApiUserSendGetRailTicketAvailabilityRequest() {
        railAPIData.setResponseGetRailAvailability(railAvailabilityController.getRailAvailability());
    }

    @Then("^\\[rail-search-api\\] user should see get rail ticket availability response with status code (\\d+)$")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithStatusCode(int statusCode) {
        assertThat("status code is different with expected",
                railAPIData.getResponseGetRailAvailability().getCode(), equalTo(statusCode));
    }

    @And("^\\[rail-search-api\\] user should see get rail ticket availability response with origin code is same$")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithOriginCodeIsSame() {
        List<DepartureRailListItem> listDepartureRail = railAPIData.getResponseGetRailAvailability().getValue().getDepartureRailList();
        List<String> originCode = new ArrayList<>();
        switch (railAPIData.getOriginType()) {
            case "STATION":
                for (DepartureRailListItem departureRailListItem : listDepartureRail)
                    originCode.add(departureRailListItem.getOrigin().getStationCode());
                break;
            case "CITY":
                for (DepartureRailListItem departureRailListItem : listDepartureRail)
                    originCode.add(departureRailListItem.getOrigin().getCity().getCityCode());
                break;
            default:
                break;
        }
        assertThat("origin code is different with expected", originCode, everyItem(equalTo(railAPIData.getOriginCode())));
    }

    @And("^\\[rail-search-api\\] user should see get rail ticket availability response with destination code is same$")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithDestinationCodeIsSame() {
        List<DepartureRailListItem> listDepartureRail = railAPIData.getResponseGetRailAvailability().getValue().getDepartureRailList();
        List<String> destinationCode = new ArrayList<>();
        switch (railAPIData.getDestinationType()) {
            case "STATION":
                for (DepartureRailListItem departureRailListItem : listDepartureRail)
                    destinationCode.add(departureRailListItem.getDestination().getStationCode());
                break;
            case "CITY":
                for (DepartureRailListItem departureRailListItem : listDepartureRail)
                    destinationCode.add(departureRailListItem.getDestination().getCity().getCityCode());
                break;
            default:
                break;
        }
        assertThat("destination code is different with expected", destinationCode, everyItem(equalTo(railAPIData.getDestinationCode())));
    }

    @And("^\\[rail-search-api\\] user should see get rail ticket availability response with departure date is same$")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithDepartureDateIsSame() {
        List<DepartureRailListItem> listDepartureRail = railAPIData.getResponseGetRailAvailability().getValue().getDepartureRailList();
        List<String> departureDate = new ArrayList<>();
        for (DepartureRailListItem departureRailListItem : listDepartureRail)
            departureDate.add(departureRailListItem.getDepartureDate());
        assertThat("departure date is different with expected", departureDate, everyItem(equalTo(railAPIData.getDepartureDate())));
    }

    @Given("^\\[rail-search-api\\] user prepare data get rail ticket availability with origin code '(.*)'$")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithOriginCode(String originCode) {
        railAPIData.setOriginCode(originCode);
    }

    @And("^\\[rail-search-api\\] user prepare data get rail ticket availability with origin type '(.*)'$")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithOriginType(String originType) {
        railAPIData.setOriginType(originType);
    }

    @And("^\\[rail-search-api\\] user prepare data get rail ticket availability with destination code '(.*)'$")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithDestinationCode(String destinationCode) {
        railAPIData.setDestinationCode(destinationCode);
    }

    @And("^\\[rail-search-api\\] user prepare data get rail ticket availability with destination type '(.*)'$")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithDestinationType(String destinationType) {
        railAPIData.setDestinationType(destinationType);
    }

    @And("^\\[rail-search-api\\] user prepare data get rail ticket availability with date departure '(.*)'$")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithDateDeparture(String dateDeparture) {
        if (dateDeparture.contains("day plus")) {
            int dayPlus = Integer.parseInt(dateDeparture.replace("day plus ", ""));
            Date currentDatePlusDay = dateUtility.getCurrentDateWithDayPlus(dayPlus);
            String dateFormat = "yyyy-MM-dd";
            dateDeparture = dateUtility.convertDateToString(currentDatePlusDay, dateFormat);
        }
        railAPIData.setDepartureDate(dateDeparture);
    }

    @And("^\\[rail-search-api\\] user prepare data get rail ticket availability with date return '(.*)'$")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithDateReturn(String dateReturn) {
        switch (dateReturn) {
            case "validDateReturn":
                railAPIData.setReturnDate(railAPIProperties.get("validDateReturn"));
                break;
            default:
                railAPIData.setReturnDate(dateReturn);
                break;
        }
    }

    @And("^\\[rail-search-api\\] user prepare data get rail ticket availability with total adult passenger (\\d+)$")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithTotalAdultPassenger(int totalAdult) {
        railAPIData.setAdult(totalAdult);
    }

    @And("^\\[rail-search-api\\] user prepare data get rail ticket availability with total infant passenger (\\d+)$")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithTotalInfantPassenger(int totalInfant) {
        railAPIData.setInfant(totalInfant);
    }

    @And("^\\[rail-search-api\\] user should see get rail ticket availability response with total adult is same$")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithTotalAdultIsSame() {
        List<DepartureRailListItem> listDepartureRail = railAPIData.getResponseGetRailAvailability().getValue().getDepartureRailList();
        List<Integer> totalAdult = new ArrayList<>();
        for (DepartureRailListItem departureRailListItem : listDepartureRail)
            totalAdult.add(departureRailListItem.getAdult());
        assertThat("total adult is different with expected", totalAdult, everyItem(equalTo(railAPIData.getAdult())));
    }

    @And("^\\[rail-search-api\\] user should see get rail ticket availability response with total infant is same$")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithTotalInfantIsSame() {
        List<DepartureRailListItem> listDepartureRail = railAPIData.getResponseGetRailAvailability().getValue().getDepartureRailList();
        List<Integer> totalInfant = new ArrayList<>();
        for (DepartureRailListItem departureRailListItem : listDepartureRail)
            totalInfant.add(departureRailListItem.getInfant());
        assertThat("total infant is different with expected", totalInfant, everyItem(equalTo(railAPIData.getInfant())));
    }

    @And("^\\[rail-search-api\\] user should see get rail ticket availability response with message '(.*)'$")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithMessage(String message) {
        assertThat("message is different with expected",
                railAPIData.getResponseGetRailAvailability().getMessage(), equalTo(message));
    }

    @Given("^\\[rail-search-api\\] user get search popular station$")
    public void railSearchApiUserGetSearchPopularStation() {
        railAPIData.setResponseGetSearchPopularStation(searchPopularStationController.getSearchPopularStation());
    }

    @Then("^\\[rail-search-api\\] user should see get search popular station response with status code (\\d+)$")
    public void railSearchApiUserShouldSeeGetSearchPopularStationResponseWithStatusCode(int statusCode) {
        assertThat("status code is different with expected", railAPIData.getResponseGetSearchPopularStation().getCode(),
                equalTo(statusCode));
    }

    @And("^\\[rail-search-api\\] user should see get search popular station response with city name$")
    public void railSearchApiUserShouldSeeGetSearchPopularStationResponseWithCityName(DataTable dt) {
        List<Map<String, String>> maps = dt.asMaps(String.class, String.class);
        List<String> expectedData = new ArrayList<>();
        for (Map<String, String> data : maps) {
            expectedData.add(data.get("cityName"));
        }

        List<String> actualData = new ArrayList<>();
        List<CityListItem> cityList = railAPIData.getResponseGetSearchPopularStation().getValue().getCityList();
        for (CityListItem data : cityList) {
            actualData.add(data.getCityName());
        }

        assertThat("city name is different with expected", actualData, equalTo(expectedData));
    }

    @And("^\\[desktop-rail-search-page\\] user fill date of departure with day plus '(\\d+)'$")
    public void desktopRailSearchPageUserFillDateOfDepartureWithDayPlus(int dayPlus) {
        Date currentDatePlusDay = dateUtility.getCurrentDateWithDayPlus(dayPlus);
        String dateFormat = "dd-MM-yyyy";
        String date = dateUtility.convertDateToString(currentDatePlusDay, dateFormat);
        railData.setDateDeparture(date);
        desktopRailSearchPage.clickDateDeparture().fillDateDeparture(railData.getDateDeparture());
    }

    @And("^\\[desktop-rail-search-page\\] user fill date of return with day plus '(\\d+)'$")
    public void desktopRailSearchPageUserFillDateOfReturnWithDayPlus(int dayPlus) {
        Date currentDatePlusDay = dateUtility.getCurrentDateWithDayPlus(dayPlus);
        String dateFormat = "dd-MM-yyyy";
        String date = dateUtility.convertDateToString(currentDatePlusDay, dateFormat);
        railData.setDateReturn(date);
        desktopRailSearchPage.clickDateReturn().fillDateReturn(railData.getDateReturn());
    }

    @And("^\\[android-home-page\\] user click all category$")
    public void androidHomePageUserClickAllCategory() {
        androidHomePage.clickAllCategory();
    }

    @And("^\\[android-all-category-page\\] user click kereta$")
    public void androidAllCategoryPageUserClickKereta() {
        androidAllCategoryPage.clickKereta();
    }

    @Then("^\\[android-rail-search-page\\] user should see rail search form$")
    public void androidRailSearchPageUserShouldSeeRailSearchForm() {
        assertThat("rail search form is not visible", androidRailSearchPage.isRailSearchFormVisible(),
                equalTo(true));
    }

    @When("^\\[android-rail-search-page\\] user click departure location$")
    public void androidRailSearchPageUserClickDepartureLocation() {
        androidRailSearchPage.clickDepartureLocation();
    }

    @Then("^\\[android-rail-departure-location-page\\] user should see list popular city$")
    public void androidRailDepartureLocationPageUserShouldSeeListPopularCity() {
        assertThat("list popular city is not visible", androidRailDepartureLocationPage.isListPopularCityVisible(),
                equalTo(true));
    }

    @When("^\\[android-rail-departure-location-page\\] user click back button$")
    public void androidRailDepartureLocationPageUserClickBackButton() {
        androidRailDepartureLocationPage.clickBackButton();
    }

    @And("^\\[android-rail-search-page\\] user click destination location$")
    public void androidRailSearchPageUserClickDestinationLocation() {
        androidRailSearchPage.clickDestinationLocation();
    }

    @Then("^\\[android-rail-destination-location-page\\] user should see list popular city$")
    public void androidRailDestinationLocationPageUserShouldSeeListPopularCity() {
        assertThat("list popular city is not visible", androidRailDestinationLocationPage.isListPopularCityVisible(),
                equalTo(true));
    }

    @And("^\\[android-rail-departure-location-page\\] user fill departure city '(.*)'$")
    public void androidRailDepartureLocationPageUserFillDepartureCity(String deptCity) {
        switch (deptCity) {
            case "validDeptCity":
                railData.setDeptCity(railProperties.get("validDeptCity"));
                break;
            case "invalidDeptCity":
                railData.setDeptCity(railProperties.get("invalidDeptCity"));
                break;
            case "validChangeDeptCity":
                railData.setDeptCity(railProperties.get("validChangeDeptCity"));
                break;
            default:
                railData.setDeptCity(deptCity);
                break;
        }
        androidRailDepartureLocationPage.fillDepartureCity(railData.getDeptCity());
    }

    @And("^\\[android-rail-departure-location-page\\] user fill departure station '(.*)'$")
    public void androidRailDepartureLocationPageUserFillDepartureStation(String deptStation) {
        switch (deptStation) {
            case "validDeptStation":
                railData.setDeptStation(railProperties.get("validDeptStation"));
                break;
            case "validChangeDeptStation":
                railData.setDeptStation(railProperties.get("validChangeDeptStation"));
                break;
            default:
                railData.setDeptStation(deptStation);
                break;
        }
        androidRailDepartureLocationPage.fillDepartureStation(railData.getDeptStation());
    }

    @And("^\\[android-rail-destination-location-page\\] user fill destination city '(.*)'$")
    public void androidRailDestinationLocationPageUserFillDestinationCity(String destCity) {
        switch (destCity) {
            case "validDestCity":
                railData.setDestCity(railProperties.get("validDestCity"));
                break;
            case "invalidDestCity":
                railData.setDestCity(railProperties.get("invalidDestCity"));
                break;
            case "validChangeDestCity":
                railData.setDestCity(railProperties.get("validChangeDestCity"));
                break;
            default:
                railData.setDestCity(destCity);
                break;
        }
        androidRailDestinationLocationPage.fillDestinationCity(railData.getDestCity());
    }

    @And("^\\[android-rail-destination-location-page\\] user fill destination station '(.*)'$")
    public void androidRailDestinationLocationPageUserFillDestinationStation(String destStation) {
        switch (destStation) {
            case "validDestStation":
                railData.setDestStation(railProperties.get("validDestStation"));
                break;
            case "validChangeDestStation":
                railData.setDestStation(railProperties.get("validChangeDestStation"));
                break;
            default:
                railData.setDestStation(destStation);
                break;
        }
        androidRailDestinationLocationPage.fillDestinationStation(railData.getDestStation());
    }

    @And("^\\[android-rail-search-page\\] user fill date of departure with day plus '(\\d+)'$")
    public void androidRailSearchPageUserFillDateOfDepartureWithDayPlus(int dayPlus) {
        Date currentDatePlusDay = dateUtility.getCurrentDateWithDayPlus(dayPlus);
        String dateFormat = "dd-MM-yyyy";
        String date = dateUtility.convertDateToString(currentDatePlusDay, dateFormat);
        railData.setDateDeparture(date);
        androidRailSearchPage.clickDateDeparture();
        androidRailDateDeparturePage.fillDateDeparture(railData.getDateDeparture());
    }

    @And("^\\[android-rail-search-page\\] user fill passenger$")
    public void androidRailSearchPageUserFillPassenger(DataTable passenger) {
        List<Map<String, String>> listTotalPassenger = passenger.asMaps(String.class, String.class);
        String totalAdultPassenger = listTotalPassenger.get(0).get("totalAdultPassenger");
        String totalInfantPassenger = listTotalPassenger.get(0).get("totalInfantPassenger");
        switch (totalAdultPassenger) {
            case "validTotalAdultPassenger":
                railData.setTotalAdultPassenger(Integer.parseInt(railProperties.get("validTotalAdultPassenger")));
                break;
            default:
                railData.setTotalAdultPassenger(Integer.parseInt(totalAdultPassenger));
                break;
        }
        switch (totalInfantPassenger) {
            case "validTotalInfantPassenger":
                railData.setTotalInfantPassenger(Integer.parseInt(railProperties.get("validTotalInfantPassenger")));
                break;
            default:
                railData.setTotalInfantPassenger(Integer.parseInt(totalInfantPassenger));
                break;
        }
        androidRailSearchPage.clickPassenger();
        androidRailPassengerPage
                .fillPassenger(railData.getTotalAdultPassenger(), railData.getTotalInfantPassenger())
                .clickConfirm();
    }

    @And("^\\[android-rail-search-page\\] user click cari$")
    public void androidRailSearchPageUserClickCari() {
        androidRailSearchPage.clickCari();
    }

    @Then("^\\[android-rail-departure-location-page\\] user should see search error message$")
    public void androidRailDepartureLocationPageUserShouldSeeSearchErrorMessage() {
        assertThat("search error message is not visible",
                androidRailDepartureLocationPage.isSearchErrorMessageVisible(), equalTo(true));
    }

    @Then("^\\[android-rail-destination-location-page\\] user should see search error message$")
    public void androidRailDestinationLocationPageUserShouldSeeSearchErrorMessage() {
        assertThat("search error message is not visible",
                androidRailDestinationLocationPage.isSearchErrorMessageVisible(), equalTo(true));
    }

    @And("^\\[android-rail-search-page\\] user fill date of return with day plus '(\\d+)'$")
    public void androidRailSearchPageUserFillDateOfReturnWithDayPlus(int dayPlus) {
        Date currentDatePlusDay = dateUtility.getCurrentDateWithDayPlus(dayPlus);
        String dateFormat = "dd-MM-yyyy";
        String date = dateUtility.convertDateToString(currentDatePlusDay, dateFormat);
        railData.setDateReturn(date);
        androidRailSearchPage.clickRoundTrip()
                .clickDateReturn();
        androidRailDateReturnPage.fillDateReturn(railData.getDateReturn());
    }

    @And("^\\[android-rail-search-page\\] user click swap station$")
    public void androidRailSearchPageUserClickSwapStation() {
        androidRailSearchPage.clickSwapStation();
    }

    @Then("^\\[android-rail-search-page\\] user should see departure station has been swapped$")
    public void androidRailSearchPageUserShouldSeeDepartureStationHasBeenSwapped() {
        assertThat("departure station is different with expected", androidRailSearchPage.getDepartureStation(),
                containsString(railData.getDestStation()));
    }

    @And("^\\[android-rail-search-page\\] user should see destination station has been swapped$")
    public void androidRailSearchPageUserShouldSeeDestinationStationHasBeenSwapped() {
        assertThat("destination station is different with expected", androidRailSearchPage.getDestinationStation(),
                containsString(railData.getDeptStation()));
    }

    @Then("^\\[rail-search-api\\] user save popular city name to API data$")
    public void railSearchApiUserSavePopularCityNameToAPIData() {
        List<String> listCityName = new ArrayList<>();
        List<CityListItem> cityList = railAPIData.getResponseGetSearchPopularStation().getValue().getCityList();
        for (CityListItem data : cityList) {
            listCityName.add(data.getCityName());
        }
        apiData.setPopularCityName(listCityName);
    }

    @Then("^\\[rail-search-api\\] user save popular city code to API data$")
    public void railSearchApiUserSavePopularCityCodeToAPIData() {
        List<String> listCityCode = new ArrayList<>();
        List<CityListItem> cityList = railAPIData.getResponseGetSearchPopularStation().getValue().getCityList();
        for (CityListItem data : cityList) {
            listCityCode.add(data.getCityCode());
        }
        apiData.setPopularCityCode(listCityCode);
    }

    @Then("^\\[desktop-rail-search-page\\] user get list popular city$")
    public void desktopRailSearchPageUserGetListPopularCity() {
        desktopData.setPopularCityName(desktopRailSearchPage.getListPopularCity());
    }

    @Then("^\\[android-rail-departure-location-page\\] user get list popular city$")
    public void androidRailDepartureLocationPageUserGetListPopularCity() {
        mobileData.setPopularCityName(androidRailDepartureLocationPage.getListPopularCity());
    }
}
