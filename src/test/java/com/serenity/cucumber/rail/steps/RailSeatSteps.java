package com.serenity.cucumber.rail.steps;

import com.serenity.cucumber.rail.data.RailData;
import com.serenity.cucumber.rail.pages.android.AndroidRailSeatPage;
import com.serenity.cucumber.rail.pages.desktop.DesktopRailSeatPage;
import cucumber.api.java.en.And;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

public class RailSeatSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    DesktopRailSeatPage desktopRailSeatPage;

    @Autowired
    AndroidRailSeatPage androidRailSeatPage;

    @Autowired
    RailData railData;

    @And("^\\[desktop-rail-seat-page\\] user choose seat$")
    public void desktopRailSeatPageUserChooseSeat() {
        for (int i = 0; i < railData.getTotalAdultPassenger(); i++) {
            desktopRailSeatPage.choosePassenger(i)
                    .chooseCarriage()
                    .chooseSeat();
        }
        railData.setCarriageDeparture(desktopRailSeatPage.getSelectedCarriage());
        railData.setSeatDeparture(desktopRailSeatPage.getSelectedSeat());
        if (railData.getDateReturn() != null) {
            desktopRailSeatPage.clickSelectReturnSeat();
            for (int i = 0; i < railData.getTotalAdultPassenger(); i++) {
                desktopRailSeatPage.choosePassenger(i)
                        .chooseCarriage()
                        .chooseSeat();
            }
            railData.setCarriageReturn(desktopRailSeatPage.getSelectedCarriage());
            railData.setSeatReturn(desktopRailSeatPage.getSelectedSeat());
        }
    }

    @And("^\\[desktop-rail-seat-page\\] user click proceed to payment$")
    public void desktopRailSeatPageUserClickProceedToPayment() {
        desktopRailSeatPage.clickProceedToPayment();
    }

    @And("^\\[android-rail-seat-page\\] user choose seat$")
    public void androidRailSeatPageUserChooseSeat() {
        for (int i = 0; i < railData.getTotalAdultPassenger(); i++) {
            androidRailSeatPage.choosePassenger(i)
                    .chooseCarriage()
                    .chooseSeat();
        }
        railData.setCarriageDeparture(androidRailSeatPage.getSelectedCarriage());
        railData.setSeatDeparture(androidRailSeatPage.getSelectedSeat());
        if (railData.getDateReturn() != null) {
            androidRailSeatPage.changeReturnSeat();
            for (int i = 0; i < railData.getTotalAdultPassenger(); i++) {
                androidRailSeatPage.choosePassenger(i)
                        .chooseCarriage()
                        .chooseSeat();
            }
            railData.setCarriageReturn(androidRailSeatPage.getSelectedCarriage());
            railData.setSeatReturn(androidRailSeatPage.getSelectedSeat());
        }
    }

    @And("^\\[android-rail-seat-page\\] user click proceed to payment$")
    public void androidRailSeatPageUserClickProceedToPayment() {
        androidRailSeatPage.clickProceedToPayment();
    }
}
