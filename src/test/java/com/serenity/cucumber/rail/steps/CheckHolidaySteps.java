package com.serenity.cucumber.rail.steps;

import com.serenity.cucumber.rail.controller.CheckHolidayController;
import com.serenity.cucumber.rail.data.CheckHolidayAPIData;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CheckHolidaySteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    CheckHolidayAPIData checkHolidayAPIData;

    @Autowired
    CheckHolidayController checkHolidayController;

    @Given("^\\[check-holiday-api\\] user prepare data get check holiday with year start (\\d+)$")
    public void checkHolidayApiUserPrepareDataGetCheckHolidayWithYearStart(int yearStart) {
        checkHolidayAPIData.setYearStart(yearStart);
    }

    @And("^\\[check-holiday-api\\] user prepare data get check holiday with month start (\\d+)$")
    public void checkHolidayApiUserPrepareDataGetCheckHolidayWithMonthStart(int monthStart) {
        checkHolidayAPIData.setMonthStart(monthStart);
    }

    @And("^\\[check-holiday-api\\] user prepare data get check holiday with year end (\\d+)$")
    public void checkHolidayApiUserPrepareDataGetCheckHolidayWithYearEnd(int yearEnd) {
        checkHolidayAPIData.setYearEnd(yearEnd);
    }

    @And("^\\[check-holiday-api\\] user prepare data get check holiday with month end (\\d+)$")
    public void checkHolidayApiUserPrepareDataGetCheckHolidayWithMonthEnd(int monthEnd) {
        checkHolidayAPIData.setMonthEnd(monthEnd);
    }

    @When("^\\[check-holiday-api\\] user get check holiday$")
    public void checkHolidayApiUserGetCheckHoliday() {
        checkHolidayAPIData.setResponseGetCheckHoliday(checkHolidayController.getCheckHoliday());
    }

    @Then("^\\[check-holiday-api\\] user should see get check holiday response with status code '(.*)'$")
    public void checkHolidayApiUserShouldSeeGetCheckHolidayResponseWithStatusCode(String statusCode) {
        assertThat("status code is different with expected", checkHolidayAPIData.getResponseGetCheckHoliday().getCode(),
                equalTo(statusCode));
    }

    @And("^\\[check-holiday-api\\] user should see get check holiday response with status message '(.*)'$")
    public void checkHolidayApiUserShouldSeeGetCheckHolidayResponseWithStatusMessage(String statusMessage) {
        assertThat("status message is different with expected", checkHolidayAPIData.getResponseGetCheckHoliday().getMessage(),
                equalTo(statusMessage));
    }
}
