package com.serenity.cucumber.rail.steps;

import com.serenity.cucumber.rail.data.RailData;
import com.serenity.cucumber.rail.pages.android.AndroidRailCheckoutPage;
import com.serenity.cucumber.rail.pages.android.AndroidRailOrderDetailsPage;
import com.serenity.cucumber.rail.pages.desktop.DesktopRailCheckoutPage;
import com.serenity.cucumber.rail.properties.RailProperties;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class RailCheckoutSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    RailProperties railProperties;

    @Autowired
    DesktopRailCheckoutPage desktopRailCheckoutPage;

    @Autowired
    AndroidRailCheckoutPage androidRailCheckoutPage;

    @Autowired
    AndroidRailOrderDetailsPage androidRailOrderDetailsPage;

    @Autowired
    RailData railData;

    @Then("^\\[desktop-rail-checkout-page\\] user should see order detail$")
    public void desktopRailCheckoutPageUserShouldSeeOrderDetail() {
        assertThat("order detail is not visible", desktopRailCheckoutPage.isOrderDetailVisible(), equalTo(true));
    }

    @And("^\\[desktop-rail-checkout-page\\] user should see origin station$")
    public void desktopRailCheckoutPageUserShouldSeeOriginStation() {
        assertThat("departure origin station is different with expected", desktopRailCheckoutPage.getDepartureOriginStation(),
                containsString(railData.getDeptStation()));
        if (railData.getDateReturn() != null) {
            assertThat("return origin station is different with expected", desktopRailCheckoutPage.getReturnOriginStation(),
                    containsString(railData.getDestStation()));
        }
    }

    @And("^\\[desktop-rail-checkout-page\\] user should see destination station$")
    public void desktopRailCheckoutPageUserShouldSeeDestinationStation() {
        assertThat("departure destination station is different with expected", desktopRailCheckoutPage.getDepartureDestinationStation(),
                containsString(railData.getDestStation()));
        if (railData.getDateReturn() != null) {
            assertThat("return destination station is different with expected", desktopRailCheckoutPage.getReturnDestinationStation(),
                    containsString(railData.getDeptStation()));
        }
    }

    @And("^\\[desktop-rail-checkout-page\\] user should see ticket price$")
    public void desktopRailCheckoutPageUserShouldSeeTicketPrice() {
        assertThat("departure ticket price is not visible", desktopRailCheckoutPage.isDepartureTicketPriceVisible(),
                equalTo(true));
        if (railData.getDateReturn() != null) {
            assertThat("return ticket price is not visible", desktopRailCheckoutPage.isReturnTicketPriceVisible(),
                    equalTo(true));
        }
    }

    @And("^\\[desktop-rail-checkout-page\\] user should see total ticket price$")
    public void desktopRailCheckoutPageUserShouldSeeTotalTicketPrice() {
        assertThat("total ticket price is not visible", desktopRailCheckoutPage.isTotalTicketPriceVisible(),
                equalTo(true));
    }

    @And("^\\[desktop-rail-checkout-page\\] user should see total passenger$")
    public void desktopRailCheckoutPageUserShouldSeeTotalPassenger() {
        String[] passennger = StringUtils.substringBetween(desktopRailCheckoutPage.getTotalPassenger(), "(", ")").split(", ");
        String actualAdultPassenger = passennger[0];
        String actualInfantPassenger = passennger[1];

        String expectedAdultPassenger = String.valueOf(railData.getTotalAdultPassenger());
        String expectedInfantPassenger = String.valueOf(railData.getTotalInfantPassenger());

        assertThat("total adult passenger is different with expected", actualAdultPassenger,
                containsString(expectedAdultPassenger));
        assertThat("total infant passenger is different with expected", actualInfantPassenger,
                containsString(expectedInfantPassenger));
    }

    @And("^\\[desktop-rail-checkout-page\\] user should see train class$")
    public void desktopRailCheckoutPageUserShouldSeeTrainClass() {
        assertThat("departure train class is different with expected", desktopRailCheckoutPage.getDepartureTrainClass(),
                containsString(railData.getClassDeparture()));
        if (railData.getDateReturn() != null) {
            assertThat("return train class is different with expected", desktopRailCheckoutPage.getReturnTrainClass(),
                    containsString(railData.getClassReturn()));
        }
    }

    @And("^\\[desktop-rail-checkout-page\\] user should see train type$")
    public void desktopRailCheckoutPageUserShouldSeeTrainType() {
        assertThat("departure train type is different with expected", desktopRailCheckoutPage.getDepartureTrainType(),
                containsString(railData.getTrainTypeDeparture()));
        if (railData.getDateReturn() != null) {
            assertThat("return train type is different with expected", desktopRailCheckoutPage.getReturnTrainType(),
                    containsString(railData.getTrainTypeReturn()));
        }
    }

    @And("^\\[desktop-rail-checkout-page\\] user should see date$")
    public void desktopRailCheckoutPageUserShouldSeeDate() {
        Locale locale = new Locale("in", "ID");
        DateTimeFormatter formatterDateDeparture = DateTimeFormatter.ofPattern("dd-MM-yyyy", locale);
        LocalDate localDateDeparture = LocalDate.parse(railData.getDateDeparture(), formatterDateDeparture);
        formatterDateDeparture = DateTimeFormatter.ofPattern("E, d MMM yyyy", locale);
        String expectedDateDeparture = localDateDeparture.format(formatterDateDeparture);

        if (expectedDateDeparture.contains("Agu")) {
            expectedDateDeparture = expectedDateDeparture.replace("Agu", "Ags");
        }

        assertThat("departure date is different with expected", desktopRailCheckoutPage.getDepartureDate(),
                equalTo(expectedDateDeparture));

        if (railData.getDateReturn() != null) {
            DateTimeFormatter formatterDateReturn = DateTimeFormatter.ofPattern("dd-MM-yyyy", locale);
            LocalDate localDateReturn = LocalDate.parse(railData.getDateReturn(), formatterDateReturn);
            formatterDateReturn = DateTimeFormatter.ofPattern("E, d MMM yyyy", locale);
            String expectedDateReturn = localDateReturn.format(formatterDateReturn);

            if (expectedDateReturn.contains("Agu")) {
                expectedDateReturn = expectedDateReturn.replace("Agu", "Ags");
            }

            assertThat("return date is different with expected", desktopRailCheckoutPage.getReturnDate(),
                    equalTo(expectedDateReturn));
        }
    }

    @When("^\\[desktop-rail-checkout-page\\] user fill customer title '(.*)'$")
    public void desktopRailCheckoutPageUserFillCustomerTitle(String customerTitle) {
        switch (customerTitle) {
            case "customerTitle":
                railData.setCustomerTitle(railProperties.get("customerTitle"));
                break;
            default:
                railData.setCustomerTitle(customerTitle);
                break;
        }
        desktopRailCheckoutPage.fillCustomerTitle(railData.getCustomerTitle());
    }

    @And("^\\[desktop-rail-checkout-page\\] user fill customer name '(.*)'$")
    public void desktopRailCheckoutPageUserFillCustomerName(String customerName) {
        switch (customerName) {
            case "validCustomerName":
                railData.setCustomerName(railProperties.get("validCustomerName"));
                break;
            default:
                railData.setCustomerName(customerName);
                break;
        }
        desktopRailCheckoutPage.fillCustomerName(railData.getCustomerName());
    }

    @And("^\\[desktop-rail-checkout-page\\] user fill customer phone number '(.*)'$")
    public void desktopRailCheckoutPageUserFillCustomerPhoneNumber(String customerPhoneNumber) {
        switch (customerPhoneNumber) {
            case "validCustomerPhoneNumber":
                railData.setCustomerPhoneNumber(railProperties.get("validCustomerPhoneNumber"));
                break;
            default:
                railData.setCustomerPhoneNumber(customerPhoneNumber);
                break;
        }
        desktopRailCheckoutPage.fillCustomerPhoneNumber(railData.getCustomerPhoneNumber());
    }

    @And("^\\[desktop-rail-checkout-page\\] user fill customer email '(.*)'$")
    public void desktopRailCheckoutPageUserFillCustomerEmail(String customerEmail) {
        switch (customerEmail) {
            case "validCustomerEmail":
                railData.setCustomerEmail(railProperties.get("validCustomerEmail"));
                break;
            default:
                railData.setCustomerEmail(customerEmail);
                break;
        }
        desktopRailCheckoutPage.fillCustomerEmail(railData.getCustomerEmail());
    }

    @And("^\\[desktop-rail-checkout-page\\] user click continue to fill in passenger data$")
    public void desktopRailCheckoutPageUserClickContinueToFillInPassengerData() {
        desktopRailCheckoutPage.clickContinueToFillInPassengerData();
    }

    @Then("^\\[desktop-rail-checkout-page\\] user should see customer title$")
    public void desktopRailCheckoutPageUserShouldSeeCustomerTitle() {
        assertThat("customer title is different with expected", desktopRailCheckoutPage.getCustomerTitle(),
                equalTo(railData.getCustomerTitle()));
    }

    @And("^\\[desktop-rail-checkout-page\\] user should see customer name$")
    public void desktopRailCheckoutPageUserShouldSeeCustomerName() {
        assertThat("customer name is different with expected", desktopRailCheckoutPage.getCustomerName(),
                equalTo(railData.getCustomerName()));
    }

    @And("^\\[desktop-rail-checkout-page\\] user should see customer phone number$")
    public void desktopRailCheckoutPageUserShouldSeeCustomerPhoneNumber() {
        assertThat("customer phone number is different with expected", desktopRailCheckoutPage.getCustomerPhoneNumber(),
                equalTo(railData.getCustomerPhoneNumber()));
    }

    @And("^\\[desktop-rail-checkout-page\\] user should see customer email$")
    public void desktopRailCheckoutPageUserShouldSeeCustomerEmail() {
        assertThat("customer email is different with expected", desktopRailCheckoutPage.getCustomerEmail(),
                equalTo(railData.getCustomerEmail()));
    }

    @Then("^\\[desktop-rail-checkout-page\\] user should see error message '(.*)' on customer section$")
    public void desktopRailCheckoutPageUserShouldSeeErrorMessageOnCustomerSection(String errorMessage) {
        assertThat("error message is different with expected", desktopRailCheckoutPage.getErrorMessageOnCustomerSection(),
                equalTo(errorMessage));
    }

    @When("^\\[desktop-rail-checkout-page\\] user click same as customer$")
    public void desktopRailCheckoutPageUserClickSameAsCustomer() {
        desktopRailCheckoutPage.clickSameAsCustomer();
    }

    @Then("^\\[desktop-rail-checkout-page\\] user should see title adult passenger is same as customer$")
    public void desktopRailCheckoutPageUserShouldSeeTitleAdultPassengerIsSameAsCustomer() {
        assertThat("title adult passenger is different with expected", desktopRailCheckoutPage.getTitlePassengerSameAsCustomer(),
                equalTo(railData.getCustomerTitle()));
    }

    @And("^\\[desktop-rail-checkout-page\\] user should see name adult passenger is same as customer$")
    public void desktopRailCheckoutPageUserShouldSeeNameAdultPassengerIsSameAsCustomer() {
        assertThat("name adult passenger is different with expected", desktopRailCheckoutPage.getNamePassengerSameAsCustomer(),
                equalTo(railData.getCustomerName()));
    }

    @When("^\\[desktop-rail-checkout-page\\] user fill adult passenger data$")
    public void desktopRailCheckoutPageUserFillAdultPassengerData() {
        desktopRailCheckoutPage.fillAdultPassenger(railData.getTotalAdultPassenger());
    }

    @Then("^\\[desktop-rail-checkout-page\\] user should see adult passenger data$")
    public void desktopRailCheckoutPageUserShouldSeeAdultPassengerData() {
        assertThat("adult passenger data is not visible", desktopRailCheckoutPage.isAdultPassengerDataVisible(),
                equalTo(true));
    }

    @When("^\\[desktop-rail-checkout-page\\] user fill infant passenger data$")
    public void desktopRailCheckoutPageUserFillInfantPassengerData() {
        desktopRailCheckoutPage.fillInfantPassenger(railData.getTotalInfantPassenger());
    }

    @Then("^\\[desktop-rail-checkout-page\\] user should see infant passenger data$")
    public void desktopRailCheckoutPageUserShouldSeeInfantPassengerData() {
        assertThat("infant passenger data is not visible", desktopRailCheckoutPage.isInfantPassengerDataVisible(),
                equalTo(true));
    }

    @When("^\\[desktop-rail-checkout-page\\] user click change seat$")
    public void desktopRailCheckoutPageUserClickChangeSeat() {
        desktopRailCheckoutPage.clickChangeSeat();
    }

    @When("^\\[desktop-rail-checkout-page\\] user click proceed to payment$")
    public void desktopRailCheckoutPageUserClickProceedToPayment() {
        desktopRailCheckoutPage.clickProceedToPayment();
    }

    @Then("^\\[android-rail-checkout-page\\] user should see order details$")
    public void androidRailCheckoutPageUserShouldSeeOrderDetails() {
        assertThat("order details is not visible", androidRailCheckoutPage.isOrderDetailsVisible(), equalTo(true));
    }

    @When("^\\[android-rail-checkout-page\\] user click see order details$")
    public void androidRailCheckoutPageUserClickSeeOrderDetails() {
        androidRailCheckoutPage.clickSeeOrderDetails();
    }

    @Then("^\\[android-rail-order-details-page\\] user should see origin station$")
    public void androidRailOrderDetailsPageUserShouldSeeOriginStation() {
        assertThat("departure origin station is different with expected", androidRailOrderDetailsPage.getDepartureOriginStation(),
                containsString(railData.getDeptStation()));
        if (railData.getDateReturn() != null) {
            assertThat("return origin station is different with expected", androidRailOrderDetailsPage.getReturnOriginStation(),
                    containsString(railData.getDestStation()));
        }
    }

    @And("^\\[android-rail-order-details-page\\] user should see destination station$")
    public void androidRailOrderDetailsPageUserShouldSeeDestinationStation() {
        assertThat("departure destination station is different with expected", androidRailOrderDetailsPage.getDepartureDestinationStation(),
                containsString(railData.getDestStation()));
        if (railData.getDateReturn() != null) {
            assertThat("return destination station is different with expected", androidRailOrderDetailsPage.getReturnDestinationStation(),
                    containsString(railData.getDeptStation()));
        }
    }

    @And("^\\[android-rail-order-details-page\\] user should see total ticket price$")
    public void androidRailOrderDetailsPageUserShouldSeeTotalTicketPrice() {
        assertThat("total ticket price is not visible", androidRailOrderDetailsPage.isTotalTicketPriceVisible(),
                equalTo(true));
    }

    @And("^\\[android-rail-order-details-page\\] user should see ticket price$")
    public void androidRailOrderDetailsPageUserShouldSeeTicketPrice() {
        assertThat("departure ticket price is not visible", androidRailOrderDetailsPage.isDepartureTicketPriceVisible(),
                equalTo(true));
        if (railData.getDateReturn() != null) {
            assertThat("return ticket price is not visible", androidRailOrderDetailsPage.isReturnTicketPriceVisible(),
                    equalTo(true));
        }
    }

    @And("^\\[android-rail-order-details-page\\] user should see total passenger$")
    public void androidRailOrderDetailsPageUserShouldSeeTotalPassenger() {
        androidRailOrderDetailsPage.scrollToTotalTicketPrice();
        String actualAdultPassenger = androidRailOrderDetailsPage.getTotalAdultPassenger();
        String actualInfantPassenger = androidRailOrderDetailsPage.getTotalInfantPassenger();

        String expectedAdultPassenger = String.valueOf(railData.getTotalAdultPassenger());
        String expectedInfantPassenger = String.valueOf(railData.getTotalInfantPassenger());

        assertThat("total adult passenger is different with expected", actualAdultPassenger,
                equalTo(expectedAdultPassenger));
        assertThat("total infant passenger is different with expected", actualInfantPassenger,
                equalTo(expectedInfantPassenger));
    }

    @And("^\\[android-rail-order-details-page\\] user should see train class$")
    public void androidRailOrderDetailsPageUserShouldSeeTrainClass() {
        assertThat("departure train class is different with expected", androidRailOrderDetailsPage.getDepartureTrainClass(),
                containsString(railData.getClassDeparture()));
        if (railData.getDateReturn() != null) {
            assertThat("return train class is different with expected", androidRailOrderDetailsPage.getReturnTrainClass(),
                    containsString(railData.getClassReturn()));
        }
    }

    @And("^\\[android-rail-order-details-page\\] user should see train type$")
    public void androidRailOrderDetailsPageUserShouldSeeTrainType() {
        assertThat("departure train type is different with expected", androidRailOrderDetailsPage.getDepartureTrainType(),
                containsString(railData.getTrainTypeDeparture()));
        if (railData.getDateReturn() != null) {
            assertThat("return train type is different with expected", androidRailOrderDetailsPage.getReturnTrainType(),
                    containsString(railData.getTrainTypeReturn()));
        }
    }

    @When("^\\[android-rail-order-details-page\\] user close order details page$")
    public void androidRailOrderDetailsPageUserCloseOrderDetailsPage() {
        androidRailOrderDetailsPage.closeOrderDetailsPage();
    }

    @When("^\\[android-rail-checkout-page\\] user fill customer title '(.*)'$")
    public void androidRailCheckoutPageUserFillCustomerTitle(String customerTitle) {
        switch (customerTitle) {
            case "customerTitle":
                railData.setCustomerTitle(railProperties.get("customerTitle"));
                break;
            default:
                railData.setCustomerTitle(customerTitle);
                break;
        }
        androidRailCheckoutPage.fillCustomerTitle(railData.getCustomerTitle());
    }

    @And("^\\[android-rail-checkout-page\\] user fill customer name '(.*)'$")
    public void androidRailCheckoutPageUserFillCustomerName(String customerName) {
        switch (customerName) {
            case "validCustomerName":
                railData.setCustomerName(railProperties.get("validCustomerName"));
                break;
            default:
                railData.setCustomerName(customerName);
                break;
        }
        androidRailCheckoutPage.fillCustomerName(railData.getCustomerName());
    }

    @And("^\\[android-rail-checkout-page\\] user fill customer phone number '(.*)'$")
    public void androidRailCheckoutPageUserFillCustomerPhoneNumber(String customerPhoneNumber) {
        switch (customerPhoneNumber) {
            case "validCustomerPhoneNumber":
                railData.setCustomerPhoneNumber(railProperties.get("validCustomerPhoneNumber"));
                break;
            default:
                railData.setCustomerPhoneNumber(customerPhoneNumber);
                break;
        }
        androidRailCheckoutPage.fillCustomerPhoneNumber(railData.getCustomerPhoneNumber());
    }

    @And("^\\[android-rail-checkout-page\\] user fill customer email '(.*)'$")
    public void androidRailCheckoutPageUserFillCustomerEmail(String customerEmail) {
        switch (customerEmail) {
            case "validCustomerEmail":
                railData.setCustomerEmail(railProperties.get("validCustomerEmail"));
                break;
            default:
                railData.setCustomerEmail(customerEmail);
                break;
        }
        androidRailCheckoutPage.fillCustomerEmail(railData.getCustomerEmail());
    }

    @And("^\\[android-rail-checkout-page\\] user click continue to fill in passenger data$")
    public void androidRailCheckoutPageUserClickContinueToFillInPassengerData() {
        androidRailCheckoutPage.clickContinueToFillInPassengerData();
        railData.setProgressBarValue(0);
    }

    @Then("^\\[android-rail-checkout-page\\] user should see progress bar value is increased$")
    public void androidRailCheckoutPageUserShouldSeeProgressBarValueIsIncreased() {
        double currentProgressBarValue = Double.parseDouble(androidRailCheckoutPage.getValueProgressBar());
        assertThat("progress bar value is different with expected", androidRailCheckoutPage
                        .IsProgressBarValueIncreased(currentProgressBarValue, railData.getProgressBarValue()),
                equalTo(true));
        railData.setProgressBarValue(currentProgressBarValue);
    }

    @When("^\\[android-rail-checkout-page\\] user click same as customer$")
    public void androidRailCheckoutPageUserClickSameAsCustomer() {
        androidRailCheckoutPage.clickSameAsCustomer();
    }

    @Then("^\\[android-rail-checkout-page\\] user should see title adult passenger is same as customer$")
    public void androidRailCheckoutPageUserShouldSeeTitleAdultPassengerIsSameAsCustomer() {
        assertThat("title adult passenger is different with expected", androidRailCheckoutPage.getTitlePassengerSameAsCustomer(),
                equalTo(railData.getCustomerTitle()));
    }

    @And("^\\[android-rail-checkout-page\\] user should see name adult passenger is same as customer$")
    public void androidRailCheckoutPageUserShouldSeeNameAdultPassengerIsSameAsCustomer() {
        assertThat("name adult passenger is different with expected", androidRailCheckoutPage.getNamePassengerSameAsCustomer(),
                equalTo(railData.getCustomerName()));
    }

    @When("^\\[android-rail-checkout-page\\] user fill adult passenger data$")
    public void androidRailCheckoutPageUserFillAdultPassengerData() {
        androidRailCheckoutPage.fillAdultPassenger(railData.getTotalAdultPassenger());
    }

    @When("^\\[android-rail-checkout-page\\] user fill infant passenger data$")
    public void androidRailCheckoutPageUserFillInfantPassengerData() {
        androidRailCheckoutPage.fillInfantPassenger(railData.getTotalInfantPassenger());
    }

    @And("^\\[android-rail-checkout-page\\] user should see customer data$")
    public void androidRailCheckoutPageUserShouldSeeCustomerData() {
        assertThat("customer data is not visible", androidRailCheckoutPage.isCustomerDataVisible(),
                equalTo(true));
    }

    @And("^\\[android-rail-checkout-page\\] user should see adult passenger data$")
    public void androidRailCheckoutPageUserShouldSeeAdultPassengerData() {
        assertThat("adult passenger data is not visible", androidRailCheckoutPage.isAdultPassengerDataVisible(),
                equalTo(true));
    }

    @And("^\\[android-rail-checkout-page\\] user should see infant passenger data$")
    public void androidRailCheckoutPageUserShouldSeeInfantPassengerData() {
        assertThat("infant passenger data is not visible", androidRailCheckoutPage.isInfantPassengerDataVisible(),
                equalTo(true));
    }

    @When("^\\[android-rail-checkout-page\\] user click proceed to payment$")
    public void androidRailCheckoutPageUserClickProceedToPayment() {
        androidRailCheckoutPage.clickProceedToPayment();
    }

    @When("^\\[android-rail-checkout-page\\] user click change seat$")
    public void androidRailCheckoutPageUserClickChangeSeat() {
        androidRailCheckoutPage.clickChangeSeat();
    }

    @Then("^\\[android-rail-checkout-page\\] user should see error message '(.*)'$")
    public void androidRailCheckoutPageUserShouldSeeErrorMessage(String errorMessage) {
        assertThat("error message is different with expected", androidRailCheckoutPage.getToastByAttribute("text"),
                equalTo(errorMessage));
    }

    @When("^\\[android-rail-checkout-page\\] user fill adult passenger title '(.*)'$")
    public void androidRailCheckoutPageUserFillAdultPassengerTitle(String adultPassengerTitle) {
        androidRailCheckoutPage.fillAdultPassengerTitle(adultPassengerTitle);
    }

    @And("^\\[android-rail-checkout-page\\] user fill adult passenger name '(.*)'$")
    public void androidRailCheckoutPageUserFillAdultPassengerName(String adultPassengerName) {
        androidRailCheckoutPage.fillAdultPassengerName(adultPassengerName);
    }

    @And("^\\[android-rail-checkout-page\\] user fill adult passenger identityCard '(.*)'$")
    public void androidRailCheckoutPageUserFillAdultPassengerIdentityCard(String adultPassengerIdentityCard) {
        androidRailCheckoutPage.fillAdultPassengerIdentityCard(adultPassengerIdentityCard);
    }

    @And("^\\[android-rail-checkout-page\\] user click save passenger data$")
    public void androidRailCheckoutPageUserClickSavePassengerData() {
        androidRailCheckoutPage.clickSavePassengerData();
    }
}
