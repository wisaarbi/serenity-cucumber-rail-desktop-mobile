package com.serenity.cucumber.rail.steps;

import com.serenity.cucumber.rail.controller.LoginController;
import com.serenity.cucumber.rail.data.LoginAPIData;
import com.serenity.cucumber.rail.data.LoginData;
import com.serenity.cucumber.rail.pages.android.AndroidAccountPage;
import com.serenity.cucumber.rail.pages.android.AndroidBottomNavigationPage;
import com.serenity.cucumber.rail.pages.android.AndroidHomePage;
import com.serenity.cucumber.rail.pages.android.AndroidLoginPage;
import com.serenity.cucumber.rail.pages.android.AndroidProfilePage;
import com.serenity.cucumber.rail.pages.desktop.DesktopFavouritesPage;
import com.serenity.cucumber.rail.pages.desktop.DesktopHeaderMainPage;
import com.serenity.cucumber.rail.pages.desktop.DesktopHeaderPage;
import com.serenity.cucumber.rail.pages.desktop.DesktopHeaderTravelPage;
import com.serenity.cucumber.rail.pages.desktop.DesktopHomePage;
import com.serenity.cucumber.rail.pages.desktop.DesktopLoginPage;
import com.serenity.cucumber.rail.pages.desktop.DesktopProfilePage;
import com.serenity.cucumber.rail.pages.desktop.DesktopRailSearchPage;
import com.serenity.cucumber.rail.properties.LoginProperties;
import com.serenity.cucumber.rail.utility.FetchingEmail;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class LoginSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    DesktopHomePage desktopHomePage;

    @Autowired
    DesktopFavouritesPage desktopFavouritesPage;

    @Autowired
    DesktopHeaderTravelPage desktopHeaderTravelPage;

    @Autowired
    DesktopLoginPage desktopLoginPage;

    @Autowired
    DesktopHeaderMainPage desktopHeaderMainPage;

    @Autowired
    DesktopProfilePage desktopProfilePage;

    @Autowired
    LoginProperties loginProperties;

    @Autowired
    LoginData loginData;

    @Autowired
    LoginAPIData loginAPIData;

    @Autowired
    LoginController loginController;

    @Autowired
    AndroidHomePage androidHomePage;

    @Autowired
    AndroidLoginPage androidLoginPage;

    @Autowired
    AndroidBottomNavigationPage androidBottomNavigationPage;

    @Autowired
    AndroidAccountPage androidAccountPage;

    @Autowired
    AndroidProfilePage androidProfilePage;

    @Autowired
    DesktopHeaderPage desktopHeaderPage;

    @Autowired
    FetchingEmail fetchingEmail;

    @Autowired
    DesktopRailSearchPage desktopRailSearchPage;

    @Given("^\\[desktop-home-page\\] user open home page blibli$")
    public void desktopHomePageUserOpenHomepageBlibli() {
        desktopHomePage.open();
        desktopHomePage.addDefaultCookies();
    }

    @And("^\\[desktop-home-page\\] user click all category$")
    public void desktopHomePageUserClickAllCategory() {
        desktopHomePage.clickAllCategory();
    }

    @And("^\\[desktop-favourites-page\\] user click kereta$")
    public void desktopFavouritesPageUserClickKereta() {
        if (desktopHomePage.isIframeVisible()) {
            desktopHomePage.closeIframeIfShowUp();
        }
        desktopFavouritesPage.clickKereta();
    }

    @Then("^\\[desktop-rail-search-page\\] user should see rail search form$")
    public void desktopRailSearchPageUserShouldSeeRailSearchForm() {
        assertThat("rail search form is not visible",
                desktopRailSearchPage.isRailSearchFormVisible(), equalTo(true));
    }

    @When("^\\[desktop-header-travel-page\\] user click login$")
    public void desktopHeaderTravelPageUserClickLogin() {
        desktopHeaderTravelPage.clickLogin();
    }

    @Then("^\\[desktop-login-page\\] user should see login form$")
    public void desktopLoginPageUserShouldSeeLoginForm() {
        assertThat("login form not visible", desktopLoginPage.isLoginFormVisible(), equalTo(true));
    }

    @When("^\\[desktop-login-page\\] user fill email with '(.*)'$")
    public void desktopLoginPageUserFillEmail(String email) {
        switch (email) {
            case "validEmail":
                loginData.setEmail(loginProperties.get("validEmail"));
                break;
            case "notRegisteredEmail":
                loginData.setEmail(loginProperties.get("notRegisteredEmail"));
                break;
            case "invalidEmail":
                loginData.setEmail(loginProperties.get("invalidEmail"));
                break;
            default:
                loginData.setEmail(email);
                break;
        }
        desktopLoginPage.fillEmail(loginData.getEmail());
    }

    @And("^\\[desktop-login-page\\] user fill password with '(.*)'$")
    public void desktopLoginPageUserFillPassword(String password) {
        switch (password) {
            case "validPassword":
                loginData.setPassword(loginProperties.get("validPassword"));
                break;
            case "invalidPassword":
                loginData.setPassword(loginProperties.get("invalidPassword"));
                break;
            default:
                loginData.setPassword(password);
                break;
        }
        desktopLoginPage.fillPassword(loginData.getPassword());
    }

    @And("^\\[desktop-login-page\\] user click login$")
    public void desktopLoginPageUserClickLogin() {
        desktopLoginPage.clickLogin();
    }

    @And("^\\[desktop-header-main-page\\] user click profile$")
    public void desktopHeadermainPageUserClickProfile() {
        desktopHeaderMainPage.clickProfile();
    }

    @Then("^\\[desktop-profile-page\\] user should see the same email$")
    public void desktopProfilePageUserShouldSeeTheSameEmail() {
        assertThat("email is different", desktopProfilePage.getEmail(), equalTo(loginData.getEmail()));
    }

    @And("^\\[desktop-profile-page\\] user open profile page$")
    public void desktopProfilePageUserOpenProfile() {
        desktopProfilePage.open();
    }

    @Then("^\\[desktop-login-page\\] user should see error message '(.*)'$")
    public void desktopLoginPageUserShouldSeeErrorMessage(String errorMessage) {
        assertThat("error message not visible", desktopLoginPage.isErrorMessageVisible(), equalTo(true));
        assertThat("error message is different with expected", desktopLoginPage.getErrorMessage(), equalTo(errorMessage));
    }

    @When("^\\[login-api\\] user send get user has logged in request$")
    public void loginApiUserSendGetUserHasLoggedInRequest() {
        loginAPIData.setResponseGetUserLogin(loginController.getUserLogin());
    }

    @Then("^\\[login-api\\] user should see get user has logged in response with status code (\\d+)$")
    public void loginApiUserShouldSeeGetUserHasLoggedInStatusCode(int statusCode) {
        assertThat("status code is different", loginAPIData.getResponseGetUserLogin().getCode(), equalTo(statusCode));
    }

    @And("^\\[login-api\\] user should see get user has logged in response with email '(.*)'$")
    public void loginApiUserShouldSeeGetUserHasLoggedInEmail(String email) {
        switch (email) {
            case "validEmail":
                email = loginProperties.get("validEmail");
                break;
            case "invalidEmail":
                email = loginProperties.get("invalidEmail");
                break;
            default:
                break;
        }
        assertThat("email is different", loginAPIData.getResponseGetUserLogin().getData().getEmail(), equalTo(email));
    }

    @And("^\\[login-api\\] user should see get user has logged in response with status message '(.*)'$")
    public void loginApiUserShouldSeeGetUserHasLoggedInStatusMessage(String statusMessage) {
        assertThat("status message is different", loginAPIData.getResponseGetUserLogin().getStatus(), equalTo(statusMessage));
    }

    @And("^\\[desktop-home-page\\] user close iframe if show up$")
    public void desktopHomePageUserCloseIframeIfShowUp() {
        if (desktopHomePage.isIframeVisible()) {
            desktopHomePage.closeIframeIfShowUp();
        }
    }

    @Given("^\\[desktop-profile-page\\] user get cookies$")
    public void desktopProfilePageUserGetCookies() {
        loginData.setLoginCookies(desktopProfilePage.getCookies());
    }

    @And("^\\[login-api\\] user prepare data get user has logged in with cookies$")
    public void loginApiUserPrepareDataGetUserHasLoggedInWithCookies() {
        loginAPIData.setLoginCookies(loginData.getLoginCookies());
    }

    @Given("^\\[android-home-page\\] user open blibli application$")
    public void androidHomepageUserOpenBlibliApplication() {
        androidHomePage.openApp();
    }

    @Given("^\\[android-home-page\\] user click login button$")
    public void androidHomepageUserClickLoginButton() {
        androidHomePage.clickLoginButton();
    }

    @When("^\\[android-login-page\\] user fill email '(.*)'$")
    public void androidLoginPageUserFillEmail(String email) {
        switch (email) {
            case "validEmail":
                loginData.setEmail(loginProperties.get("validEmail"));
                break;
            case "notRegisteredEmail":
                loginData.setEmail(loginProperties.get("notRegisteredEmail"));
                break;
            case "invalidEmail":
                loginData.setEmail(loginProperties.get("invalidEmail"));
                break;
            default:
                loginData.setEmail(email);
                break;
        }
        androidLoginPage.fillEmail(loginData.getEmail());
    }

    @And("^\\[android-login-page\\] user fill password '(.*)'$")
    public void androidLoginPageUserFillPassword(String password) {
        switch (password) {
            case "validPassword":
                loginData.setPassword(loginProperties.get("validPassword"));
                break;
            case "invalidPassword":
                loginData.setPassword(loginProperties.get("invalidPassword"));
                break;
            default:
                loginData.setPassword(password);
                break;
        }
        androidLoginPage.fillPassword(loginData.getPassword());
    }

    @And("^\\[android-login-page\\] user click login$")
    public void androidLoginPageUserClickLogin() {
        androidLoginPage.clickLogin();
    }

    @When("^\\[android-bottom-navigation-page\\] user click '(.*)'$")
    public void androidBottomNavigationPageUserClick(String content) {
        androidBottomNavigationPage.clickBottomNavigation(content);
    }

    @And("^\\[android-account-page\\] user click profile info$")
    public void androidAccountPageUserClickProfileInfo() {
        androidAccountPage.clickProfileInfo();
    }

    @Then("^\\[android-profile-page\\] user should see the same email$")
    public void androidProfilePageUserShouldSeeTheSameEmail() {
        assertThat("email is different", androidProfilePage.getEmail(), equalTo(loginData.getEmail()));
    }

    @Then("^\\[desktop-login-page\\] user should see button login is '(.*)'$")
    public void desktopLoginPageUserShouldSeeButtonLoginIs(String buttonStatus) {
        if (buttonStatus.equals("enabled")) {
            assertThat("button login is not enabled", desktopLoginPage.isButtonLoginEnabled(), equalTo(true));
        } else {
            assertThat("button login is not disabled", desktopLoginPage.isButtonLoginEnabled(), equalTo(false));
        }
    }

    @And("^\\[desktop-login-page\\] user click send verification code via email$")
    public void desktopLoginPageUserClickSendVerificationCodeViaEmail() {
        desktopLoginPage.clickVerificationCodeViaEmail();
    }

    @Given("^\\[common-page\\] user open new tab$")
    public void commonPageUserOpenNewTab() {
        desktopLoginPage.openNewTab();
    }

    @And("^\\[common-page\\] user take control of the new tab$")
    public void commonPageUserTakeControlOfTheNewTab() {
        desktopLoginPage.takeControlOfTheNewTab();
    }

    @And("^\\[common-page\\] user close the new tab$")
    public void commonPageUserCloseTheNewTab() {
        desktopHomePage.closeTheNewTab();
    }

    @And("^\\[common-page\\] user take control of the old tab$")
    public void commonPageUserTakeControlOfTheOldTab() {
        desktopHomePage.takeControlOfTheOldTab();
    }

    @When("^\\[desktop-login-page\\] user fill the verification code$")
    public void desktopLoginPageUserFillTheVerificationCode() {
        desktopLoginPage.fillVerificationCode(loginData.getVerificationCode());
    }

    @And("^\\[desktop-login-page\\] user click verification$")
    public void desktopLoginPageUserClickVerification() {
        desktopLoginPage.clickVerification();
    }

    @Then("^\\[desktop-header-page\\] user should see user already login$")
    public void desktopHeaderPageUserShouldSeeUserAlreadyLogin() {
        assertThat("user already login is not visible", desktopHeaderPage.isUserAlreadyLoginVisible(),
                equalTo(true));
    }

    @And("^\\[desktop-login-page\\] user gets a verification code with$")
    public void desktopLoginPageUserGetsAVerificationCodeWith(DataTable dt) {
        List<Map<String, String>> params = dt.asMaps(String.class, String.class);
        String email = params.get(0).get("email");
        switch (email) {
            case "validEmailGoogle":
                email = loginProperties.get("validEmailGoogle");
                break;
            default:
                break;
        }
        String password = params.get(0).get("password");
        switch (password) {
            case "validPasswordGoogle":
                password = loginProperties.get("validPasswordGoogle");
                break;
            default:
                break;
        }
        String host = params.get(0).get("host");
        String port = params.get(0).get("port");
        loginData.setVerificationCode(fetchingEmail.getVerificationCode(host, port, email, password));
    }

    @And("^\\[desktop-home-page\\] user decline location permission$")
    public void desktopHomePageUserDeclineLocationPermission() {
        desktopHomePage.declineLocationPermission();
    }

    @Then("^\\[android-login-page\\] user should see error message '(.*)'$")
    public void androidLoginPageUserShouldSeeErrorMessage(String errorMessage) {
        assertThat("error message not visible", androidLoginPage.isErrorMessageVisible(), equalTo(true));
        assertThat("error message is different with expected", androidLoginPage.getErrorMessage(), equalTo(errorMessage));
    }

    @Then("^\\[android-login-page\\] user should see button login is '(.*)'$")
    public void androidLoginPageUserShouldSeeButtonLoginIs(String buttonStatus) {
        if (buttonStatus.equals("enabled")) {
            assertThat("button login is not enabled", androidLoginPage.isButtonLoginEnabled(), equalTo(true));
        } else {
            assertThat("button login is not disabled", androidLoginPage .isButtonLoginEnabled(), equalTo(false));
        }
    }
}
