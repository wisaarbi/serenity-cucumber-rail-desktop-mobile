package com.serenity.cucumber.rail.steps;

import com.serenity.cucumber.rail.data.APIData;
import com.serenity.cucumber.rail.data.DesktopData;
import com.serenity.cucumber.rail.data.MobileData;
import com.serenity.cucumber.rail.utility.FileUtility;
import com.serenity.cucumber.rail.utility.JsonUtility;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class RailCompareDataSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    FileUtility fileUtility;

    @Autowired
    JsonUtility jsonUtility;

    @Autowired
    DesktopData desktopData;

    @Autowired
    MobileData mobileData;

    @Autowired
    APIData apiData;

    @Given("^\\[rail-compare-data\\] users get data from desktop JSON files$")
    public void railCompareDataUsersGetDataFromDesktopJSONFiles() {
        String directory = System.getProperty("user.dir") + "\\target\\json\\";
        String fileName = "desktop-data.json";
        String json = fileUtility.readFile(directory, fileName);

        desktopData = jsonUtility.convertStringJsonToPojo(json, DesktopData.class);
    }

    @And("^\\[rail-compare-data\\] users get data from mobile JSON files$")
    public void railCompareDataUsersGetDataFromMobileJSONFiles() {
        String directory = System.getProperty("user.dir") + "\\target\\json\\";
        String fileName = "mobile-data.json";
        String json = fileUtility.readFile(directory, fileName);

        mobileData = jsonUtility.convertStringJsonToPojo(json, MobileData.class);
    }

    @And("^\\[rail-compare-data\\] users get data from API JSON files$")
    public void railCompareDataUsersGetDataFromAPIJSONFiles() {
        String directory = System.getProperty("user.dir") + "\\target\\json\\";
        String fileName = "api-data.json";
        String json = fileUtility.readFile(directory, fileName);

        apiData = jsonUtility.convertStringJsonToPojo(json, APIData.class);
    }

    @Then("^\\[rail-compare-data\\] user compare popular city name data between desktop, mobile, and API$")
    public void railCompareDataUserComparePopularCitynameDataBetweenDesktopMobileAndAPI() {
        Serenity.recordReportData().withTitle("Desktop Data").andContents(desktopData.getPopularCityName().toString());
        Serenity.recordReportData().withTitle("Mobile Data").andContents(mobileData.getPopularCityName().toString());
        Serenity.recordReportData().withTitle("API Data").andContents(apiData.getPopularCityName().toString());
        for (int i = 0; i < desktopData.getPopularCityName().size(); i++) {
            assertThat("data between desktop and mobile is different", desktopData.getPopularCityName().get(i),
                    containsString(apiData.getPopularCityName().get(i)));
            assertThat("data between desktop and api is different", desktopData.getPopularCityName().get(i),
                    containsString(apiData.getPopularCityName().get(i)));
        }
    }
}
