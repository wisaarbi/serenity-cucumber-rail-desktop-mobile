package com.serenity.cucumber.rail.steps;

import com.serenity.cucumber.rail.data.RailData;
import com.serenity.cucumber.rail.pages.android.*;
import com.serenity.cucumber.rail.pages.desktop.DesktopRailListPage;
import com.serenity.cucumber.rail.properties.RailProperties;
import com.serenity.cucumber.rail.utility.DateUtility;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public class RailListTicketSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    RailData railData;

    @Autowired
    DesktopRailListPage desktopRailListPage;

    @Autowired
    RailProperties railProperties;

    @Autowired
    DateUtility dateUtility;

    @Autowired
    AndroidRailListPage androidRailListPage;

    @Autowired
    AndroidRailFilterPage androidRailFilterPage;

    @Autowired
    AndroidRailRouteChangePage androidRailRouteChangePage;

    @Autowired
    AndroidRailDateDeparturePage androidRailDateDeparturePage;

    @Autowired
    AndroidRailPassengerPage androidRailPassengerPage;

    @When("^\\[desktop-rail-list-page\\] user filter departure time$")
    public void desktopRailListPageUserFilterDepartureTime() {
        List<String> listFilterTimeDeparture = desktopRailListPage.getListFilterTimeDeparture();
        String timeDeparture = listFilterTimeDeparture.get(0)
                .substring(0, listFilterTimeDeparture.get(0).indexOf("(") - 1);
        railData.setTimeDeparture(timeDeparture);
        desktopRailListPage.filterDepartureTime(railData.getTimeDeparture());
    }

    @Then("^\\[desktop-rail-list-page\\] user should see filtered list departure time$")
    public void desktopRailListPageUserShouldSeeFilteredListDepartureTime() {
        String lowerTime = null;
        String upperTime = null;
        switch (railData.getTimeDeparture()) {
            case "Pagi":
                lowerTime = "00:00";
                upperTime = "11:59";
                break;
            case "Siang":
                lowerTime = "12:00";
                upperTime = "17:59";
                break;
            case "Malam":
                lowerTime = "18:00";
                upperTime = "23:59";
                break;
            default:
                break;
        }
        assertThat("departure time is different with expected", desktopRailListPage.getListDepartureTime(),
                everyItem(both(greaterThanOrEqualTo(lowerTime)).and(lessThanOrEqualTo(upperTime))));
    }

    @When("^\\[desktop-rail-list-page\\] user filter departure class$")
    public void desktopRailListPageUserFilterDepartureClass() {
        List<String> listFilterClassDeparture = desktopRailListPage.getListFilterClassDeparture();
        railData.setClassDeparture(listFilterClassDeparture.get(0));
        desktopRailListPage.filterDepartureClass(railData.getClassDeparture());
    }

    @Then("^\\[desktop-rail-list-page\\] user should see filtered list departure class$")
    public void desktopRailListPageUserShouldSeeFilteredListDepartureClass() {
        assertThat("departure class is different with expected", desktopRailListPage.getListDepartureClass(),
                everyItem(containsString(railData.getClassDeparture())));
    }

    @When("^\\[desktop-rail-list-page\\] user filter departure price to '(.*)' price$")
    public void desktopRailListPageUserFilterDeparturePriceToPrice(String price) {
        switch (price) {
            case "MAX":
                railData.setMaxPriceDeparture(desktopRailListPage.getMaxPriceDeparture());
                desktopRailListPage.filterDeparturePriceToMaxPrice();
                break;
            case "MIN":
                railData.setMinPriceDeparture(desktopRailListPage.getMinPriceDeparture());
                desktopRailListPage.filterDeparturePriceToMinPrice();
                break;
            default:
                break;
        }

    }

    @Then("^\\[desktop-rail-list-page\\] user should see filtered list '(.*)' departure price$")
    public void desktopRailListPageUserShouldSeeFilteredListDeparturePrice(String price) {
        String expectedPrice = null;
        switch (price) {
            case "MAX":
                expectedPrice = railData.getMaxPriceDeparture();
                break;
            case "MIN":
                expectedPrice = railData.getMinPriceDeparture();
                break;
            default:
                break;
        }
        assertThat("departure price is different with expected", desktopRailListPage.getListDeparturePrice(),
                everyItem(containsString(expectedPrice)));
    }

    @When("^\\[desktop-rail-list-page\\] user filter departure train type$")
    public void desktopRailListPageUserFilterDepartureTrainType() {
        desktopRailListPage.clickDepartureTrainType();
        List<String> listFilterTrainTypeDeparture = desktopRailListPage.getListFilterTrainTypeDeparture();
        railData.setTrainTypeDeparture(listFilterTrainTypeDeparture.get(0));
        desktopRailListPage.filterDepartureTrainType(railData.getTrainTypeDeparture());
    }

    @Then("^\\[desktop-rail-list-page\\] user should see filtered list departure train type$")
    public void desktopRailListPageUserShouldSeeFilteredListDepartureTrainType() {
        assertThat("departure train type is different with expected", desktopRailListPage.getListDepartureTrainType(),
                everyItem(containsString(railData.getTrainTypeDeparture())));
    }

    @When("^\\[desktop-rail-list-page\\] user click ganti pencarian$")
    public void desktopRailListPageUserClickGantiPencarian() {
        desktopRailListPage.clickGantiPencarian();
    }

    @And("^\\[desktop-rail-list-page\\] user click departure location$")
    public void desktopRailListPageUserClickDepartureLocation() {
        desktopRailListPage.clickDepartureLocation();
    }

    @And("^\\[desktop-rail-list-page\\] user change departure city '(.*)'$")
    public void desktopRailListPageUserChangeDepartureCity(String changeDeptCity) {
        switch (changeDeptCity) {
            case "validChangeDeptCity":
                railData.setDeptCity(railProperties.get("validChangeDeptCity"));
                break;
            default:
                railData.setDeptCity(changeDeptCity);
                break;
        }
        desktopRailListPage.changeDepartureCity(railData.getDeptCity());
    }

    @And("^\\[desktop-rail-list-page\\] user change departure station '(.*)'$")
    public void desktopRailListPageUserChangeDepartureStation(String changeDeptStation) {
        switch (changeDeptStation) {
            case "validChangeDeptStation":
                railData.setDeptStation(railProperties.get("validChangeDeptStation"));
                break;
            default:
                railData.setDeptStation(changeDeptStation);
                break;
        }
        desktopRailListPage.changeDepartureStation(railData.getDeptStation());
    }

    @And("^\\[desktop-rail-list-page\\] user click destination location$")
    public void desktopRailListPageUserClickDestinationLocation() {
        desktopRailListPage.clickDestinationLocation();
    }

    @And("^\\[desktop-rail-list-page\\] user change destination city '(.*)'$")
    public void desktopRailListPageUserChangeDestinationCity(String changeDestCity) {
        switch (changeDestCity) {
            case "validChangeDestCity":
                railData.setDestCity(railProperties.get("validChangeDestCity"));
                break;
            default:
                railData.setDestCity(changeDestCity);
                break;
        }
        desktopRailListPage.changeDestinationCity(railData.getDestCity());
    }

    @And("^\\[desktop-rail-list-page\\] user change destination station '(.*)'$")
    public void desktopRailListPageUserChangeDestinationStation(String changeDestStation) {
        switch (changeDestStation) {
            case "validChangeDestStation":
                railData.setDestStation(railProperties.get("validChangeDestStation"));
                break;
            default:
                railData.setDestStation(changeDestStation);
                break;
        }
        desktopRailListPage.changeDestinationStation(railData.getDestStation());
    }

    @And("^\\[desktop-rail-list-page\\] user change passenger$")
    public void desktopRailListPageUserChangePassenger(DataTable passenger) {
        List<Map<String, String>> listTotalPassenger = passenger.asMaps(String.class, String.class);
        String changeTotalAdultPassenger = listTotalPassenger.get(0).get("changeTotalAdultPassenger");
        String changeTotalInfantPassenger = listTotalPassenger.get(0).get("changeTotalInfantPassenger");
        switch (changeTotalAdultPassenger) {
            case "validChangeTotalAdultPassenger":
                railData.setTotalAdultPassenger(Integer.parseInt(railProperties.get("validChangeTotalAdultPassenger")));
                break;
            default:
                railData.setTotalAdultPassenger(Integer.parseInt(changeTotalAdultPassenger));
                break;
        }
        switch (changeTotalInfantPassenger) {
            case "validChangeTotalInfantPassenger":
                railData.setTotalInfantPassenger(Integer.parseInt(railProperties.get("validChangeTotalInfantPassenger")));
                break;
            default:
                railData.setTotalInfantPassenger(Integer.parseInt(changeTotalInfantPassenger));
                break;
        }
        desktopRailListPage.clickPassenger()
                .changePassenger(railData.getTotalAdultPassenger(), railData.getTotalInfantPassenger());
    }

    @And("^\\[desktop-rail-list-page\\] user click cari$")
    public void desktopRailListPageUserClickCari() {
        desktopRailListPage.clickCari();
    }

    @When("^\\[desktop-rail-list-page\\] user click choose departure ticket$")
    public void desktopRailListPageUserClickChooseDepartureTicket() {
        desktopRailListPage.clickChooseDepartureTicket();
    }

    @When("^\\[desktop-rail-list-page\\] user click choose return ticket$")
    public void desktopRailListPageUserClickChooseReturnTicket() {
        desktopRailListPage.clickChooseReturnTicket();
    }

    @Then("^\\[desktop-rail-list-page\\] user should see ticket summary departure$")
    public void desktopRailListPageUserShouldSeeTicketSummaryDeparture() {
        assertThat("ticket summary departure is not visible", desktopRailListPage.isTicketSummaryDepartureVisible(),
                equalTo(true));
    }

    @Then("^\\[desktop-rail-list-page\\] user should see ticket summary return$")
    public void desktopRailListPageUserShouldSeeTicketSummaryReturn() {
        assertThat("ticket summary return is not visible", desktopRailListPage.isTicketSummaryReturnVisible(),
                equalTo(true));
    }

    @When("^\\[desktop-rail-list-page\\] user filter return time$")
    public void desktopRailListPageUserFilterReturnTime() {
        List<String> listFilterTimeReturn = desktopRailListPage.getListFilterTimeReturn();
        String timeReturn = listFilterTimeReturn.get(0)
                .substring(0, listFilterTimeReturn.get(0).indexOf("(") - 1);
        railData.setTimeReturn(timeReturn);
        desktopRailListPage.filterReturnTime(railData.getTimeReturn());
    }

    @Then("^\\[desktop-rail-list-page\\] user should see filtered list return time$")
    public void desktopRailListPageUserShouldSeeFilteredListReturnTime() {
        String lowerTime = null;
        String upperTime = null;
        switch (railData.getTimeReturn()) {
            case "Pagi":
                lowerTime = "00:00";
                upperTime = "11:59";
                break;
            case "Siang":
                lowerTime = "12:00";
                upperTime = "17:59";
                break;
            case "Malam":
                lowerTime = "18:00";
                upperTime = "23:59";
                break;
            default:
                break;
        }
        assertThat("return time is different with expected", desktopRailListPage.getListReturnTime(),
                everyItem(both(greaterThanOrEqualTo(lowerTime)).and(lessThanOrEqualTo(upperTime))));
    }

    @When("^\\[desktop-rail-list-page\\] user filter return class$")
    public void desktopRailListPageUserFilterReturnClass() {
        List<String> listFilterClassReturn = desktopRailListPage.getListFilterClassReturn();
        railData.setClassReturn(listFilterClassReturn.get(0));
        desktopRailListPage.filterReturnClass(railData.getClassReturn());
    }

    @Then("^\\[desktop-rail-list-page\\] user should see filtered list return class$")
    public void desktopRailListPageUserShouldSeeFilteredListReturnClass() {
        assertThat("return class is different with expected", desktopRailListPage.getListReturnClass(),
                everyItem(containsString(railData.getClassReturn())));
    }

    @When("^\\[desktop-rail-list-page\\] user filter return train type$")
    public void desktopRailListPageUserFilterReturnTrainType() {
        desktopRailListPage.clickReturnTrainType();
        List<String> listFilterTrainTypeReturn = desktopRailListPage.getListFilterTrainTypeReturn();
        railData.setTrainTypeReturn(listFilterTrainTypeReturn.get(0));
        desktopRailListPage.filterReturnTrainType(railData.getTrainTypeReturn());
    }

    @Then("^\\[desktop-rail-list-page\\] user should see filtered list return train type$")
    public void desktopRailListPageUserShouldSeeFilteredListReturnTrainType() {
        assertThat("return train type is different with expected", desktopRailListPage.getListReturnTrainType(),
                everyItem(containsString(railData.getTrainTypeReturn())));
    }

    @When("^\\[desktop-rail-list-page\\] user click order$")
    public void desktopRailListPageUserClickOrder() {
        desktopRailListPage.clickOrder();
    }

    @And("^\\[desktop-rail-list-page\\] user change date of departure with day plus '(\\d+)'$")
    public void desktopRailListPageUserChangeDateOfDepartureWithDayPlus(int dayPlus) {
        Date currentDatePlusDay = dateUtility.getCurrentDateWithDayPlus(dayPlus);
        String dateFormat = "dd-MM-yyyy";
        String date = dateUtility.convertDateToString(currentDatePlusDay, dateFormat);
        railData.setDateDeparture(date);
        desktopRailListPage.clickDateDeparture().changeDateDeparture(railData.getDateDeparture());
    }

    @And("^\\[desktop-rail-list-page\\] user change date of return with day plus '(\\d+)'$")
    public void desktopRailListPageUserChangeDateOfReturnWithDayPlus(int dayPlus) {
        Date currentDatePlusDay = dateUtility.getCurrentDateWithDayPlus(dayPlus);
        String dateFormat = "dd-MM-yyyy";
        String date = dateUtility.convertDateToString(currentDatePlusDay, dateFormat);
        railData.setDateReturn(date);
        desktopRailListPage.clickDateReturn().changeDateReturn(railData.getDateReturn());
    }

    @Then("^\\[android-rail-list-page\\] user should see route departure station$")
    public void androidRailListPageUserShouldSeeRouteDepartureStation() {
        assertThat("departure station is not visible",
                androidRailListPage.isDepartureStationVisible(), equalTo(true));
        assertThat("departure station is different with expected",
                androidRailListPage.getDepartureStation(), equalTo(railData.getDeptStation()));
    }

    @And("^\\[android-rail-list-page\\] user should see route destination station$")
    public void androidRailListPageUserShouldSeeRouteDestinationStation() {
        assertThat("destination station is not visible",
                androidRailListPage.isDestinationStationVisible(), equalTo(true));
        assertThat("destination station is different with expected",
                androidRailListPage.getDestinationStation(), equalTo(railData.getDestStation()));
    }

    @And("^\\[android-rail-list-page\\] user should see total passenger$")
    public void androidRailListPageUserShouldSeeTotalPassenger() {
        int totalPassenger = railData.getTotalInfantPassenger() + railData.getTotalAdultPassenger();
        assertThat("total passenger is not visible",
                androidRailListPage.isTotalPassengerVisible(), equalTo(true));
        assertThat("total passenger is different with expected",
                androidRailListPage.getTotalPassenger(), equalTo(totalPassenger));
    }

    @When("^\\[android-rail-list-page\\] user filter departure time$")
    public void androidRailListPageUserFilterDepartureTime() {
        androidRailListPage.clickFilter();
        List<String> listFilterTimeDeparture = androidRailFilterPage.getListFilterTimeDeparture();
        String timeDeparture = listFilterTimeDeparture.get(0)
                .substring(0, listFilterTimeDeparture.get(0).indexOf("(") - 1);
        railData.setTimeDeparture(timeDeparture);
        androidRailFilterPage.filterTimeDeparture(railData.getTimeDeparture()).clickApply();
    }

    @Then("^\\[android-rail-list-page\\] user should see filtered list departure time$")
    public void androidRailListPageUserShouldSeeFilteredListDepartureTime() {
        String lowerTime = null;
        String upperTime = null;
        switch (railData.getTimeDeparture()) {
            case "Pagi":
                lowerTime = "00:00";
                upperTime = "11:59";
                break;
            case "Siang":
                lowerTime = "12:00";
                upperTime = "17:59";
                break;
            case "Malam":
                lowerTime = "18:00";
                upperTime = "23:59";
                break;
            default:
                break;
        }
        assertThat("departure time is different with expected", androidRailListPage.getListDepartureTime(),
                everyItem(both(greaterThanOrEqualTo(lowerTime)).and(lessThanOrEqualTo(upperTime))));
    }

    @When("^\\[android-rail-list-page\\] user filter departure class$")
    public void androidRailListPageUserFilterDepartureClass() {
        androidRailListPage.clickFilter();
        androidRailFilterPage.scrollToTitleClass();
        List<String> listFilterClassDeparture = androidRailFilterPage.getListFilterClassDeparture();
        railData.setClassDeparture(listFilterClassDeparture.get(0));
        androidRailFilterPage.filterClassDeparture(railData.getClassDeparture()).clickApply();
    }

    @Then("^\\[android-rail-list-page\\] user should see filtered list departure class$")
    public void androidRailListPageUserShouldSeeFilteredListDepartureClass() {
        assertThat("departure class is different with expected", androidRailListPage.getListDepartureClass(),
                everyItem(containsString(railData.getClassDeparture())));
    }

    @When("^\\[android-rail-list-page\\] user filter departure train type$")
    public void androidRailListPageUserFilterDepartureTrainType() {
        androidRailListPage.clickFilter();
        List<String> listFilterTrainTypeDeparture = androidRailFilterPage.getListFilterTrainTypeDeparture();
        railData.setTrainTypeDeparture(listFilterTrainTypeDeparture.get(0));
        androidRailFilterPage.filterTrainTypeDeparture(railData.getTrainTypeDeparture()).clickApply();
    }

    @Then("^\\[android-rail-list-page\\] user should see filtered list departure train type$")
    public void androidRailListPageUserShouldSeeFilteredListDepartureTrainType() {
        assertThat("departure train type is different with expected", androidRailListPage.getListDepartureTrainType(),
                everyItem(containsString(railData.getTrainTypeDeparture())));
    }

    @When("^\\[android-rail-list-page\\] user click ganti pencarian$")
    public void androidRailListPageUserClickGantiPencarian() {
        androidRailListPage.clickGantiPencarian();
    }

    @And("^\\[android-route-change-page\\] user click departure location$")
    public void androidRouteChangePageUserClickDepartureLocation() {
        androidRailRouteChangePage.clickDepartureLocation();
    }

    @And("^\\[android-route-change-page\\] user click destination location$")
    public void androidRouteChangePageUserClickDestinationLocation() {
        androidRailRouteChangePage.clickDestinationLocation();
    }

    @And("^\\[android-route-change-page\\] user fill date of departure with day plus '(\\d+)'$")
    public void androidRouteChangePageUserFillDateOfDepartureWithDayPlus(int dayPlus) {
        Date currentDatePlusDay = dateUtility.getCurrentDateWithDayPlus(dayPlus);
        String dateFormat = "dd-MM-yyyy";
        String date = dateUtility.convertDateToString(currentDatePlusDay, dateFormat);
        railData.setDateDeparture(date);
        androidRailRouteChangePage.clickDateDeparture();
        androidRailDateDeparturePage.fillDateDeparture(railData.getDateDeparture());
    }

    @And("^\\[android-route-change-page\\] user fill passenger$")
    public void androidRouteChangePageUserFillPassenger(DataTable passenger) {
        List<Map<String, String>> listTotalPassenger = passenger.asMaps(String.class, String.class);
        String totalAdultPassenger = listTotalPassenger.get(0).get("totalAdultPassenger");
        String totalInfantPassenger = listTotalPassenger.get(0).get("totalInfantPassenger");
        switch (totalAdultPassenger) {
            case "validTotalAdultPassenger":
                railData.setTotalAdultPassenger(Integer.parseInt(railProperties.get("validTotalAdultPassenger")));
                break;
            case "validChangeTotalAdultPassenger":
                railData.setTotalAdultPassenger(Integer.parseInt(railProperties.get("validChangeTotalAdultPassenger")));
                break;
            default:
                railData.setTotalAdultPassenger(Integer.parseInt(totalAdultPassenger));
                break;
        }
        switch (totalInfantPassenger) {
            case "validTotalInfantPassenger":
                railData.setTotalInfantPassenger(Integer.parseInt(railProperties.get("validTotalInfantPassenger")));
                break;
            case "validChangeTotalInfantPassenger":
                railData.setTotalInfantPassenger(Integer.parseInt(railProperties.get("validChangeTotalInfantPassenger")));
                break;
            default:
                railData.setTotalInfantPassenger(Integer.parseInt(totalInfantPassenger));
                break;
        }
        androidRailRouteChangePage.clickPassenger();
        androidRailPassengerPage
                .fillPassenger(railData.getTotalAdultPassenger(), railData.getTotalInfantPassenger())
                .clickConfirm();
    }

    @And("^\\[android-route-change-page\\] user click cari$")
    public void androidRouteChangePageUserClickCari() {
        androidRailRouteChangePage.clickCari();
    }

    @When("^\\[android-rail-list-page\\] user filter departure price to '(.*)' price$")
    public void androidRailListPageUserFilterDeparturePriceToPrice(String price) {
//        androidRailListPage.clickFilter();
//        androidRailFilterPage.scrollToTitlePrice();
//        switch (price) {
//            case "MAX":
//                androidRailFilterPage.filterDeparturePriceToMaxPrice();
//                railData.setMaxPriceDeparture(androidRailFilterPage.getMaxPriceDeparture());
//                androidRailFilterPage.clickApply();
//                break;
//            case "MIN":
//                androidRailFilterPage.filterDeparturePriceToMinPrice();
//                railData.setMinPriceDeparture(androidRailFilterPage.getMinPriceDeparture());
//                androidRailFilterPage.clickApply();
//                break;
//            default:
//                break;
//        }
    }

    @Then("^\\[android-rail-list-page\\] user should see filtered list '(.*)' departure price$")
    public void androidRailListPageUserShouldSeeFilteredListDeparturePrice(String price) {
//        String expectedPrice = null;
//        int priceValue;
//        switch (price) {
//            case "MAX":
//                priceValue = Integer.parseInt(railData.getMaxPriceDeparture());
//                expectedPrice = String.format("%.d", priceValue);
//                break;
//            case "MIN":
//                priceValue = Integer.parseInt(railData.getMinPriceDeparture());
//                expectedPrice = String.format("%.d", priceValue);
//                break;
//            default:
//                break;
//        }
//        assertThat("departure price is different with expected", androidRailListPage.getListDeparturePrice(),
//                everyItem(containsString(expectedPrice)));
    }

    @And("^\\[android-rail-list-page\\] user should see list departure station$")
    public void androidRailListPageUserShouldSeeListDepartureStation() {
        assertThat("list departure station is not visible",
                androidRailListPage.isListDepartureStationVisible(), equalTo(true));
    }

    @And("^\\[android-rail-list-page\\] user should see list destination station$")
    public void androidRailListPageUserShouldSeeListDestinationStation() {
        assertThat("list departure station is not visible",
                androidRailListPage.isListDestinationStationVisible(), equalTo(true));
    }

    @When("^\\[android-rail-list-page\\] user click choose departure ticket$")
    public void androidRailListPageUserClickChooseDepartureTicket() {
        androidRailListPage.clickChooseDepartureTicket();
    }

    @Then("^\\[android-rail-list-page\\] user should see ticket summary departure$")
    public void androidRailListPageUserShouldSeeTicketSummaryDeparture() {
        assertThat("ticket summary departure is not visible", androidRailListPage.isTicketSummaryDepartureVisible(),
                equalTo(true));
    }

    @Then("^\\[android-rail-list-page\\] user should see filtered list return class$")
    public void androidRailListPageUserShouldSeeFilteredListReturnClass() {
        assertThat("return class is different with expected", androidRailListPage.getListReturnClass(),
                everyItem(containsString(railData.getClassReturn())));
    }

    @Then("^\\[android-rail-list-page\\] user should see filtered list return train type$")
    public void androidRailListPageUserShouldSeeFilteredListReturnTrainType() {
        assertThat("return train type is different with expected", androidRailListPage.getListReturnTrainType(),
                everyItem(containsString(railData.getTrainTypeReturn())));
    }

    @When("^\\[android-rail-list-page\\] user click choose return ticket$")
    public void androidRailListPageUserClickChooseReturnTicket() {
        androidRailListPage.clickChooseReturnTicket();
    }

    @When("^\\[android-rail-list-page\\] user filter return class$")
    public void androidRailListPageUserFilterReturnClass() {
        androidRailListPage.clickFilter();
        androidRailFilterPage.scrollToTitleClass();
        List<String> listFilterClassReturn = androidRailFilterPage.getListFilterClassReturn();
        railData.setClassReturn(listFilterClassReturn.get(0));
        androidRailFilterPage.filterClassReturn(railData.getClassReturn()).clickApply();
    }

    @When("^\\[android-rail-list-page\\] user filter return train type$")
    public void androidRailListPageUserFilterReturnTrainType() {
        androidRailListPage.clickFilter();
        List<String> listFilterTrainTypeReturn = androidRailFilterPage.getListFilterTrainTypeDeparture();
        railData.setTrainTypeReturn(listFilterTrainTypeReturn.get(0));
        androidRailFilterPage.filterTrainTypeReturn(railData.getTrainTypeReturn()).clickApply();
    }
}
