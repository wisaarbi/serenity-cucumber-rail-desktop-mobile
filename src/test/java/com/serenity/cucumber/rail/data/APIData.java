package com.serenity.cucumber.rail.data;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("com.serenity.cucumber.rail.data.APIData")
@Data
public class APIData {
    List<String> popularCityName;
    List<String> popularCityCode;
}
