package com.serenity.cucumber.rail.data;

import com.serenity.cucumber.rail.responses.checkholiday.getcheckholiday.GetCheckHolidayResponse;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.data.CheckHolidayAPIData")
@Data
public class CheckHolidayAPIData {
    private int yearStart;
    private int monthStart;
    private int yearEnd;
    private int monthEnd;

    private GetCheckHolidayResponse responseGetCheckHoliday;
}
