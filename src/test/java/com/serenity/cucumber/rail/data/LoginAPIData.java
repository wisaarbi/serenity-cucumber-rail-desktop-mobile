package com.serenity.cucumber.rail.data;

import com.serenity.cucumber.rail.responses.login.getuserlogin.GetUserLoginResponse;
import io.restassured.http.Cookies;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.data.LoginAPIData")
@Data
public class LoginAPIData {
    private Cookies loginCookies;

    private GetUserLoginResponse responseGetUserLogin;
}
