package com.serenity.cucumber.rail.data;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("com.serenity.cucumber.rail.data.DesktopData")
@Data
public class DesktopData {
    List<String> popularCityName;
}
