package com.serenity.cucumber.rail.data;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.data.DownloadData")
@Data
public class DownloadData {
    String fileName;
}
