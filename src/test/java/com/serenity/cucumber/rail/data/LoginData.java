package com.serenity.cucumber.rail.data;

import io.restassured.http.Cookies;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.data.LoginData")
@Data
public class LoginData {
    private String email;
    private String password;
    private String verificationCode;

    private Cookies loginCookies;
}
