package com.serenity.cucumber.rail.data;

import com.serenity.cucumber.rail.responses.railavailability.getrailticketavailability.GetRailTicketAvailabilityResponse;
import com.serenity.cucumber.rail.responses.railavailability.getsearchpopularstation.GetSearchPopularStationResponse;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.data.RailAPIData")
@Data
public class RailAPIData {
    private String originCode;
    private String originType;
    private String destinationCode;
    private String destinationType;
    private String departureDate;
    private String returnDate;
    private int adult;
    private int infant;

    private GetRailTicketAvailabilityResponse responseGetRailAvailability;
    private GetSearchPopularStationResponse responseGetSearchPopularStation;
}
