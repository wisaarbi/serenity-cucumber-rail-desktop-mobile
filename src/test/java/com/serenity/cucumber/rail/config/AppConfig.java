package com.serenity.cucumber.rail.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@EnableConfigurationProperties
@ComponentScan(basePackages = "com.serenity.cucumber.rail")
public class AppConfig {

}
