package com.serenity.cucumber.rail.utility;

import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component("com.serenity.cucumber.rail.utility.FileUtility")
public class FileUtility {

    public void deleteFile(String direcotry, String fileName) {
        String filePath = direcotry + fileName;
        File file = new File(filePath);
        file.delete();
    }

    @SneakyThrows
    public void writeFile(String directory, String fileName, String content) {
        File file = new File(directory);
        file.mkdirs();
        String filePath = directory + fileName;
        BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
        writer.write(content);
        writer.close();
    }

    @SneakyThrows
    public String readFile(String directory, String fileName) {
        String filePath = directory + fileName;
        String data = new String(Files.readAllBytes(Paths.get(filePath)));
        return data;
    }
}
