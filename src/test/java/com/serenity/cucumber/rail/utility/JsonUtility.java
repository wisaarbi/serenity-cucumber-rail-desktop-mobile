package com.serenity.cucumber.rail.utility;

import gherkin.deps.com.google.gson.Gson;
import org.springframework.stereotype.Component;

@Component("com.serenity.cucumber.rail.utility.JsonUtility")
public class JsonUtility {

    public String generateStringJson(Object data) {
        Gson gson = new Gson();
        return gson.toJson(data);
    }

    public <T> T convertStringJsonToPojo(String json, Class<T> classOfT) {
        Gson gson = new Gson();
        return gson.fromJson(json, classOfT);
    }

}
