package com.serenity.cucumber.rail.hooks;

import com.serenity.cucumber.rail.data.APIData;
import com.serenity.cucumber.rail.data.DesktopData;
import com.serenity.cucumber.rail.data.MobileData;
import com.serenity.cucumber.rail.steps.BaseSteps;
import com.serenity.cucumber.rail.utility.FileUtility;
import com.serenity.cucumber.rail.utility.JsonUtility;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import lombok.SneakyThrows;
import net.thucydides.core.steps.ScenarioSteps;
import org.springframework.beans.factory.annotation.Autowired;

public class CucumberHooks extends ScenarioSteps implements BaseSteps {

    @Autowired
    APIData apiData;

    @Autowired
    DesktopData desktopData;

    @Autowired
    MobileData mobileData;

    @Autowired
    JsonUtility jsonUtility;

    @Autowired
    FileUtility fileUtility;

    @Before
    public void beforeExecution() {

    }

    @After
    public void afterExecution() {

    }

    @SneakyThrows
    @After("@ProduceAPIData")
    public void doSomethingAfterProduceAPIData(){
        String directory = System.getProperty("user.dir") + "\\target\\json\\";
        String fileName = "api-data.json";
        String json = jsonUtility.generateStringJson(apiData);
        fileUtility.writeFile(directory, fileName, json);
    }

    @SneakyThrows
    @After("@ProduceDesktopData")
    public void doSomethingAfterProduceDesktopData(){
        String directory = System.getProperty("user.dir") + "\\target\\json\\";
        String fileName = "desktop-data.json";
        String json = jsonUtility.generateStringJson(desktopData);
        fileUtility.writeFile(directory, fileName, json);
    }

    @SneakyThrows
    @After("@ProduceMobileData")
    public void doSomethingAfterProduceMobileData(){
        String directory = System.getProperty("user.dir") + "\\target\\json\\";
        String fileName = "mobile-data.json";
        String json = jsonUtility.generateStringJson(mobileData);
        fileUtility.writeFile(directory, fileName, json);
    }
}
