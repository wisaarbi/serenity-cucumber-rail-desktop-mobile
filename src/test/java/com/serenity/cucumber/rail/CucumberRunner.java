package com.serenity.cucumber.rail;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/",
        glue = {"com.serenity.cucumber.rail.hooks", "com.serenity.cucumber.rail.steps"},
        tags = "@DesktopLoginFeature and @Positive"
)
public class CucumberRunner {}
