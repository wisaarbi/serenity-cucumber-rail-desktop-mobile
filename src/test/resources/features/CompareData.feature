@CompareData
Feature: Compare Data Feature

  Scenario: [COMPARE-DATA] compare data between desktop, mobile, and API
    Given [rail-compare-data] users get data from desktop JSON files
    And [rail-compare-data] users get data from mobile JSON files
    And [rail-compare-data] users get data from API JSON files
    Then [rail-compare-data] user compare popular city name data between desktop, mobile, and API
