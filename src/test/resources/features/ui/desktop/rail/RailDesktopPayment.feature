@RailDesktop @RailDesktopPaymentFeature @UI
Feature: Rail Desktop Payment Feature

  Background: [DESKTOP] user fill passenger data
    Given [desktop-home-page] user open home page blibli
    And [desktop-home-page] user close iframe if show up
    And [desktop-home-page] user decline location permission
    And [desktop-home-page] user click all category
    And [desktop-favourites-page] user click kereta
    Then [desktop-rail-search-page] user should see rail search form

    When [desktop-rail-search-page] user click departure location
    And [desktop-rail-search-page] user fill departure city 'validDeptCity'
    And [desktop-rail-search-page] user fill departure station 'validDeptStation'
    And [desktop-rail-search-page] user click destination location
    And [desktop-rail-search-page] user fill destination city 'validDestCity'
    And [desktop-rail-search-page] user fill destination station 'validDestStation'
    And [desktop-rail-search-page] user fill date of departure with day plus '4'
    And [desktop-rail-search-page] user fill passenger
      | totalAdultPassenger | totalInfantPassenger |
      | 1                   | 1                    |
    And [desktop-rail-search-page] user click cari
    Then [desktop-rail-list-page] user should see route departure station
    And [desktop-rail-list-page] user should see route destination station
    And [desktop-rail-list-page] user should see list departure station
    And [desktop-rail-list-page] user should see list destination station
    And [desktop-rail-list-page] user should see total passenger

    When [desktop-rail-list-page] user filter departure time
    Then [desktop-rail-list-page] user should see filtered list departure time

    When [desktop-rail-list-page] user filter departure class
    Then [desktop-rail-list-page] user should see filtered list departure class

    When [desktop-rail-list-page] user click choose departure ticket
    Then [desktop-rail-checkout-page] user should see order detail
    And [desktop-rail-checkout-page] user should see origin station
    And [desktop-rail-checkout-page] user should see destination station
    And [desktop-rail-checkout-page] user should see ticket price
    And [desktop-rail-checkout-page] user should see total ticket price
    And [desktop-rail-checkout-page] user should see total passenger
    And [desktop-rail-checkout-page] user should see train class
    And [desktop-rail-checkout-page] user should see date

    When [desktop-rail-checkout-page] user fill customer title 'customerTitle'
    And [desktop-rail-checkout-page] user fill customer name 'validCustomerName'
    And [desktop-rail-checkout-page] user fill customer phone number 'validCustomerPhoneNumber'
    And [desktop-rail-checkout-page] user fill customer email 'validCustomerEmail'
    And [desktop-rail-checkout-page] user click continue to fill in passenger data
    Then [desktop-rail-checkout-page] user should see customer title
    And [desktop-rail-checkout-page] user should see customer name
    And [desktop-rail-checkout-page] user should see customer phone number
    And [desktop-rail-checkout-page] user should see customer email

    When [desktop-rail-checkout-page] user fill adult passenger data
    Then [desktop-rail-checkout-page] user should see adult passenger data

    When [desktop-rail-checkout-page] user fill infant passenger data
    Then [desktop-rail-checkout-page] user should see infant passenger data

  @Positive @Regression
  Scenario: [DESKTOP] user pay for the ticket
    When [desktop-rail-checkout-page] user click proceed to payment
    Then [desktop-rail-payment-page] user should see bill details
    And [desktop-rail-payment-page] user should see trip details
    And [desktop-rail-payment-page] user should see passenger details

    When [desktop-rail-payment-page] user fill payment method
    Then [desktop-rail-thankyou-page] user should see thank you section

  @Positive @Regression
  Scenario: [DESKTOP] user cancel order
    When [desktop-rail-checkout-page] user click proceed to payment
    Then [desktop-rail-payment-page] user should see bill details
    And [desktop-rail-payment-page] user should see trip details
    And [desktop-rail-payment-page] user should see passenger details

    When [desktop-rail-payment-page] user click cancel order
    And [desktop-rail-payment-page] user click confirm cancel order
    Then [desktop-rail-payment-page] user see a message cancel order was successful