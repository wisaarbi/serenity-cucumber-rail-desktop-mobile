@RailDesktop @RailDesktopCheckoutTicketFeature @UI
Feature: Rail Desktop Checkout Ticket Feature

  Background: [DESKTOP] user search ticket
    Given [desktop-home-page] user open home page blibli
    And [desktop-home-page] user close iframe if show up
    And [desktop-home-page] user decline location permission
    And [desktop-home-page] user click all category
    And [desktop-favourites-page] user click kereta
    Then [desktop-rail-search-page] user should see rail search form

    When [desktop-rail-search-page] user click departure location
    And [desktop-rail-search-page] user fill departure city 'validDeptCity'
    And [desktop-rail-search-page] user fill departure station 'validDeptStation'
    And [desktop-rail-search-page] user click destination location
    And [desktop-rail-search-page] user fill destination city 'validDestCity'
    And [desktop-rail-search-page] user fill destination station 'validDestStation'
    And [desktop-rail-search-page] user fill date of departure with day plus '4'
    And [desktop-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [desktop-rail-search-page] user click cari
    Then [desktop-rail-list-page] user should see route departure station
    And [desktop-rail-list-page] user should see route destination station
    And [desktop-rail-list-page] user should see list departure station
    And [desktop-rail-list-page] user should see list destination station
    And [desktop-rail-list-page] user should see total passenger

    When [desktop-rail-list-page] user filter departure time
    Then [desktop-rail-list-page] user should see filtered list departure time

    When [desktop-rail-list-page] user filter departure class
    Then [desktop-rail-list-page] user should see filtered list departure class

    When [desktop-rail-list-page] user click choose departure ticket
    Then [desktop-rail-checkout-page] user should see order detail
    And [desktop-rail-checkout-page] user should see origin station
    And [desktop-rail-checkout-page] user should see destination station
    And [desktop-rail-checkout-page] user should see ticket price
    And [desktop-rail-checkout-page] user should see total ticket price
    And [desktop-rail-checkout-page] user should see total passenger
    And [desktop-rail-checkout-page] user should see train class
    And [desktop-rail-checkout-page] user should see date

  @Positive @Regression
  Scenario: [DESKTOP] user fill customer data with valid data
    When [desktop-rail-checkout-page] user fill customer title 'customerTitle'
    And [desktop-rail-checkout-page] user fill customer name 'validCustomerName'
    And [desktop-rail-checkout-page] user fill customer phone number 'validCustomerPhoneNumber'
    And [desktop-rail-checkout-page] user fill customer email 'validCustomerEmail'
    And [desktop-rail-checkout-page] user click continue to fill in passenger data
    Then [desktop-rail-checkout-page] user should see customer title
    And [desktop-rail-checkout-page] user should see customer name
    And [desktop-rail-checkout-page] user should see customer phone number
    And [desktop-rail-checkout-page] user should see customer email

  @Negative @Regression
  Scenario Outline: [DESKTOP] user fill customer data with invalid data : customer title <customerTitle>, customerName <customerName>, customer phone number <customerPhoneNumber>, customer email <customerEmail>
    When [desktop-rail-checkout-page] user fill customer title '<customerTitle>'
    And [desktop-rail-checkout-page] user fill customer name '<customerName>'
    And [desktop-rail-checkout-page] user fill customer phone number '<customerPhoneNumber>'
    And [desktop-rail-checkout-page] user fill customer email '<customerEmail>'
    And [desktop-rail-checkout-page] user click continue to fill in passenger data
    Then [desktop-rail-checkout-page] user should see error message '<errorMessage>' on customer section
    Examples:
      | customerTitle | customerName                 | customerPhoneNumber      | customerEmail      | errorMessage                                      |
      |               | validCustomerName            | validCustomerPhoneNumber | validCustomerEmail | Title harus dipilih.                              |
      | customerTitle |                              | validCustomerPhoneNumber | validCustomerEmail | Nama lengkap tidak boleh kosong.                  |
      | customerTitle | validCustomerName            |                          | validCustomerEmail | Nomor telepon tidak boleh kosong                  |
      | customerTitle | validCustomerName            | validCustomerPhoneNumber |                    | Email tidak boleh kosong                          |
      | customerTitle | ai                           | validCustomerPhoneNumber | validCustomerEmail | Nama lengkap harus lebih panjang dari 3 karakter. |
      | customerTitle | wisa arbi reaktanoe ashsidiq | validCustomerPhoneNumber | validCustomerEmail | Nama lengkap harus lebih pendek dari 25 karakter. |
      | customerTitle | validCustomerName            | 081                      | validCustomerEmail | Format nomor telepon yang Anda masukkan salah     |
      | customerTitle | validCustomerName            | validCustomerPhoneNumber | email              | Format email yang Anda masukkan salah             |

  @Positive @Regression
  Scenario: [DESKTOP] user fill adult passenger (main) data same as customer
    When [desktop-rail-checkout-page] user fill customer title 'customerTitle'
    And [desktop-rail-checkout-page] user fill customer name 'validCustomerName'
    And [desktop-rail-checkout-page] user fill customer phone number 'validCustomerPhoneNumber'
    And [desktop-rail-checkout-page] user fill customer email 'validCustomerEmail'
    And [desktop-rail-checkout-page] user click continue to fill in passenger data
    Then [desktop-rail-checkout-page] user should see customer title
    And [desktop-rail-checkout-page] user should see customer name
    And [desktop-rail-checkout-page] user should see customer phone number
    And [desktop-rail-checkout-page] user should see customer email

    When [desktop-rail-checkout-page] user click same as customer
    Then [desktop-rail-checkout-page] user should see title adult passenger is same as customer
    And [desktop-rail-checkout-page] user should see name adult passenger is same as customer

  @Positive @Regression
  Scenario: [DESKTOP] user fill passenger data with valid data
    When [desktop-rail-checkout-page] user fill customer title 'customerTitle'
    And [desktop-rail-checkout-page] user fill customer name 'validCustomerName'
    And [desktop-rail-checkout-page] user fill customer phone number 'validCustomerPhoneNumber'
    And [desktop-rail-checkout-page] user fill customer email 'validCustomerEmail'
    And [desktop-rail-checkout-page] user click continue to fill in passenger data
    Then [desktop-rail-checkout-page] user should see customer title
    And [desktop-rail-checkout-page] user should see customer name
    And [desktop-rail-checkout-page] user should see customer phone number
    And [desktop-rail-checkout-page] user should see customer email

    When [desktop-rail-checkout-page] user fill adult passenger data
    Then [desktop-rail-checkout-page] user should see adult passenger data

    When [desktop-rail-checkout-page] user fill infant passenger data
    Then [desktop-rail-checkout-page] user should see infant passenger data