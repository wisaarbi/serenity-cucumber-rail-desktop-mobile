@RailDesktop @DesktopLoginFeature @UI
Feature: Desktop Login Feature

  Background: [DESKTOP] user open login page
    Given [desktop-home-page] user open home page blibli
    And [desktop-home-page] user close iframe if show up
    And [desktop-home-page] user decline location permission
    And [desktop-home-page] user click all category
    And [desktop-favourites-page] user click kereta
    Then [desktop-rail-search-page] user should see rail search form

    When [desktop-header-travel-page] user click login
    Then [desktop-login-page] user should see login form

  @Positive @Regression
  Scenario: [DESKTOP] user login with registered email and password
    When [desktop-login-page] user fill email with 'validEmail'
    And [desktop-login-page] user fill password with 'validPassword'
    And [desktop-login-page] user click login
    And [desktop-login-page] user click send verification code via email
    And [desktop-login-page] user gets a verification code with
      | email            | password            | host           | port |
      | validEmailGoogle | validPasswordGoogle | imap.gmail.com | 993  |

    When [desktop-login-page] user fill the verification code
    And [desktop-login-page] user click verification
    And [desktop-profile-page] user open profile page
    Then [desktop-header-page] user should see user already login
    Then [desktop-profile-page] user should see the same email

    Given [desktop-profile-page] user get cookies
    And [login-api] user prepare data get user has logged in with cookies
    When [login-api] user send get user has logged in request
    Then [login-api] user should see get user has logged in response with status code 200
    And [login-api] user should see get user has logged in response with status message 'OK'
    And [login-api] user should see get user has logged in response with email 'validEmail'

  @Negative @Regression
  Scenario Outline: [DESKTOP] user can't login with following data <email>, <password>
    When [desktop-login-page] user fill email with '<email>'
    And [desktop-login-page] user fill password with '<password>'
    And [desktop-login-page] user click login
    Then [desktop-login-page] user should see error message '<errorMessage>'

    Given [desktop-profile-page] user get cookies
    And [login-api] user prepare data get user has logged in with cookies
    When [login-api] user send get user has logged in request
    Then  [login-api] user should see get user has logged in response with status code 401
    And [login-api] user should see get user has logged in response with status message 'UNAUTHORIZED'
    Examples:
      | email              | password        | errorMessage                        |
      | notRegisteredEmail | invalidPassword | Yakin itu benar? Coba diingat lagi. |
      | invalidEmail       | invalidPassword | Email/nomor HP-nya tidak valid      |

  @Negative @Regression
  Scenario Outline: [DESKTOP] user can't click login with empty field <email> or <password>
    When [desktop-login-page] user fill email with '<email>'
    And [desktop-login-page] user fill password with '<password>'
    Then [desktop-login-page] user should see button login is 'disabled'
    Examples:
      | email              | password        |
      | notRegisteredEmail |                 |
      |                    | invalidPassword |