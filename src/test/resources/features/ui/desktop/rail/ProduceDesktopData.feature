@ProduceDesktopData
Feature: Produce Desktop Data Feature

  Scenario: [DESKTOP] get list popular city
    Given [desktop-home-page] user open home page blibli
    And [desktop-home-page] user close iframe if show up
    And [desktop-home-page] user decline location permission
    And [desktop-home-page] user click all category
    And [desktop-favourites-page] user click kereta
    Then [desktop-rail-search-page] user should see rail search form
    When [desktop-rail-search-page] user click departure location
    Then [desktop-rail-search-page] user get list popular city