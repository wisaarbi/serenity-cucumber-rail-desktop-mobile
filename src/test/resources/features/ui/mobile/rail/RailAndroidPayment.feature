@RailAndroid @RailAndroidPaymentFeature @UI
Feature: Rail Android Payment Feature

  Background: [ANDROID] user open rail search page
    Given [android-home-page] user open blibli application
    And [android-home-page] user click all category
    And [android-all-category-page] user click kereta
    Then [android-rail-search-page] user should see rail search form

    When [android-rail-search-page] user click departure location
    And [android-rail-departure-location-page] user fill departure city 'validDeptCity'
    And [android-rail-departure-location-page] user fill departure station 'validDeptStation'
    And [android-rail-search-page] user click destination location
    And [android-rail-destination-location-page] user fill destination city 'validDestCity'
    And [android-rail-destination-location-page] user fill destination station 'validDestStation'
    And [android-rail-search-page] user fill date of departure with day plus '4'
    And [android-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [android-rail-search-page] user click cari
    Then [android-rail-list-page] user should see route departure station
    And [android-rail-list-page] user should see route destination station
    And [android-rail-list-page] user should see list departure station
    And [android-rail-list-page] user should see list destination station
    And [android-rail-list-page] user should see total passenger

    When [android-rail-list-page] user filter departure class
    Then [android-rail-list-page] user should see filtered list departure class

    When [android-rail-list-page] user filter departure train type
    Then [android-rail-list-page] user should see filtered list departure train type

    When [android-rail-list-page] user click choose departure ticket
    Then [android-rail-checkout-page] user should see order details

    When [android-rail-checkout-page] user click see order details
    Then [android-rail-order-details-page] user should see origin station
    And [android-rail-order-details-page] user should see destination station
    And [android-rail-order-details-page] user should see train class
    And [android-rail-order-details-page] user should see train type
    And [android-rail-order-details-page] user should see total passenger
    And [android-rail-order-details-page] user should see ticket price
    And [android-rail-order-details-page] user should see total ticket price

    When [android-rail-order-details-page] user close order details page
    And [android-rail-checkout-page] user fill customer title 'customerTitle'
    And [android-rail-checkout-page] user fill customer name 'validCustomerName'
    And [android-rail-checkout-page] user fill customer phone number 'validCustomerPhoneNumber'
    And [android-rail-checkout-page] user fill customer email 'validCustomerEmail'
    And [android-rail-checkout-page] user click continue to fill in passenger data
    Then [android-rail-checkout-page] user should see progress bar value is increased

    When [android-rail-checkout-page] user fill adult passenger data
    Then [android-rail-checkout-page] user should see progress bar value is increased
    When [android-rail-checkout-page] user fill infant passenger data
    Then [android-rail-checkout-page] user should see progress bar value is increased
    And [android-rail-checkout-page] user should see customer data
    And [android-rail-checkout-page] user should see adult passenger data
    And [android-rail-checkout-page] user should see infant passenger data

  @Positive @Regression
  Scenario: [ANDROID] user want to see bill details
    When [android-rail-checkout-page] user click proceed to payment
    And [android-rail-payment-page] user click bill details
    Then [android-rail-payment-page] user should see total payment
    And [android-rail-payment-page] user should see trip details
    And [android-rail-payment-page] user should see passenger details

  @Positive @Regression
  Scenario: [ANDROID] user cancel order
    When [android-rail-checkout-page] user click proceed to payment
    And [android-rail-payment-page] user click cancel order
    And [android-rail-payment-page] user click confirm cancel order
    Then [android-rail-search-page] user should see rail search form

  @Positive @Regression
  Scenario: [ANDROID] user pay for the ticket
    When [android-rail-checkout-page] user click proceed to payment
    And [android-rail-payment-page] user fill payment method
    Then [android-rail-thankyou-page] user should see thank you section