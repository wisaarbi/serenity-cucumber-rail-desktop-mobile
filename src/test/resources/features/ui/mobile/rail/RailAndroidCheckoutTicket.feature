@RailAndroid @RailAndroidCheckoutTicketFeature @UI
Feature: Rail Android Checkout Ticket Feature

  Background: [ANDROID] user open rail search page
    Given [android-home-page] user open blibli application
    And [android-home-page] user click all category
    And [android-all-category-page] user click kereta
    Then [android-rail-search-page] user should see rail search form

    When [android-rail-search-page] user click departure location
    And [android-rail-departure-location-page] user fill departure city 'validDeptCity'
    And [android-rail-departure-location-page] user fill departure station 'validDeptStation'
    And [android-rail-search-page] user click destination location
    And [android-rail-destination-location-page] user fill destination city 'validDestCity'
    And [android-rail-destination-location-page] user fill destination station 'validDestStation'
    And [android-rail-search-page] user fill date of departure with day plus '4'
    And [android-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [android-rail-search-page] user click cari
    Then [android-rail-list-page] user should see route departure station
    And [android-rail-list-page] user should see route destination station
    And [android-rail-list-page] user should see list departure station
    And [android-rail-list-page] user should see list destination station
    And [android-rail-list-page] user should see total passenger

    When [android-rail-list-page] user filter departure class
    Then [android-rail-list-page] user should see filtered list departure class

    When [android-rail-list-page] user filter departure train type
    Then [android-rail-list-page] user should see filtered list departure train type

    When [android-rail-list-page] user click choose departure ticket
    Then [android-rail-checkout-page] user should see order details

    When [android-rail-checkout-page] user click see order details
    Then [android-rail-order-details-page] user should see origin station
    And [android-rail-order-details-page] user should see destination station
    And [android-rail-order-details-page] user should see train class
    And [android-rail-order-details-page] user should see train type
    And [android-rail-order-details-page] user should see total passenger
    And [android-rail-order-details-page] user should see ticket price
    And [android-rail-order-details-page] user should see total ticket price

    When [android-rail-order-details-page] user close order details page

  @Positive @Regression
  Scenario: [ANDROID] user fill customer data with valid data
    When [android-rail-checkout-page] user fill customer title 'customerTitle'
    And [android-rail-checkout-page] user fill customer name 'validCustomerName'
    And [android-rail-checkout-page] user fill customer phone number 'validCustomerPhoneNumber'
    And [android-rail-checkout-page] user fill customer email 'validCustomerEmail'
    And [android-rail-checkout-page] user click continue to fill in passenger data
    Then [android-rail-checkout-page] user should see progress bar value is increased

  @Negative @Regression
  Scenario Outline: [ANDROID] user fill customer data with invalid data
    When [android-rail-checkout-page] user fill customer title 'customerTitle'
    And [android-rail-checkout-page] user fill customer name '<customerName>'
    And [android-rail-checkout-page] user fill customer phone number '<customerPhoneNumber>'
    And [android-rail-checkout-page] user fill customer email '<customerEmail>'
    And [android-rail-checkout-page] user click continue to fill in passenger data
    Then [android-rail-checkout-page] user should see error message '<errorMessage>'
    Examples:
      | customerName      | customerPhoneNumber      | customerEmail      | errorMessage                              |
      |                   |                          |                    | Nama, Email dan Nomor Telepon wajib diisi |
      | ai                | validCustomerPhoneNumber | validCustomerEmail | Salah nama                                |
      | validCustomerName | 12                       | validCustomerEmail | Nomor Telepon valid                       |
      | validCustomerName | validCustomerPhoneNumber | email              | Email tidak valid                         |

  @Positive @Regression
  Scenario: [ANDROID] user fill adult passenger (main) data same as customer
    When [android-rail-checkout-page] user fill customer title 'customerTitle'
    And [android-rail-checkout-page] user fill customer name 'validCustomerName'
    And [android-rail-checkout-page] user fill customer phone number 'validCustomerPhoneNumber'
    And [android-rail-checkout-page] user fill customer email 'validCustomerEmail'
    And [android-rail-checkout-page] user click continue to fill in passenger data
    Then [android-rail-checkout-page] user should see progress bar value is increased

    When [android-rail-checkout-page] user click same as customer
    Then [android-rail-checkout-page] user should see title adult passenger is same as customer
    And [android-rail-checkout-page] user should see name adult passenger is same as customer

  @Positive @Regression
  Scenario: [ANDROID] user fill passenger data with valid data
    When [android-rail-checkout-page] user fill customer title 'customerTitle'
    And [android-rail-checkout-page] user fill customer name 'validCustomerName'
    And [android-rail-checkout-page] user fill customer phone number 'validCustomerPhoneNumber'
    And [android-rail-checkout-page] user fill customer email 'validCustomerEmail'
    And [android-rail-checkout-page] user click continue to fill in passenger data
    Then [android-rail-checkout-page] user should see progress bar value is increased

    When [android-rail-checkout-page] user fill adult passenger data
    Then [android-rail-checkout-page] user should see progress bar value is increased
    When [android-rail-checkout-page] user fill infant passenger data
    Then [android-rail-checkout-page] user should see progress bar value is increased
    And [android-rail-checkout-page] user should see customer data
    And [android-rail-checkout-page] user should see adult passenger data
    And [android-rail-checkout-page] user should see infant passenger data

  @Negative @Regression
  Scenario Outline: [ANDROID] user fill passenger data with invalid data
    When [android-rail-checkout-page] user fill customer title 'customerTitle'
    And [android-rail-checkout-page] user fill customer name 'validCustomerName'
    And [android-rail-checkout-page] user fill customer phone number 'validCustomerPhoneNumber'
    And [android-rail-checkout-page] user fill customer email 'validCustomerEmail'
    And [android-rail-checkout-page] user click continue to fill in passenger data
    Then [android-rail-checkout-page] user should see progress bar value is increased

    When [android-rail-checkout-page] user fill adult passenger title 'Tuan'
    And [android-rail-checkout-page] user fill adult passenger name '<adultPassengerName>'
    And [android-rail-checkout-page] user fill adult passenger identityCard '<adultPassengerIdentityCard>'
    And [android-rail-checkout-page] user click save passenger data
    Then [android-rail-checkout-page] user should see error message '<errorMessage>'
    Examples:
      | adultPassengerName                   | adultPassengerIdentityCard           | errorMessage                                                         |
      |                                      | 16051899                             | Nama Lengkap adalah wajib                                            |
      | ad                                   | 16051899                             | Panjang Nama Lengkap harus lebih besar dari atau sama dengan 3       |
      | namapanjangbangetlebihdari25karakter | 16051899                             | Panjang Nama Lengkap harus kurang dari atau sama dengan 25           |
      | 11123131132                          | 16051899                             | Salah Nama Lengkap                                                   |
      | nama orang                           |                                      | KTP / SIM / Paspor adalah wajib                                      |
      | nama orang                           | 12                                   | Panjang KTP / SIM / Paspor harus lebih besar dari atau sama dengan 7 |
      | nama orang                           | 126124186412541524178216864184262147 | Panjang KTP / SIM / Paspor harus kurang dari atau sama dengan 25     |