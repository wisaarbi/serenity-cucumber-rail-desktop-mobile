@ProduceMobileData
Feature: Produce Mobile Data Feature

  @Positive @Regression
  Scenario: [ANDROID] user get list popular city
    Given [android-home-page] user open blibli application
    And [android-home-page] user click all category
    And [android-all-category-page] user click kereta
    Then [android-rail-search-page] user should see rail search form
    When [android-rail-search-page] user click departure location
    Then [android-rail-departure-location-page] user get list popular city