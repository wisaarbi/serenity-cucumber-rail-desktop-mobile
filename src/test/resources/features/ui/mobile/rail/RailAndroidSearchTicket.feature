@RailAndroid @RailAndroidSearchTicketFeature @UI
Feature: Rail Android Search Ticket Feature

  Background: [ANDROID] user open rail search page
    Given [android-home-page] user open blibli application
    And [android-home-page] user click all category
    And [android-all-category-page] user click kereta
    Then [android-rail-search-page] user should see rail search form

  @Positive @Regression
  Scenario: [ANDROID] user see list popular city
    When [android-rail-search-page] user click departure location
    Then [android-rail-departure-location-page] user should see list popular city

    When [android-rail-departure-location-page] user click back button
    And [android-rail-search-page] user click destination location
    Then [android-rail-destination-location-page] user should see list popular city

  @Positive @Regression
  Scenario: [ANDROID] user swap station
    When [android-rail-search-page] user click departure location
    And [android-rail-departure-location-page] user fill departure city 'validDeptCity'
    And [android-rail-departure-location-page] user fill departure station 'validDeptStation'
    And [android-rail-search-page] user click destination location
    And [android-rail-destination-location-page] user fill destination city 'validDestCity'
    And [android-rail-destination-location-page] user fill destination station 'validDestStation'
    And [android-rail-search-page] user click swap station
    Then [android-rail-search-page] user should see departure station has been swapped
    And [android-rail-search-page] user should see destination station has been swapped

  @Positive @Regression
  Scenario: [ANDROID] user search ticket with valid data
    When [android-rail-search-page] user click departure location
    And [android-rail-departure-location-page] user fill departure city 'validDeptCity'
    And [android-rail-departure-location-page] user fill departure station 'validDeptStation'
    And [android-rail-search-page] user click destination location
    And [android-rail-destination-location-page] user fill destination city 'validDestCity'
    And [android-rail-destination-location-page] user fill destination station 'validDestStation'
    And [android-rail-search-page] user fill date of departure with day plus '4'
    And [android-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [android-rail-search-page] user click cari
    Then [android-rail-list-page] user should see route departure station
    And [android-rail-list-page] user should see route destination station
    And [android-rail-list-page] user should see list departure station
    And [android-rail-list-page] user should see list destination station
    And [android-rail-list-page] user should see total passenger

  @Negative @Regression
  Scenario: [ANDROID] user search ticket with invalid city or station
    When [android-rail-search-page] user click departure location
    And [android-rail-departure-location-page] user fill departure city 'invalidDeptCity'
    Then [android-rail-departure-location-page] user should see search error message

    When [android-rail-departure-location-page] user click back button
    And [android-rail-search-page] user click destination location
    And [android-rail-destination-location-page] user fill destination city 'invalidDestCity'
    Then [android-rail-destination-location-page] user should see search error message