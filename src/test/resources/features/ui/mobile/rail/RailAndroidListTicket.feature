@RailAndroid @RailAndroidListTicketFeature @UI
Feature: Rail Android List Ticket Feature

  Background: [ANDROID] user open rail search page
    Given [android-home-page] user open blibli application
    And [android-home-page] user click all category
    And [android-all-category-page] user click kereta
    Then [android-rail-search-page] user should see rail search form

    When [android-rail-search-page] user click departure location
    And [android-rail-departure-location-page] user fill departure city 'validDeptCity'
    And [android-rail-departure-location-page] user fill departure station 'validDeptStation'
    And [android-rail-search-page] user click destination location
    And [android-rail-destination-location-page] user fill destination city 'validDestCity'
    And [android-rail-destination-location-page] user fill destination station 'validDestStation'
    And [android-rail-search-page] user fill date of departure with day plus '4'

  @Positive @Regression
  Scenario: [ANDROID] user filter by time
    And [android-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [android-rail-search-page] user click cari
    Then [android-rail-list-page] user should see route departure station
    And [android-rail-list-page] user should see route destination station
    And [android-rail-list-page] user should see list departure station
    And [android-rail-list-page] user should see list destination station
    And [android-rail-list-page] user should see total passenger

    When [android-rail-list-page] user filter departure time
    Then [android-rail-list-page] user should see filtered list departure time

  @Positive @Regression
  Scenario: [ANDROID] user filter by class
    And [android-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [android-rail-search-page] user click cari
    Then [android-rail-list-page] user should see route departure station
    And [android-rail-list-page] user should see route destination station
    And [android-rail-list-page] user should see list departure station
    And [android-rail-list-page] user should see list destination station
    And [android-rail-list-page] user should see total passenger

    When [android-rail-list-page] user filter departure class
    Then [android-rail-list-page] user should see filtered list departure class

  @Positive @Regression
  Scenario: [ANDROID] user filter by train type
    And [android-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [android-rail-search-page] user click cari
    Then [android-rail-list-page] user should see route departure station
    And [android-rail-list-page] user should see route destination station
    And [android-rail-list-page] user should see list departure station
    And [android-rail-list-page] user should see list destination station
    And [android-rail-list-page] user should see total passenger

    When [android-rail-list-page] user filter departure train type
    Then [android-rail-list-page] user should see filtered list departure train type

  @Positive @Regression
  Scenario Outline: [ANDROID] user filter by price with filter price <filterPrice>
    And [android-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [android-rail-search-page] user click cari
    Then [android-rail-list-page] user should see route departure station
    And [android-rail-list-page] user should see route destination station
    And [android-rail-list-page] user should see list departure station
    And [android-rail-list-page] user should see list destination station
    And [android-rail-list-page] user should see total passenger

    When [android-rail-list-page] user filter departure price to '<filterPrice>' price
    Then [android-rail-list-page] user should see filtered list '<filterPrice>' departure price
    Examples:
      | filterPrice |
      | MAX         |
      | MIN         |

  @Positive @Regression
  Scenario: [ANDROID] user change ticket
    And [android-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [android-rail-search-page] user click cari
    Then [android-rail-list-page] user should see route departure station
    And [android-rail-list-page] user should see route destination station
    And [android-rail-list-page] user should see list departure station
    And [android-rail-list-page] user should see list destination station
    And [android-rail-list-page] user should see total passenger

    When [android-rail-list-page] user click ganti pencarian
    And [android-route-change-page] user click departure location
    And [android-rail-departure-location-page] user fill departure city 'validChangeDeptCity'
    And [android-rail-departure-location-page] user fill departure station 'validChangeDeptStation'
    And [android-route-change-page] user click destination location
    And [android-rail-destination-location-page] user fill destination city 'validChangeDestCity'
    And [android-rail-destination-location-page] user fill destination station 'validChangeDestStation'
    And [android-route-change-page] user fill date of departure with day plus '5'
    And [android-route-change-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validChangeTotalAdultPassenger | validChangeTotalInfantPassenger |
    And [android-route-change-page] user click cari
    Then [android-rail-list-page] user should see route departure station
    And [android-rail-list-page] user should see route destination station
    And [android-rail-list-page] user should see total passenger

  @Positive @Regression
  Scenario: [ANDROID] user choose ticket departure (one way)
    And [android-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [android-rail-search-page] user click cari
    Then [android-rail-list-page] user should see route departure station
    And [android-rail-list-page] user should see route destination station
    And [android-rail-list-page] user should see list departure station
    And [android-rail-list-page] user should see list destination station
    And [android-rail-list-page] user should see total passenger

    When [android-rail-list-page] user filter departure class
    Then [android-rail-list-page] user should see filtered list departure class

    When [android-rail-list-page] user filter departure train type
    Then [android-rail-list-page] user should see filtered list departure train type

    When [android-rail-list-page] user click choose departure ticket
    Then [android-rail-checkout-page] user should see order details

    When [android-rail-checkout-page] user click see order details
    Then [android-rail-order-details-page] user should see origin station
    And [android-rail-order-details-page] user should see destination station
    And [android-rail-order-details-page] user should see train class
    And [android-rail-order-details-page] user should see train type
    And [android-rail-order-details-page] user should see total passenger
    And [android-rail-order-details-page] user should see ticket price
    And [android-rail-order-details-page] user should see total ticket price

  @Positive @Regression
  Scenario: [ANDROID] user choose ticket departure and return (round trip)
    And [android-rail-search-page] user fill date of return with day plus '5'
    And [android-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [android-rail-search-page] user click cari
    Then [android-rail-list-page] user should see route departure station
    And [android-rail-list-page] user should see route destination station
    And [android-rail-list-page] user should see list departure station
    And [android-rail-list-page] user should see list destination station
    And [android-rail-list-page] user should see total passenger

    When [android-rail-list-page] user filter departure class
    Then [android-rail-list-page] user should see filtered list departure class

    When [android-rail-list-page] user filter departure train type
    Then [android-rail-list-page] user should see filtered list departure train type

    When [android-rail-list-page] user click choose departure ticket
    Then [android-rail-list-page] user should see ticket summary departure

    When [android-rail-list-page] user filter return class
    Then [android-rail-list-page] user should see filtered list return class

    When [android-rail-list-page] user filter return train type
    Then [android-rail-list-page] user should see filtered list return train type

    When [android-rail-list-page] user click choose return ticket
    Then [android-rail-checkout-page] user should see order details

    When [android-rail-checkout-page] user click see order details
    Then [android-rail-order-details-page] user should see origin station
    And [android-rail-order-details-page] user should see destination station
    And [android-rail-order-details-page] user should see train class
    And [android-rail-order-details-page] user should see train type
    And [android-rail-order-details-page] user should see total passenger
    And [android-rail-order-details-page] user should see ticket price
    And [android-rail-order-details-page] user should see total ticket price
