@RailAndroid @AndroidLoginFeature @UI
Feature: Android Login Feature

  Background: [ANDROID] user open login page
    Given [android-home-page] user open blibli application
    When [android-home-page] user click login button

  @Positive @Regression
  Scenario: [ANDROID] user login with registered email and password
    When [android-login-page] user fill email 'validEmail'
    And [android-login-page] user fill password 'validPassword'
    And [android-login-page] user click login

    When [android-bottom-navigation-page] user click 'Akun'
    And [android-account-page] user click profile info
    Then [android-profile-page] user should see the same email

  @Negative @Regression
  Scenario Outline: [ANDROID] user can't login with following data <email>, <password>
    When [android-login-page] user fill email '<email>'
    And [android-login-page] user fill password '<password>'
    And [android-login-page] user click login
    Then [android-login-page] user should see error message '<errorMessage>'
    Examples:
      | email              | password        | errorMessage                        |
      | notRegisteredEmail | invalidPassword | Yakin itu benar? Coba diingat lagi. |
      | invalidEmail       | invalidPassword | Email/nomor HP-nya tidak valid.     |

  @Negative @Regression
  Scenario Outline: [ANDROID] user can't click login with empty field <email> or <password>
    When [android-login-page] user fill email '<email>'
    And [android-login-page] user fill password '<password>'
    Then [android-login-page] user should see button login is 'disabled'
    Examples:
      | email              | password        |
      | notRegisteredEmail |                 |
      |                    | invalidPassword |