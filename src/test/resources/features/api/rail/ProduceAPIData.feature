@ProduceAPIData
Feature: Produce API Data Feature

  Scenario: [API] get popular city name
    Given [rail-search-api] user get search popular station
    Then [rail-search-api] user save popular city name to API data

  Scenario: [API] get popular city code
    Given [rail-search-api] user get search popular station
    Then [rail-search-api] user save popular city code to API data