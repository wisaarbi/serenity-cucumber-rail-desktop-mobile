@RailAPI @SearchPopularStationFeature @API
Feature: Search Popular Station Feature

  @Positive @Regression
  Scenario: [API] user get search popular station
    Given [rail-search-api] user get search popular station
    Then [rail-search-api] user should see get search popular station response with status code 200
    And [rail-search-api] user should see get search popular station response with city name
      | cityName        |
      | Kota Bandung    |
      | Kota Jakarta    |
      | Kota Malang     |
      | Kota Solo       |
      | Kota Surabaya   |
      | Kota Yogyakarta |