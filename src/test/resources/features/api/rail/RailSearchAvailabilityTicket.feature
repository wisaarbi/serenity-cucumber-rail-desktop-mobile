@RailAPI @RailSearchAvailabilityTicketFeature @API
Feature: Rail Search Availability Ticket Feature

  @Positive @Regression
  Scenario Outline: [API] user get rail ticket availability with valid data : origin type <originType>, destination type <destinationType>, date departure <dateDeparture>, total adult passenger <totalAdultPassenger>, total infant passenger <totalInfantPassenger>
    Given [rail-search-api] user prepare data get rail ticket availability with origin code '<originCode>'
    And [rail-search-api] user prepare data get rail ticket availability with origin type '<originType>'
    And [rail-search-api] user prepare data get rail ticket availability with destination code '<destinationCode>'
    And [rail-search-api] user prepare data get rail ticket availability with destination type '<destinationType>'
    And [rail-search-api] user prepare data get rail ticket availability with date departure '<dateDeparture>'
    And [rail-search-api] user prepare data get rail ticket availability with total adult passenger <totalAdultPassenger>
    And [rail-search-api] user prepare data get rail ticket availability with total infant passenger <totalInfantPassenger>
    When [rail-search-api] user send get rail ticket availability request
    Then [rail-search-api] user should see get rail ticket availability response with status code 200
    And [rail-search-api] user should see get rail ticket availability response with origin code is same
    And [rail-search-api] user should see get rail ticket availability response with destination code is same
    And [rail-search-api] user should see get rail ticket availability response with departure date is same
    And [rail-search-api] user should see get rail ticket availability response with total adult is same
    And [rail-search-api] user should see get rail ticket availability response with total infant is same
    Examples:
      | originCode | originType | destinationCode | destinationType | dateDeparture | totalAdultPassenger | totalInfantPassenger |
      | Sidoarjo   | CITY       | Surabaya        | CITY            | day plus 4    | 1                   | 0                    |
      | SDA        | STATION    | SGU             | STATION         | day plus 4    | 1                   | 1                    |
      | SDA        | STATION    | Surabaya        | CITY            | day plus 4    | 5                   | 5                    |
      | Surabaya   | CITY       | SDA             | STATION         | day plus 4    | 0                   | 0                    |

  @Negative @Regression
  Scenario Outline: [API] user get rail ticket availability with invalid data : origin type <originType>, destination type <destinationType>, date departure <dateDeparture>, total adult passenger <totalAdultPassenger>, total infant passenger <totalInfantPassenger>
    Given [rail-search-api] user prepare data get rail ticket availability with origin code '<originCode>'
    And [rail-search-api] user prepare data get rail ticket availability with origin type '<originType>'
    And [rail-search-api] user prepare data get rail ticket availability with destination code '<destinationCode>'
    And [rail-search-api] user prepare data get rail ticket availability with destination type '<destinationType>'
    And [rail-search-api] user prepare data get rail ticket availability with date departure '<dateDeparture>'
    And [rail-search-api] user prepare data get rail ticket availability with total adult passenger <totalAdultPassenger>
    And [rail-search-api] user prepare data get rail ticket availability with total infant passenger <totalInfantPassenger>
    When [rail-search-api] user send get rail ticket availability request
    Then [rail-search-api] user should see get rail ticket availability response with status code 500
    And [rail-search-api] user should see get rail ticket availability response with message '<message>'
    Examples:
      | originCode | originType | destinationCode | destinationType | dateDeparture | totalAdultPassenger | totalInfantPassenger | message                                         |
      | Jakarta    | STATION    | Bandung         | CITY            | 2020-04-15    | 1                   | 0                    | Station not found                               |
      | SDA        | STATION    | SGU             | CITY            | 2020-04-15    | 1                   | 0                    | City not found                                  |
      | SDA        | STATION    | Jakarta         | CITY            | 2020-03-25    | 1                   | 0                    | No train available                              |
      | Jakarta    | CITY       | SDA             | STATION         | 2020-04-15    | 1                   | 2                    | Total infant cannot be greater than total adult |