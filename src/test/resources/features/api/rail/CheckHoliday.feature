@RailAPI @CheckHolidayFeature @API
Feature: Check Holiday Feature

  @Positive @Regression
  Scenario Outline: [API] user get check holiday with valid data : year start <yearStart>, month start <monthStart>, year end <yearEnd>, month end <monthEnd>
    Given [check-holiday-api] user prepare data get check holiday with year start <yearStart>
    And [check-holiday-api] user prepare data get check holiday with month start <monthStart>
    And [check-holiday-api] user prepare data get check holiday with year end <yearEnd>
    And [check-holiday-api] user prepare data get check holiday with month end <monthEnd>
    When [check-holiday-api] user get check holiday
    Then [check-holiday-api] user should see get check holiday response with status code '<statusCode>'
    And [check-holiday-api] user should see get check holiday response with status message '<statusMessage>'
    Examples:
      | yearStart | monthStart | yearEnd | monthEnd | statusCode | statusMessage |
      | 2020      | 7          | 2020    | 9        | OK         | ACCEPTED      |

  @Negative @Regression
  Scenario Outline: [API] user get check holiday with invalid data : year start <yearStart>, month start <monthStart>, year end <yearEnd>, month end <monthEnd>
    Given [check-holiday-api] user prepare data get check holiday with year start <yearStart>
    And [check-holiday-api] user prepare data get check holiday with month start <monthStart>
    And [check-holiday-api] user prepare data get check holiday with year end <yearEnd>
    And [check-holiday-api] user prepare data get check holiday with month end <monthEnd>
    When [check-holiday-api] user get check holiday
    Then [check-holiday-api] user should see get check holiday response with status code '<statusCode>'
    And [check-holiday-api] user should see get check holiday response with status message '<statusMessage>'
    Examples:
      | yearStart | monthStart | yearEnd | monthEnd | statusCode   | statusMessage |
      | 2020      | 7          | 2020    | 6        | INVALID_DATE | Invalid date  |