pipeline {
    agent any
    stages {
        stage("Setup IFrame for Selenoid") {
            steps {
                script {
                    currentBuild.rawBuild.project.setDescription("<iframe src='http://192.168.1.8:8888/#/' width='1400' height='500'></iframe>")
                }
            }
        }
        stage('Prepare Project') {
            steps {
                bat 'mvn clean compile'
            }
        }
        stage('Run Parallel UI Scenarios on Desktop and Mobile') {
            parallel {
                stage('Run UI Scenarios on Desktop using Chrome') {
                    steps {
                        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                            bat 'mvn verify -Dconfiguration.webdriver.driver=chrome -Dcontext=chrome -Dreport.customfields.Driver=Chrome -Dconfiguration.webdriver.isRemote=true -Dcucumber.options="--tags @RailDesktop" -Dspring.profiles.active=qa1'
                        }
                    }
                }
                stage('Run UI Scenarios on Samsung Mobile Device') {
                    steps {
                        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                            bat 'mvn verify -Dconfiguration.webdriver.driver=appium -Dcontext=samsung -Dreport.customfields.Driver=Appium -Dconfiguration.webdriver.isRemote=false -Dconfiguration.webdriver.appium.hub=http://127.0.0.1:4723/wd/hub -Dconfiguration.webdriver.appium.platformName=Android -Dconfiguration.webdriver.appium.udid=RR8M9076DJT -Dconfiguration.webdriver.appium.systemPort=8200 -Dcucumber.options="--tags @RailAndroid" -Dspring.profiles.active=qa1'
                        }
                    }
                }
            }
        }
        stage('Run Parallel API Scenarios') {
            parallel {
                stage('Run Positive API Scenarios') {
                    steps {
                        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                            bat 'mvn verify -Dconfiguration.webdriver.driver=chrome -Dcontext=chrome -Dreport.customfields.Driver=Chrome -Dconfiguration.webdriver.isRemote=true -Dcucumber.options="--tags @RailAPI --tags @Positive" -Dspring.profiles.active=qa1'
                        }
                    }
                }
                stage('Run Negative API Scenarios') {
                    steps {
                        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                            bat 'mvn verify -Dconfiguration.webdriver.driver=chrome -Dcontext=chrome -Dreport.customfields.Driver=Chrome -Dconfiguration.webdriver.isRemote=true -Dcucumber.options="--tags @RailAPI --tags @Negative" -Dspring.profiles.active=qa1'
                        }
                    }
                }
            }
        }
        stage('Generate Report') {
            steps {
                bat 'mvn serenity:aggregate'
            }
        }
    }
    post {
        always {
            script {
                currentBuild.rawBuild.project.setDescription("")
            }
            publishHTML(target: [
                    allowMissing         : false,
                    alwaysLinkToLastBuild: false,
                    keepAll              : true,
                    reportDir            : 'target/site/serenity',
                    reportFiles          : 'index.html',
                    reportName           : "Serenity Report"
            ])
            slackUploadFile channel: '#jenkins-pipeline-serenity-rail', filePath: 'target\\site\\serenity\\serenity-summary.html', initialComment: 'Summary Report', credentialId: 'slack-upload-plugin-token'
        }
        success {
            slackSend baseUrl: 'https://hooks.slack.com/services/', channel: '#jenkins-pipeline-serenity-rail', color: 'good', message: "SUCCESS after ${currentBuild.durationString.minus(' and counting')}\n${env.JOB_NAME} \nBuild #${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open Report>) (<http://192.168.1.8:8888/#/videos|Open Video Record>)", teamDomain: 'blibli-future-program', tokenCredentialId: 'slack-notification-serenity-rail'
        }
        failure {
            slackSend baseUrl: 'https://hooks.slack.com/services/', channel: '#jenkins-pipeline-serenity-rail', color: 'danger', message: "FAILED after ${currentBuild.durationString.minus(' and counting')}\n${env.JOB_NAME} \nBuild #${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open Report>) (<http://192.168.1.8:8888/#/videos|Open Video Record>)", teamDomain: 'blibli-future-program', tokenCredentialId: 'slack-notification-serenity-rail'
        }
    }
}